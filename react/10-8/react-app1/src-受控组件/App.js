import React, { Component } from 'react'
import './App.scss'

class App extends Component {

  state = {
    user: '王小明'
  }

  render() {
    const { user } = this.state
    return (
      <div className='app'>
        <h1>受控组件</h1>
        <div>
          <h4>{user}</h4>
          {/* 受控组件: 表单的value值受state或者props数据的控制，受控组件必须定义onChange事件修改数据 */}
          <input type="text" value={user} onChange={e => {
            console.log(e.target.value)
            this.setState({
              user: e.target.value
            })
          }} />
        </div>
        <button onClick={() => {
          console.log('user', this.state.user)
        }}>提交</button>
      </div>
    )
  }
}

export default App
