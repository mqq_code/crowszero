import React, { Component, createRef } from 'react'
import './App.scss'

class App extends Component {
  /* 方式二： */
  user = createRef() // 创建一个ref对象
  age = createRef()
  sex = createRef()
  address = createRef()
  hobby = createRef()
  height = createRef()

  submit = () => {
    // 获取dom元素
    console.log(this.user.current.value)
    console.log(this.age.current.value)
    console.log(this.sex.current.value)
    console.log(this.address.current.value)
    console.log(this.hobby.current.value)
    console.log(this.height.current.value)
  }

  render() {
    return (
      <div className='app'>
        <h1>非受控组件 -- ref操作dom</h1>
        <div>姓名: <input defaultValue='小明' ref={this.user} type="text" /></div>
        <div>年龄: <input defaultValue='20' ref={this.age} type="text" /></div>
        <div>性别: <input defaultValue='男' ref={this.sex} type="text" /></div>
        <div>地址: <input defaultValue='北京' ref={this.address} type="text" /></div>
        <div>爱好: <input defaultValue='吃饭' ref={this.hobby} type="text" /></div>
        <div>婚否: <input defaultChecked={true} ref={this.height} type="checkbox" /></div>
        <button onClick={this.submit}>提交</button>
      </div>
    )
  }
}

// class App extends Component {
//   user = null
//   submit = () => {
//     console.log(this.user.value)
//   }
//   render() {
//     return (
//       <div className='app'>
//         <h1>非受控组件 -- ref操作dom</h1>
//         <div>
//           {/* 方式一： */}
//           姓名: <input ref={el => this.user = el} type="text" />
//         </div>
//         <button onClick={this.submit}>提交</button>
//       </div>
//     )
//   }
// }

export default App
