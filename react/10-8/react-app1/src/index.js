import React from 'react'; // react核心语法，比如创建虚拟dom
import ReactDOM from 'react-dom/client'; // dom操作相关
import App from './App'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />)