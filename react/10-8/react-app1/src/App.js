import React, { Component } from 'react'
import './App.scss'
import Goods from './components/Goods'


class App extends Component {

  state = {
    apple: {
      title: '红富士苹果',
      price: 10,
      count: 0
    }
  }

  changeApple = (count) => {
    this.setState({
      apple: {
        ...this.state.apple,
        count
      }
    })
  }

  render() {
    const { apple } = this.state
    return (
      <div>
        <h1>组件基础</h1>
        <Goods
          title={apple.title}
          price={apple.price}
          count={apple.count}
          changeApple={this.changeApple}
        />
      </div>
    )
  }
}


export default App