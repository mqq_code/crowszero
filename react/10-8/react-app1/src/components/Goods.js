import React, { Component } from 'react'

class Goods extends Component {
  render() {

    const { title, price, count, changeApple } = this.props

    return (
      <div className='goods'>
        <h2>{ title }</h2>
        <p>价格: ¥{price}</p>
        {count > 0 && <>
          <button onClick={() => changeApple(count - 1)}>-</button>
          {count}
        </>}
        <button onClick={() => changeApple(count + 1)}>+</button>
      </div>
    )
  }
}


export default Goods