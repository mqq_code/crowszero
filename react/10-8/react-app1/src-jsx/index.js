import React from 'react'; // 创建虚拟dom
import ReactDOM from 'react-dom/client';

// const div = React.createElement('div', { className: 'box' }, [
//   React.createElement('h1', { className: 'title', id: 'h1', style: { color: 'red', fontSize: '50px' } }, [
//     '开始学习',
//     React.createElement('b', { style: { color: 'green' } }, ['react!'])
//   ]),
//   React.createElement('ul', {}, [
//     React.createElement('li', {}, ['第一项']),
//     React.createElement('li', {}, ['第二项']),
//     React.createElement('li', {}, ['第三项']),
//     React.createElement('li', {}, ['第四项'])
//   ])
// ])

// jsx: js + xml，React.createElement的语法糖，通过babel把 jsx 转译成 js
// jsx语法中使用变量需要用一个大括号包起来

const title = '默认标题'
const h1Style = {
  fontSize: '50px',
  color: 'orange'
}
const url = 'http://www.baidu.com'
const num = 70
const arr = ['第一项', '第二项', '第三项', '第四项']
const arr1 = [
  <li>11</li>,
  <li>22</li>,
  <li>33</li>
]

const div = <div className='box'>
  <h1 style={h1Style}>{ title }<b style={{ color: 'red' }}>react!</b></h1>
  <ul>
    {arr.map(item => <li key={item}>{item}</li>)}
  </ul>
  {num > 60 ? <b>分数： {num}, 恭喜你及格了</b> : <span>分数： {num}, 不及格</span>}
  
  {num > 60 && <a href={url}>跳转百度</a>}

  <div>string == {'string'}</div>
  <div>number == {100}</div>
  <div>true == {true}</div>
  <div>false == {false}</div>
  <div>null == {null}</div>
  <div>undefined == {undefined}</div>
  <div>[] == {[1,2,3,4]}</div>
  {/* <div>{} == {{ name: 'aa' }}</div> 不能在 {} 中直接渲染对象 */}
</div>

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(div)