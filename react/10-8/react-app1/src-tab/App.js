import React, { Component } from 'react'
import './App.scss'

class App extends Component {

  state = {
    tablist: [
      {
        title: '歌曲',
        list: ['重头再来', '向天再借五百年', '自由飞翔', '月亮之上']
      },
      {
        title: '电影',
        list: ['无间道', '肖申克的救赎', '电锯惊魂', '贞子']
      },
      {
        title: '明星',
        list: ['陈冠希', '吴彦祖', '赵本山', '范德彪']
      }
    ],
    curIndex: 0
  }

  render() {
    const { tablist, curIndex } = this.state
    return (
      <div className='app'>
        <h1>标题</h1>
        <nav>
          {tablist.map((item, index) =>
            <span
              className={curIndex === index ? 'active' : ''}
              key={item.title}
              onClick={() => {
                this.setState({ curIndex: index })
              }}
            >{item.title}</span>
          )}
        </nav>
        <ul>
          {tablist[curIndex].list.map(item =>
            <li key={item}>{item}</li>
          )}
        </ul>
      </div>
    )
  }
}

export default App
