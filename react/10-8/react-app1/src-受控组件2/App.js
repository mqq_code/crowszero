import React, { Component } from 'react'
import './App.scss'

class App extends Component {

  state = {
    form: {
      user: '王小明',
      age: 20,
      hobby: '通宵',
      sex: 0,
      married: true
    }
  }

  changeForm = (key, val) => {
    this.setState({
      form: {
        ...this.state.form,
        [key]: val
      }
    })
  }

  submit = () => {
    console.log(this.state.form)
  }
  render() {
    const { form } = this.state
    return (
      <div className='app'>
        <h1>受控组件</h1>
        <div>姓名: <input type="text" value={form.user} onChange={e => this.changeForm('user', e.target.value)} /></div>
        <div>年龄: <input type="text" value={form.age} onChange={e => this.changeForm('age', e.target.value)} /></div>
        <div>爱好: <input type="text" value={form.hobby} onChange={e => this.changeForm('hobby', e.target.value)} /></div>
        <div>性别: <input type="text" value={form.sex} onChange={e => this.changeForm('sex', e.target.value)} /></div>
        <div>婚否: 
          <input id='sex' type="checkbox" checked={form.married} onChange={e => this.changeForm('married', e.target.checked)} />
          <label htmlFor='sex'>{form.married ? '已婚' : '未婚'}</label>
        </div>
        <button onClick={this.submit}>提交</button>
      </div>
    )
  }
}

export default App
