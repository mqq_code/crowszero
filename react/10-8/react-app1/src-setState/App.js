import React, { Component } from 'react'
import './App.css'

// 类组件
class App extends Component {

  // 存组件数据，类似vue中的data
  state = {
    num: 0,
    title: 'class组件'
  }

  changeCount = (n) => {
    // 必须调用 setState 手动更新页面，会把传入对象合并到之前的state
    // this.setState({
    //   num: this.state.num + n
    // })

    // setState 是异步操作，连续多次调用会合并到一起执行，获取不到最新的state
    // this.setState({ num: this.state.num + 4 })
    // this.setState({ num: this.state.num + 3 })
    // this.setState({ num: this.state.num + 2 })
    // this.setState({ num: this.state.num + 5 })


    // 可以获取最新的state,会依次执行函数,把函数的返回值合并到之前的state中
    this.setState(state => ({ num: state.num + 1}))
    this.setState(state => ({ num: state.num + 1}))
    this.setState(state => ({ num: state.num + 1}))
    this.setState(state => {
      console.log('最新的state', state)
      return { num: state.num + 1}
    }, () => {
      // 数据更新后的回调函数，可以获取最新的数据
      console.log(this.state.num)
    })

    // this.setState({ num: this.state.num + 1 }, () => {
    //   // 数据更新后的回调函数，可以获取最新的数据
    //   console.log(this.state.num)
    // })
    // this.setState([object, fn], callback)

  }

  // 必须存在，返回react元素
  render() {
    const { num, title } = this.state
    console.log('渲染页面')

    return (
      <div>
        <h1>{ title }</h1>
        {num > 0 && <>
          <button onClick={() => this.changeCount(-1)}>-</button>
          <span>{num}</span>
        </>}
        <button onClick={() => this.changeCount(1)}>+</button>
      </div>
    )
  }
}


// 函数组件
// const App = () => {
//   return (
//     <div>
//       <h1>这是函数组件</h1>
//     </div>
//   )
// }

export default App
