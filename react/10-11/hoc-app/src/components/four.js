import React, { Component } from 'react'

export default class four extends Component {

  state = {
    tableData: [
      { name: '购物', type: '星期三', id: 1 },
      { name: '加班', type: '星期三', id: 2 },
      { name: '做饭', type: '星期二', id: 3 },
      { name: '爬山', type: '星期二', id: 4 },
      { name: '打游戏', type: '星期三', id: 5 },
      { name: '看电影', type: '星期一', id: 6 }
    ]
  }
  week = {
    '星期一': 0,
    '星期二': 1,
    '星期三': 2,
  }
  componentDidMount() {
    const tableData = this.state.tableData.map(item => {
      return {
        ...item,
        // 添加排序标识
        rowKey: this.week[item.type],
        // 计算有几个相同的数据
        rowSpan: this.state.tableData.filter(v => v.type === item.type).length
      }
    })

    tableData.sort((a, b) => {
      return a.rowKey - b.rowKey
    })

    console.log(tableData)
    this.setState({ tableData })
  }

  render() {
    const { tableData } = this.state
    return (
      <div>
        <table border="1" width="300" style={{ borderCollapse: 'collapse', textAlign: 'center' }}>
          <thead>
            <tr>
              <th>序号</th>
              <th>事项</th>
              <th>星期</th>
            </tr>
          </thead>
          <tbody>
            {tableData.map((item, index) =>
              <tr key={item.id}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                {index === 0 || item.type !== tableData[index - 1].type ?
                  <td rowSpan={item.rowSpan}>{item.type}</td>
                : null}
              </tr>
            )}
          </tbody>
        </table>
      </div>
    )
  }
}
