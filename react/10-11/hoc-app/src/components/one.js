import React, { Component } from 'react'

export default class one extends Component {
  state = {
    curIndex: 0
  }
  navlist = ['tab1', 'tab2', 'tab3', 'tab4']

  render() {
    const { curIndex } = this.state
    return (
      <div className='one'>
        <div className="menu">
          {this.navlist.map((item, index) =>
            <p className={curIndex === index ? 'active' : ''} onClick={() => {
              this.setState({ curIndex: index })
            }} key={item}><span>{item}</span></p>
          )}
        </div>
      </div>
    )
  }
}
