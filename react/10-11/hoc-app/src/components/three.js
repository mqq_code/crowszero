import React, { Component, createRef } from 'react'
import data from '../data/china.json'
import * as echarts from 'echarts';

export default class three extends Component {
  chartWrap = createRef()

  componentDidMount() {
    // 基于准备好的dom，初始化echarts实例
    const myChart = echarts.init(this.chartWrap.current);

    echarts.registerMap('China', data);

    const option = {
      title: {
        text: '中国地图',
        left: 'right'
      },
      tooltip: {
        trigger: 'item',
        showDelay: 0,
        transitionDuration: 0.2
      },
      series: [
        {
          name: '中国地图',
          type: 'map',
          roam: true,
          map: 'China',
          emphasis: {
            label: {
              show: true
            }
          }
        }
      ]
    };
    myChart.setOption(option);

  }
  render() {
    return (
      <div ref={this.chartWrap} style={{ height: '100%', background: '#ccc' }}>three</div>
    )
  }
}
