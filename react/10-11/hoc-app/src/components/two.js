import React, { Component, createRef } from 'react'
import * as echarts from 'echarts';

export default class two extends Component {
  chartWrap = createRef()

  barData = [
    // 接口返回数据、不能在已有的数据上增加其余月份，其余月份在逻辑处理时增加
    // sjyj 实际移交、jhyj 计划移交、wcl 完成率、month月份
    { month: '四', sjyj: 50, jhyj: 80, wcl: 79 }, 
    { month: '六', sjyj: 67, jhyj: 75, wcl: 36 },
    { month: '七', sjyj: 23, jhyj: 54, wcl: 69 },
    { month: '三', sjyj: 34, jhyj: 53, wcl: 64 },
    { month: '八', sjyj: 12, jhyj: 43, wcl: 13 },
    { month: '九', sjyj: 36, jhyj: 27, wcl: 82 }
  ]
  componentDidMount() {
    // 基于准备好的dom，初始化echarts实例
    const myChart = echarts.init(this.chartWrap.current);
    const colors = ['#5470C6', '#91CC75', '#EE6666'];
    // x轴的坐标
    const xAxisData = ['一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '十一', '十二']
    // 数据
    let series = [
      {
        name: '实际移交',
        type: 'bar',
        yAxisIndex: 0, // 使用第一个y轴
        data: [],
        key: 'sjyj' // 渲染barData中的哪一个属性
      },
      {
        name: '计划移交',
        type: 'bar',
        yAxisIndex: 0, // 使用第一个y轴
        data: [],
        key: 'jhyj'
      },
      {
        name: '完成率',
        type: 'line',
        yAxisIndex: 1, // 使用第二个y轴
        data: [],
        key: 'wcl'
      }
    ]
    // 根据接口的返回值生成数据
    series = series.map(item => {
      return {
        ...item,
        data: xAxisData.map(v => {
          // 根据月份去barData中查找数据
          let value = this.barData.find(val => val.month === v)
          return value ? value[item.key] : 0
        })
      }
    })

    const option = {
      color: colors,
      // 设置图标位置偏移量
      grid: {
        right: '10%',
        left: '10%'
      },
      // 图例
      legend: {
        data: ['实际移交', '计划移交', '完成率'],
        textStyle: {
          color: '#fff'
        }
      },
      // 设置x轴
      xAxis: [
        {
          type: 'category',
          axisTick: {
            alignWithLabel: true
          },
          axisLine: {
            lineStyle: {
              color: '#fff'
            }
          },
          // prettier-ignore
          data: xAxisData
        }
      ],
      // 设置y轴
      yAxis: [
        {
          type: 'value',
          name: '单位：批',
          position: 'left',
          alignTicks: true,
          axisLine: {
            show: true,
            lineStyle: {
              color: '#fff'
            }
          },
          splitLine: false
        },
        {
          type: 'value',
          name: '完成率',
          position: 'right',
          alignTicks: true,
          min: 0,
          max: 100,
          axisLine: {
            show: true,
            lineStyle: {
              color: '#fff'
            }
          },
          axisLabel: {
            formatter: '{value}%'
          },
          splitLine: false
        }
      ],
      // 设置数据
      series
    };

    // 绘制图表
    myChart.setOption(option);
  }

  render() {
    return (
      <div style={{ height: '100%', background: '#10274f' }} ref={this.chartWrap}></div>
    )
  }
}
