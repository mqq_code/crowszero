import React, { Component } from 'react'
import './App.css'
import One from './components/one'
import Two from './components/two'
import Three from './components/three'
import Four from './components/four'
import axios from 'axios'


axios.get('/list').then(res => {
  console.log(res.data)
})

class App extends Component {

  state = {
    curIndex: 2
  }

  navlist = [
    {
      title: '功能1',
      component: One
    },
    {
      title: '功能2',
      component: Two
    },
    {
      title: '功能3',
      component: Three
    },
    {
      title: '功能4',
      component: Four
    }
  ]

  curContent = () => {
    const Content = this.navlist[this.state.curIndex].component
    return <Content />
  }

  render() {
    const { curIndex } = this.state
    
    return (
      <div>
        <nav>
          {this.navlist.map((item, index) =>
            <span className={curIndex === index ? 'active' : ''} onClick={() => {
              this.setState({ curIndex: index })
            }} key={item.title}>{item.title}</span>
          )}
        </nav>
        <main>
          {this.curContent()}
        </main>
      </div>
    )
  }
}

export default App
