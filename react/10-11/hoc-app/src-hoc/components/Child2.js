import React, { Component } from 'react'
import date from '../hoc/date'

class Child2 extends Component {
  state = {
    num: 0,
  }

  render() {
    return (
      <div>
        <h2>组件2</h2>
        <p>{this.props.date.toLocaleString()}</p>
        <button>-</button>
        {this.state.num}
        <button>+</button>
      </div>
    )
  }
}

export default date(Child2)
