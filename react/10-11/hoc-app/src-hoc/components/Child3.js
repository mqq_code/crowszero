import React, { Component } from 'react'
import date from '../hoc/date'

class Child3 extends Component {
 
  render() {
    return (
      <div>
        <h2>组件3 {this.props.date.toLocaleString()}</h2>
        <img src="https://p.qqan.com/up/2020-9/16002207525372389.jpg" alt="" />
      </div>
    )
  }
}

// 高阶组件：给组件添加额外的功能
export default date(Child3)