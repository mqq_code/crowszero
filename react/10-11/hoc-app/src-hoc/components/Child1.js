import React, { Component } from 'react'
import date from '../hoc/date'

class Child1 extends Component {

  state = {
    arr: [1,2,3,4,5,6,7],
  }

  render() {
    console.log(this.props)
    return (
      <div style={{ background: 'red' }}>
        <h1>组件1 -- {this.props.title}</h1>
        <p>当前日期: {this.props.date.toLocaleString()}</p>
        <ul>
          {this.state.arr.map(item => <li key={item}>{item}</li>)}
        </ul>
      </div>
    )
  }
}

export default date(Child1)
