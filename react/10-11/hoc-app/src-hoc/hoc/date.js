import React, { Component } from 'react'

export default function (Com) {

  class date extends Component {

    state = {
      date: new Date()
    }
    timer = null
    componentDidMount() {
      this.timer = setInterval(() => {
        this.setState({
          date: new Date()
        })
      }, 1000)
    }
  
    componentWillUnmount() {
      clearInterval(this.timer)
    }


    render() {
      return (
        <Com {...this.props} date={this.state.date} />
      )
    }
  }

  return date
}
