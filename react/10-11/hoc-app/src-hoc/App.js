import React, { Component } from 'react'
import './App.css'
import Child1 from './components/Child1'
import Child2 from './components/Child2'
import Child3 from './components/Child3'

class App extends Component {

  render() {

    let obj = {
      name: '小明',
      age: 20,
      sex: '男'
    }

    return (
      <div>
        <h1>高阶组件</h1>
        <Child1 title="第一个组件" {...obj} />
        <hr />
        <Child2 />
        <hr />
        <Child3 />
      </div>
    )
  }
}

export default App