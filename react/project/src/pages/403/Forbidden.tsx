import { Button, Result } from 'antd';
import React from 'react';
import { Link } from 'umi'

const Forbidden: React.FC = () => (
  <Result
    status="403"
    title="403"
    subTitle="没有此页面访问权限"
    extra={<Link to="/"><Button type="primary">去首页</Button></Link>}
  />
);


export default Forbidden