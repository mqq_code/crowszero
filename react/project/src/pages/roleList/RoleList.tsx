import { getRoleList } from '@/services'
import TableList from '@/components/TableList';

const RoleList = () => {
  const columns: any =[
    {
      title: '角色id',
      dataIndex: 'id',
      key: 'id'
    },
    {
      title: '角色key',
      dataIndex: 'role',
      key: 'role'
    },
    {
      title: '角色名称',
      dataIndex: 'roleName',
      key: 'roleName'
    },
    {
      title: '角色描述',
      dataIndex: 'note',
      key: 'note'
    },
    {
      title: '操作',
      key: 'action',
      render: () => <button>按钮</button>
    },
  ]

  return (
    <div>
      <TableList
        columns={columns}
        getList={getRoleList}
        query={{ pagesize: 2 }}
        tableProps={{
          rowKey: 'id'
        }}
      />
    </div>
  )
}

export default RoleList