import React, { useState } from 'react'
import { DownOutlined, UpOutlined } from '@ant-design/icons'
import { Button, Row, Col, Form, Input, Space, Select } from 'antd';
const { Option } = Select

interface IProps {
  onSearch: (params: any) => void;
  onReset: () => void;
}

const Search: React.FC<IProps> = (props) => {
  const [show, setShow] = useState(false)
  const [form] = Form.useForm()
  return (
    <div>
      <Form form={form} labelCol={{ span: 6 }}>
        <Row gutter={20}>
          <Col span={8}>
            <Form.Item label="用户名/账号" name="account">
              <Input placeholder='请输入用户名/账号' />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="启用状态" name="status">
              <Select placeholder="选择启用状态">
                <Option value="">全部</Option>
                <Option value={0}>禁用</Option>
                <Option value={1}>启用</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="邮箱" name="email" rules={[{ type: "email", message: '邮箱格式错误' }]}>
              <Input placeholder='请输入邮箱' />
            </Form.Item>
          </Col>
        </Row>
        {show ?
          <>
            <Row gutter={20}>
              <Col span={8}>
                <Form.Item label="创建人" name="adduser">
                  <Input placeholder='请输入创建人' />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="测试" name="username">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="测试" name="username">
                  <Input />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={20}>
              <Col span={8}>
                <Form.Item label="测试" name="username">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="测试" name="username">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="测试" name="username">
                  <Input />
                </Form.Item>
              </Col>
            </Row>
          </>
        : null}
      </Form>
      <Space>
        <Button type="primary" onClick={() => {
          form.validateFields().then(res => {
            props.onSearch(res)
          })
        }}>搜索</Button>
        <Button onClick={() => {
          form.resetFields()
          props.onReset()
        }}>重置</Button>
        <Button type="link" onClick={() => setShow(!show)}>
          {show ? <>收起<UpOutlined /></> : <>展开<DownOutlined /></>}
        </Button>
      </Space>
    </div>
  )
}

export default Search