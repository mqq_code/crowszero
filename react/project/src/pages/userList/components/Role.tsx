import { Modal, Select } from 'antd'
import { getRoleList } from '@/services'
import { useEffect, useState } from 'react';

interface IProps {
  open: boolean;
  close: () => void;
  onOk: (roleList: string[]) => void;
  role: string[]
}

const Role: React.FC<IProps> = (props) => {
  const [options, setOptions] = useState([])
  const [values, setValues] = useState<string[]>([])
  useEffect(() => {
    getRoleList().then(res => {
      console.log(res.values.list)
      setOptions(res.values.list)
    })
  }, [])
  return (
    <Modal
      title="分配角色"
      open={props.open}
      onCancel={props.close}
      onOk={() => props.onOk(values)}
      cancelText="取消"
      okText="确定"
      destroyOnClose
    >
      <Select
        mode="multiple"
        allowClear
        style={{ width: '100%' }}
        placeholder="请选择角色"
        fieldNames={{ label: 'roleName', value: 'role' }}
        options={options}
        defaultValue={props.role}
        onChange={value => {
          setValues(value)
        }}
      />
    </Modal>
  )
}

export default Role