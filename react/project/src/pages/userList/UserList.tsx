import { Space, Table, Button, Switch, Popconfirm, message, Modal, Select, Form, Input } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { getUserList, delUser, editUser, addUser } from '@/services'
import { useEffect, useState, useRef } from 'react';
import moment from 'moment';
import Role from './components/Role';
import Search from './components/Search';
import TableList from '@/components/TableList';
const { Option } = Select;

const UserList = () => {
  const tabIns = useRef<any>(null)
  const [loading, setLoading] = useState(false)
  const [isModalOpen, setModalOpen] = useState(false)
  const [isRoleOpen, setRoleOpen] = useState(false)
  // 编辑的数据
  const [editInfo, setEditInfo] = useState<any>({ role: [] })
  // 获取表单实例对象
  const [form] = Form.useForm()

  // 删除数据
  const confirmDel = async (uid: string) => {
    try {
      setLoading(true)
      const res = await delUser(uid)
      if (res.code === 200) {
        message.success('删除成功')
        await tabIns.current?.getList()
        setLoading(false)
      } else {
        message.error(res.msg)
      }
    } catch (e) {
      console.log(e)
    }
  }

  // 编辑数据 
  const edit = async (params: any) => {
    setLoading(true)
    const res = await editUser({
      uid: editInfo.uid,
      ...params
    })
    if (res.code === 200) {
      message.success('修改成功')
      await tabIns.current?.getList()
      setLoading(false)
    } else {
      message.error(res.msg)
    }
  }

  // 表格中列的数据
  const columns: ColumnsType<any> = [
    {
      title: '账号',
      dataIndex: 'account',
      key: 'account',
      fixed: 'left',
      width: 100
    },
    {
      title: '姓名',
      dataIndex: 'username',
      key: 'username',
      width: 100
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: '创建人',
      dataIndex: 'adduser',
      key: 'adduser',
      width: 100
    },
    {
      title: '账号状态',
      dataIndex: 'status',
      key: 'status',
      width: 150,
      render: (_, record) => {
        return <Switch disabled={record.uid === -1} checked={_} onChange={(checked) => edit({ uid: record.uid, status: Number(checked) })} />
      }
    },
    {
      title: '创建时间',
      dataIndex: 'addTime',
      key: 'addTime',
      width: 200,
      render: (_) => {
        return _ ? moment(_).format('YYYY-MM-DD hh:mm:ss') : '-'
      }
    },
    {
      title: '最后时间',
      dataIndex: 'lastOnlineTime',
      key: 'lastOnlineTime',
      width: 200,
      render: (_) => {
        return _ ? moment(_).format('YYYY-MM-DD hh:mm:ss') : '_'
      }
    },
    {
      title: '操作',
      key: 'action',
      fixed: 'right',
      render: (_, record) => {
        return (
          <Space size="middle">
            <Button disabled={record.uid === -1} type="primary" size="small" onClick={() => {
              setEditInfo(record)
              setRoleOpen(true)
            }}>分配角色</Button>
            <Button disabled={record.uid === -1} size="small" onClick={() => {
              setEditInfo(record)
              setModalOpen(true)
              // 点击编辑弹窗内反显数据
              form.setFieldsValue({...record})
            }}>编辑</Button>
            <Popconfirm
              disabled={record.uid === -1}
              title={<p>确定删除 <b style={{ color: 'tomato' }}>{record.account}</b> 吗?</p>}
              placement="topRight"
              onConfirm={() => confirmDel(record.uid)}
              okButtonProps={{ danger: true }}
              okText="确定删除"
              cancelText="取消"
            >
              <Button disabled={record.uid === -1} size="small" danger type="primary">删除</Button>
            </Popconfirm>
          </Space>
        )
      },
    },
  ];

  // 添加用户
  const addUserFn = async (values: any) => {
    setLoading(true)
    const res = await addUser(values)
    if (res.code === 200) {
      message.success('添加成功')
      await tabIns.current?.getList()
      setLoading(false)
    } else {
      message.error(res.msg)
    }
  }

  // 新增和编辑弹窗的确定按钮
  const handleOk = async () => {
    // 触发表单校验
    try {
      const values = await form.validateFields()
      if (editInfo.uid) {
        edit(values)
        setModalOpen(false)
      } else {
        addUserFn(values)
      }
    } catch (e) {
      console.log(e)
    }
  }
  // 新增和编辑弹窗的取消按钮
  const handleCancel = () => {
    setModalOpen(false)
  }
  useEffect(() => {
    if (!isModalOpen) {
      //没数据就表示是新增
      form.resetFields()
      // 关闭弹窗是清空编辑的数据
      setEditInfo({})
    }
  }, [isModalOpen])

  return (
    <div>
      <Search
        onSearch={(values) => {
          tabIns.current?.setQuery((query: any) => ({
            ...query,
            ...values,
            page: 1
          }))
        }}
        onReset={() => {
          tabIns.current?.setQuery((query: any) => ({
            page: query.page,
            pagesize: query.pagesize
          }))
        }}
      />
      <Button style={{ margin: '20px 0' }} type="primary" onClick={() => setModalOpen(true)}>新增用户</Button>
      <TableList
        columns={columns}
        getList={getUserList}
        tableProps={{
          scroll: { x: 1300 },
          loading,
          rowKey: 'uid'
        }}
        ref={tabIns}
      />
      <Modal
        title={editInfo.uid ? '编辑用户' : '新增用户'}
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        cancelText="取消"
        okText="确定"
      >
        <Form form={form} labelCol={{ span: 4 }} labelAlign="left">
          <Form.Item
            label="账号"
            name="account"
            rules={[{ required: true, message: '请输入账号!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="用户名"
            name="username"
            rules={[{ required: true, message: '请输入用户名!' }]}
          >
            <Input />
          </Form.Item>
          {!editInfo.uid ?
            <Form.Item
              label="密码"
              name="password"
              rules={[{ required: true, message: '请输入密码!' }]}
            >
              <Input.Password />
            </Form.Item>  
          : null}
          <Form.Item
            label="邮箱"
            name="email"
            rules={[
              { required: true, message: '请输入邮箱!' },
              { type: 'email', message: '邮箱格式错误!' }
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="启用状态"
            name="status"
            rules={[
              { required: true, message: '请选择启用状态!' }
            ]}
          >
            <Select placeholder="请选择启用状态">
              <Option value={1}>启用</Option>
              <Option value={0}>禁用</Option>
            </Select>
          </Form.Item>
        </Form>
      </Modal>
      <Role
        open={isRoleOpen}
        role={editInfo.role}
        close={() => setRoleOpen(false)}
        onOk={(role) => {
          edit({ role })
          setRoleOpen(false)
        }}
      />
    </div>
  )
}

export default UserList