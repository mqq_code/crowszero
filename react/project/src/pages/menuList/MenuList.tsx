import { getMenulist } from '@/services'
import TableList from '@/components/TableList';

const MenuList = () => {
  const columns: any =[
    {
      title: '菜单id',
      dataIndex: 'id',
      key: 'id'
    },
    {
      title: '菜单路径',
      dataIndex: 'path',
      key: 'path'
    },
    {
      title: '菜单图标',
      dataIndex: 'icon',
      key: 'icon'
    },
    {
      title: '菜单标题',
      dataIndex: 'title',
      key: 'title'
    }
  ]

  return (
    <div>
      <TableList
        columns={columns}
        getList={getMenulist}
        query={{ pagesize: 2 }}
        tableProps={{
          rowKey: 'id'
        }}
      />
    </div>
  )
}

export default MenuList