import { Button, Tag, Modal, message } from 'antd';
import { getArticleList, getArticleDetail } from '@/services'
import moment from 'moment';
import TableList from '@/components/TableList'
import { useState } from 'react';
import 'braft-editor/dist/output.css'

const ArticleList = () => {
  const [isOpen, setOpen] = useState(false)
  const [detail, setDetail] = useState<any>({})
  const columns: any =[
    {
      title: '标题',
      dataIndex: 'title',
      key: 'title'
    },
    {
      title: '文章标签',
      dataIndex: 'keyCode',
      key: 'keyCode',
      render: (keyCode: any[]) => {
        return keyCode.map(item => {
          return <Tag key={item.keyCode} color={item.color}>{item.title}</Tag>
        })
      }
    },
    {
      title: '简介',
      dataIndex: 'desc',
      key: 'desc'
    },
    {
      title: '创建人',
      dataIndex: 'author',
      key: 'author'
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      key: 'createTime',
      render: (_: any) => moment(_).format('YYYY-MM-DD hh:mm:ss')
    },
    {
      title: '操作',
      key: 'action',
      render: (_: any, record: any) => <Button onClick={async () => {
        const res = await getArticleDetail(record.id)
        if (res.code === 200) {
          console.log(res)
          setDetail(res.values)
          setOpen(true)
        } else {
          message.error(res.msg)
        }
      }}>预览</Button>
    }
  ]

  return (
    <div>
      <TableList
        columns={columns}
        getList={getArticleList}
        query={{ pagesize: 3 }}
        tableProps={{
          rowKey: 'id'
        }}
      />
      <Modal
        title={detail.title}
        open={isOpen}
        onCancel={() => setOpen(false)}
        footer={null}
        width={600}
      >
        <p>作者：{detail.author}</p>
        <p>发布时间: {detail.createTime}</p>
        <p>标签：{detail.keyCode?.map((item: any) => <Tag key={item.keyCode} color={item.color}>{item.title}</Tag>)}</p>
        <div className='braft-output-content' dangerouslySetInnerHTML={{ __html: detail.content }}></div>
      </Modal>
    </div>
  )
}

export default ArticleList