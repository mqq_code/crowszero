import { Button, Result } from 'antd';
import React from 'react';
import { Link } from 'umi'

const Notfound: React.FC = () => (
  <Result
    status="404"
    title="404"
    subTitle="找不到页面地址"
    extra={<Link to="/"><Button type="primary">去首页</Button></Link>}
  />
);

export default Notfound