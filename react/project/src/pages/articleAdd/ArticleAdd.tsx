import { useEffect, useState } from 'react'
import 'braft-editor/dist/index.css'
import BraftEditor from 'braft-editor'
import { getArticleTypeList, addArticle } from '@/services'
import { Button, Select, Form, Input, message } from 'antd';
import { history } from 'umi'
const { Option } = Select

const ArticleAdd = () => {
  const [options, setOptions] = useState<any[]>([])
  const [editorState, setEditorState] = useState(BraftEditor.createEditorState(null))
  useEffect(() => {
    getArticleTypeList().then(res => {
      setOptions(res.values.list)
    })
  }, [])
  const onFinish = async (values: any) => {
    // 获取表单数据，调用接口
    const { content, ...params } = values
    const res = await addArticle({
      ...params,
      content: content.toHTML()
    })
    if (res.code === 200) {
      message.success('提交成功')
      history.push('/article/list')
    }
  }
  return (
    <div style={{ background: '#fff', padding: '20px' }}>
      <Form layout="vertical" wrapperCol={{ span: 12 }} onFinish={onFinish}>
        <Form.Item
          label="文章标题"
          name="title"
          rules={[{ required: true, message: '请输入标题！' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="文章标签"
          name="keyCode"
          rules={[{ required: true, message: '请选择标签！' }]}
        >
          <Select mode="multiple" allowClear>
            {options.map((item, index) => <Option key={index} value={item.keyCode}>{item.title}</Option>)}
          </Select>
        </Form.Item>

        <Form.Item
          label="文章简介"
          name="desc"
          rules={[{ required: true, message: '请输入简介！' }]}
        >
          <Input.TextArea />
        </Form.Item>

        <Form.Item
          label="正文"
          name="content"
          wrapperCol={{ span: 24 }}
          rules={[
            () => ({
              validator (_, value) {
                if (value && !value.isEmpty()) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error('请输入正文'));
              }
            })
          ]}
        >
          <BraftEditor style={{ border: '1px solid #d9d9d9' }} value={editorState} onChange={setEditorState} />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">Submit</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default ArticleAdd