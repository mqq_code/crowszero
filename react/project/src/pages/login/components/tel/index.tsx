import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input, Row, Col } from 'antd';

const Tel: React.FC = () => {
  const onFinish = (values: any) => {
    console.log('Received values of form: ', values);
  };
  return (
    <Form
      name="normal_login"
      initialValues={{ remember: true }}
      onFinish={onFinish}
    >
      <Form.Item
        name="tel"
        rules={[
          { required: true, message: '请输入手机号!' },
          { pattern: /^1[3-9]\d{9}$/, message: '手机号格式错误!' }
        ]}
      >
        <Input prefix={<UserOutlined />} type="tel" placeholder="手机号" />
      </Form.Item>

      <Form.Item
        name="code"
        rules={[{ required: true, message: '请输入密码!' }]}
      >
        <Row gutter={15}>
          <Col span={14}><Input prefix={<LockOutlined />} placeholder="验证码"/></Col>
          <Col span={10}><Button block>获取验证码</Button></Col>
        </Row>
      </Form.Item>

      <Form.Item>
        <Button block type="primary" htmlType="submit">登陆</Button>
      </Form.Item>
    </Form>
  )
}

export default Tel