import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input, message } from 'antd';
import style from './user.module.scss'
import { useHistory } from 'umi';
import { login } from '@/services'
interface IForm {
  username: string;
  password: string;
  remember: boolean;
}


const User: React.FC = () => {
  const history = useHistory()
  const onFinish = async (values: IForm) => {
    const { remember, ...params } = values
    try {
      const res = await login(params)
      if (res.code === 200) {
        message.success('登陆成功')
        localStorage.setItem('tt-token', res.values.token)
        if (remember) {
          localStorage.setItem('username', values.username)
        } else {
          localStorage.removeItem('username')
        }
        const redirect = history.location.query.redirect
        history.push(redirect || '/')
      } else {
        message.error(res.msg)
      }
    } catch (e) {
      console.log(e)
    }
  };

  return (
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{
        remember: localStorage.getItem('username'),
        username: localStorage.getItem('username')
      }}
      onFinish={onFinish}
    >
      <Form.Item
        name="username"
        rules={[{ required: true, message: '请输入用户名!' }]}
      >
        <Input prefix={<UserOutlined />} placeholder="用户名/账号" />
      </Form.Item>

      <Form.Item
        name="password"
        rules={[{ required: true, message: '请输入密码!' }]}
      >
        <Input
          prefix={<LockOutlined />}
          type="password"
          placeholder="密码"
        />
      </Form.Item>

      <Form.Item>
        <div className={style.row}>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>记住账号</Checkbox>
          </Form.Item>
          <a className="login-form-forgot" href="">忘记密码？</a>
        </div>
      </Form.Item>

      <Form.Item>
        <Button block type="primary" htmlType="submit">登陆</Button>
      </Form.Item>
    </Form>
  )
}

export default User