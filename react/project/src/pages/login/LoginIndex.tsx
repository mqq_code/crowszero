import { Tabs } from 'antd'
import style from './login.module.scss'
import Tel from './components/tel'
import User from './components/user'
const LoginIndex: React.FC = () => {

  const tabs = [
    {
      label: '账号登陆',
      key: '1',
      children: <User />,
    },
    {
      label: '手机号登陆',
      key: '2',
      children: <Tel />,
    }
  ]

  return (
    <div className={style.login}>
      <div className={style.content}>
        <div className={style.top}>
          <div className={style.logo}>
            <img src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg" alt="" />
            <b>管理系统</b>
          </div>
          <p>这是一个神奇的系统这是一个神奇的系统</p>
        </div>
        <div className={style.form}>
          <Tabs
            defaultActiveKey="1"
            centered
            items={tabs}
          />
        </div>
      </div>
    </div>
  )
}

export default LoginIndex