import axios from 'axios'
import { message } from 'antd'
import { history } from 'umi'

const baseURL = 'http://121.89.213.194:5000'

// 创建axios实例对象
const fetch = axios.create({
  baseURL,
  timeout: 3000
});

// 添加请求拦截器，给所有的请求添加公共参数，例如 token
fetch.interceptors.request.use(function (config: any) {
  config.headers.Authorization = localStorage.getItem('tt-token')
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
fetch.interceptors.response.use(function (response) {
  return response.data;
}, function (error) {
  // 处理公共错误
  // 401 登陆信息失效自动跳转登陆
  // 403 没有全选自动跳转 403 页面
  if (error.response?.status === 401) {
    message.error('登陆信息失效，请重新登陆')
    history.push('/login?redirect=' + encodeURIComponent(history.location.pathname))
  } else if (error.response?.status === 403) {
    message.error('没有权限')
    history.push('/403')
  } else {
    message.error(error.message)
  }
  return Promise.reject(error);
});

export default fetch
