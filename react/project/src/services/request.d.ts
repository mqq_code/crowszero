// 登陆接口
interface ILoginRequest {
  username: string;
  password: string;
}

interface ILoginResponse {
  code: number;
  msg: string;
  values: {
    token: string;
  }
}
// 用户信息返回值类型
interface IUserInfo {
  code: number;
  msg: string;
  values: {
    username: string;
    account: string;
    email: string;
    menulist: string[];
    role: string[];
  }
}
