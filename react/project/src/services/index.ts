import fetch from './fetch'

// 登陆接口
export const login = (params: ILoginRequest) => {
  return fetch.post<any, ILoginResponse, ILoginRequest>('/api/user/login', params)
}

// 获取用户信息接口
export const userInfo = () => {
  return fetch.post<any, IUserInfo>('/api/user/info')
}

// 获取用户列表
export const getUserList = (params: any = {}) => {
  return fetch.get<any, any>('/api/user/list', { params })
}

// 删除用户
export const delUser = (uid: string) => {
  return fetch.delete<any, any>('/api/user/remove', { params: { uid } })
}

// 编辑用户
export const editUser = (params = {}) => {
  return fetch.put<any, any>('/api/user/edit', params)
}

// 添加用户
export const addUser = (params = {}) => {
  return fetch.post<any, any>('/api/user/add', params)
}

// 获取角色列表
export const getRoleList = (params = {}) => {
  return fetch.get<any, any>('/api/role/list', { params })
}

// 获取菜单列表
export const getMenulist = (params = {}) => {
  return fetch.get<any, any>('/api/permissions/menulist', { params })
}

// 获取文章列表
export const getArticleList = (params = {}) => {
  return fetch.get<any, any>('/api/article/list', { params })
}

// 获取文章详情
export const getArticleDetail = (id: string) => {
  return fetch.get<any, any>('/api/article/detail', { params: { id } })
}

// 获取分类列表
export const getArticleTypeList = (params = {}) => {
  return fetch.get<any, any>('/api/article/type/list', { params })
}

// 添加文章
export const addArticle = (params = {}) => {
  return fetch.post<any, any>('/api/article/add', params)
}




