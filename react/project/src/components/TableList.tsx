import { Table } from 'antd';
import { useEffect, useState, forwardRef, useImperativeHandle } from 'react';

interface IProps {
  columns: any[];
  getList: (query: any) => Promise<any>;
  query?: { page?: number; pagesize?: number; };
  tableProps?: any;
}

const TableList: React.ForwardRefRenderFunction<{}, IProps> = (props, ref) => {
  
  const [data, setData] = useState([])
  const [total, setTotal] = useState(0)
  const [query, setQuery] = useState({
    page: props.query?.page || 1,
    pagesize: props.query?.pagesize || 5,
  })
  const getList = async () => {
    try {
      const res = await props.getList(query)
      setData(res.values.list)
      setTotal(res.values.total)
    } catch (e) {
      console.log(e)
    }
  }
  
  useEffect(() => {
    getList()
  }, [query])

  useImperativeHandle(ref, () => {
    return {
      setQuery,
      getList
    }
  }, [query])

  return (
    <div>
      <Table
        bordered
        columns={props.columns}
        dataSource={data}
        pagination={{
          current: query.page,
          pageSize: query.pagesize,
          total,
          onChange: (page, pagesize) => {
            setQuery({ ...query, page, pagesize })
          }
        }}
        {...props.tableProps}
      />
    </div>
  )
}

export default forwardRef(TableList)