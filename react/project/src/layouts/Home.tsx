import { Layout } from 'antd';
import React, { useEffect, useState } from 'react';
import { IRouteComponentProps, useDispatch } from 'umi'
import style from './home.module.scss'
import LeftMenu from './components/menu/LeftMenu';
import TopHeader from './components/header/TopHeader';

const { Content } = Layout;

const Home: React.FC<IRouteComponentProps> = (props) => {
  const [collapsed, setCollapsed] = useState(false);
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch({
      type: 'user/getUserInfo'
    })
  }, [])
  
  return (
    <Layout style={{ height: '100%' }}>
      <LeftMenu collapsed={collapsed} />
      <Layout>
        <TopHeader collapsed={collapsed} setCollapsed={setCollapsed} />
        <Content className={style['site-content']} >
          {props.children}
        </Content>
      </Layout>
    </Layout>
  );
};

export default Home