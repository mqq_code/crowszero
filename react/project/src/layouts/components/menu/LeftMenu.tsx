
import { Layout, Menu } from 'antd';
import style from '../../home.module.scss'
import { Link, useLocation, useSelector } from 'umi'
import menu from './menu'
import { useEffect, useState, useRef, useMemo } from 'react';

const { Sider } = Layout;

interface IProps {
  collapsed: boolean
}

const getDefaultOpenKeys = (key: string) => {
  const openkey = menu.find(item => {
    if (item.children) {
      return item.children.find(val => val.key === key)
    }
  })
  return openkey ? [openkey.key] : []
}

const LeftMenu: React.FC<IProps> = (props) => {
  const location = useLocation()
  const defaultOpenKeys = useMemo(() => getDefaultOpenKeys(location.pathname), [])
  const user = useSelector((state: any) => state.user)
  // 根据所有菜单和接口的菜单查找相同的数据
  const menulist = useMemo(() => {
    let arr: any = []
    menu.forEach(item => {
      if (user.menulist.includes(item.key)) {
        arr.push(item)
      }
    })
    return arr
  }, [user.menulist])
  return (
    <Sider trigger={null} collapsible collapsed={props.collapsed}>
      <Link to="/" className={style.logo}>
        <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="5174" width="48" height="48"><path d="M248.5 300.9c145 74.3 268.8 69 268.8 69s-46-162.7 97.3-235.2c143.2-72.5 244 49.5 244 49.5s24.8-7.1 44.2-14.1c19.5-7.1 46-19.5 46-19.5l-44.2 79.6 69-7.1s-8.8 12.4-35.4 37.1c-26.5 24.8-38.9 38.9-38.9 38.9s10.6 196.3-93.7 348.4C703 799.6 568.6 889.8 375.8 909.3 183 928.7 55.7 849.2 55.7 849.2s84.9-5.3 137.9-24.8c53.1-21.2 130.9-76 130.9-76s-109.6-33.6-150.3-72.5C133.5 637 124.7 614 124.7 614l109.6-1.8S119.4 550.3 87.5 502.6c-31.7-47.8-37-95.6-37-95.6l83.1 33.6s-69-95.5-79.6-168c-10.6-74.3 12.4-113.2 12.4-113.2s37.1 67.3 182.1 141.5z m0 0" p-id="5175" fill="#1890ff"></path></svg>
      </Link>
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={[location.pathname]}
        defaultOpenKeys={defaultOpenKeys}
        items={menulist}
      />
    </Sider>
  )
}

export default LeftMenu