import {
  UserOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons';
import { Link } from 'umi'

const menu = [
  {
    key: '/dashboard',
    icon: <UserOutlined />,
    label: <Link to="/dashboard">监控页</Link>
  },
  {
    key: '/article',
    icon: <VideoCameraOutlined />,
    label: '文章管理',
    children: [
      {
        key: '/article/list',
        icon: <UserOutlined />,
        label: <Link to="/article/list">文章列表</Link>
      },
      {
        key: '/article/add',
        icon: <UserOutlined />,
        label:<Link to="/article/add">添加文章</Link>
      }
    ]
  },
  {
    key: '/classify',
    icon: <VideoCameraOutlined />,
    label: '分类管理',
    children: [
      {
        key: '/classify/list',
        icon: <UserOutlined />,
        label: <Link to="/classify/list">分类列表</Link>
      },
      {
        key: '/classify/add',
        icon: <UserOutlined />,
        label:<Link to="/classify/add">添加分类</Link>
      }
    ]
  },
  {
    key: '/auth',
    icon: <VideoCameraOutlined />,
    label: '权限管理',
    children: [
      {
        key: '/auth/userlist',
        icon: <UserOutlined />,
        label: <Link to="/auth/userlist">用户管理</Link>
      },
      {
        key: '/auth/menulist',
        icon: <UserOutlined />,
        label:<Link to="/auth/menulist">菜单管理</Link>
      },
      {
        key: '/auth/rolelist',
        icon: <UserOutlined />,
        label:<Link to="/auth/rolelist">角色管理</Link>
      }
    ]
  },
  {
    key: '/setting',
    icon: <VideoCameraOutlined />,
    label: '通用设置',
    children: [
      {
        key: '/setting/userinfo',
        icon: <UserOutlined />,
        label: <Link to="/setting/userinfo">个人中心</Link>
      },
      {
        key: '/setting/setuser',
        icon: <UserOutlined />,
        label:<Link to="/setting/setuser">个人设置</Link>
      }
    ]
  },
]

export default menu