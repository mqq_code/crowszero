import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  HomeOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Layout, Breadcrumb, Avatar, Dropdown, Menu, Space } from 'antd';
import style from '../../home.module.scss'
import menu from '../menu/menu';
import { useLocation, useSelector } from 'umi'
import { useEffect, useMemo } from 'react';

const { Header } = Layout;

interface IProps {
  collapsed: boolean;
  setCollapsed: (b: boolean) => void
}

const userMenu = (
  <Menu
    items={[
      {
        key: '1',
        label: '选项一',
      },
      {
        key: '2',
        label: '选项二',
      },
      {
        key: '3',
        label: '选项三',
      },
      {
        key: '4',
        label: '选项四',
      },
    ]}
  />
);

const TopHeader: React.FC<IProps> = (props) => {
  const location = useLocation()
  const breadcrumb = useMemo(() => {
    let arr: any[] = []
    menu.forEach(item => {
      if (item.key === location.pathname) {
        arr.push(item)
      } else {
        item.children?.forEach(val => {
          if (val.key === location.pathname) {
            arr = [item, val]
          }
        })
      }
    })
    return arr
  }, [location.pathname])
  const user = useSelector((state: any) => state.user)

  return (
    <Header className={style['site-header']}>
      <div className={style.left}>
        {props.collapsed ?
          <MenuUnfoldOutlined className={style.trigger} onClick={() => props.setCollapsed(!props.collapsed)} />
        :
          <MenuFoldOutlined  className={style.trigger} onClick={() => props.setCollapsed(!props.collapsed)} />
        }
        <Breadcrumb>
          {breadcrumb.map(item =>
            <Breadcrumb.Item key={item.key}>
              <Space>
                {item.icon}
                {item.label}
              </Space>
            </Breadcrumb.Item>
          )}
        </Breadcrumb>
      </div>
      <Space>
        <Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>{user.username}</Avatar>
        <Dropdown overlay={userMenu} arrow>
          <span>{user.username}</span>
        </Dropdown>
      </Space>
    </Header>
  )
}

export default TopHeader