import { Effect, ImmerReducer } from 'umi';
import { userInfo } from '@/services'

export interface UserModelState {
  username: string;
  account: string;
  email: string;
  menulist: string[];
  role: string[];
}

export interface UserModelType {
  namespace: 'user';
  state: UserModelState;
  effects: {
    getUserInfo: Effect;
  };
  reducers: {
    // 启用 immer 之后
    setUserInfo: ImmerReducer<UserModelState>;
  };
}

const UserModel: UserModelType = {
  namespace: 'user',

  state: {
    username: '',
    account: '',
    email: '',
    menulist: [],
    role: []
  },

  effects: {
    *getUserInfo(action, { call, put }) {
      try {
        const res = yield call(userInfo)
        yield put({
          type: 'setUserInfo',
          payload: res.values
        })
      } catch (e) {
        console.log(e)
      }
    },
  },
  reducers: {
    // 启用 immer 之后
    setUserInfo(state, action) {
      return {...state, ...action.payload}
    },
  }
};

export default UserModel;