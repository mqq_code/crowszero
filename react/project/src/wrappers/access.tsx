import Loading from '@/components/Loading';
import { IRouteComponentProps, useSelector, Redirect } from 'umi'

const Access: React.FC<IRouteComponentProps> = (props) => {
  const user = useSelector((state: any) => state.user)

  // 判断页面是否有访问权限
  if (user.menulist.length === 0) {
    return <Loading />
  }
  if (user.menulist.includes(props.location.pathname)) {
    return props.children
  }
  return <Redirect to="/403" />
}

export default Access

