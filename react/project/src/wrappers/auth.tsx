import { Redirect, IRouteComponentProps } from 'umi'

const Auth: React.FC<IRouteComponentProps> = (props) => {
  const token = localStorage.getItem('tt-token')
  if (token) {
    return props.children
  } else {
    return <Redirect to={{
      pathname: '/login',
      search: 'redirect=' + encodeURIComponent(props.location.pathname)
    }} />
  }
}

export default Auth