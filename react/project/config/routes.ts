import { defineConfig } from 'umi';

export default defineConfig({
  routes: [
    { title: '登陆', exact: true, path: '/login', component: '@/pages/login/LoginIndex' },
    { title: '404', exact: true, path: '/404', component: '@/pages/404/Notfound' },
    { title: '403', exact: true, path: '/403', component: '@/pages/403/Forbidden' },
    {
      path: '/',
      component: '@/layouts/Home',
      wrappers: ['@/wrappers/auth'],
      routes: [
        { wrappers: ['@/wrappers/access'], title: '监控页', exact: true, path: '/dashboard', component: '@/pages/dashboard/Dashboard' },
        { wrappers: ['@/wrappers/access'], title: '文章列表', exact: true, path: '/article/list', component: '@/pages/articleList/ArticleList' },
        { wrappers: ['@/wrappers/access'], title: '添加文章', exact: true, path: '/article/add', component: '@/pages/articleAdd/ArticleAdd' },
        { wrappers: ['@/wrappers/access'], title: '添加分类', exact: true, path: '/classify/add', component: '@/pages/classifyAdd/ClassifyAdd' },
        { wrappers: ['@/wrappers/access'], title: '分类列表', exact: true, path: '/classify/list', component: '@/pages/classifyList/ClassifyList' },
        { wrappers: ['@/wrappers/access'], title: '用户管理', exact: true, path: '/auth/userlist', component: '@/pages/userList/UserList' },
        { wrappers: ['@/wrappers/access'], title: '菜单管理', exact: true, path: '/auth/menulist', component: '@/pages/menuList/MenuList' },
        { wrappers: ['@/wrappers/access'], title: '角色管理', exact: true, path: '/auth/rolelist', component: '@/pages/roleList/RoleList' },
        { wrappers: ['@/wrappers/access'], title: '个人中心', exact: true, path: '/setting/userinfo', component: '@/pages/userinfo/UserInfo' },
        { wrappers: ['@/wrappers/access'], title: '个人设置', exact: true, path: '/setting/setuser', component: '@/pages/setuser/SetUser' },
        { exact: true, path: '/', redirect: '/dashboard' },
        { redirect: '/404' },
      ]
    },
  ]
});
