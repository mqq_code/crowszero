import React, { useState, useMemo } from 'react'

const App = () => {
  const [count, setCount] = useState(0)
  const [list, setList] = useState([
    {
      title: '苹果',
      price: 10,
      count: 1
    },
    {
      title: '香蕉',
      price: 11,
      count: 2
    },
    {
      title: '梨',
      price: 12,
      count: 3
    }
  ])

  const getTotal = useMemo(() => {
    return list.reduce((prev, val) => {
      console.log('计算总价', val)
      return prev + val.price * val.count
    }, 0)
  }, [list])

  const changeCount = (index, num) => {
    const newList = [...list]
    newList[index].count += num
    setList(newList)
  }


  return (
    <div>
      <ul>
        {list.map((item, index) =>
          <li key={item.title}>
            <h3>{item.title}</h3>
            <p>单价: ${item.price}</p>
            <p>
              <button onClick={() => changeCount(index, -1)}>-</button>
              {item.count}
              <button onClick={() => changeCount(index, 1)}>+</button>
            </p>
          </li>
        )}
      </ul>
      <footer>
        总价: ¥{getTotal}
      </footer>

      <button onClick={() => {
        setCount(count + 1)
      }}>按钮点击次数{count}</button>
    </div>
  )
}

export default App