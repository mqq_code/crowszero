import React, { useState, useMemo } from 'react'


const App = () => {
  const [show, setShow] = useState(false)
  const [user, setUser] = useState('白老师')
  const [age, setAge] = useState(18)

  // const getInfo = () => {
  //   console.log('调用了getInfo')
  //   return `我的名字叫${user}，今年${age}岁`
  // }

  // 返回一个缓存的值
  // 类似vue中的computed，依赖项改变时执行函数重新计算，依赖项没有改变从缓存中读取数据
  const getInfo = useMemo(() => {
    console.log('user更新了，调用了getInfo')
    return `我的名字叫${user}，今年${age}岁`
  }, [user])

  return (
    <div>
      <div>姓名: {user} <input type="text" value={user} onChange={e => setUser(e.target.value)} /></div>
      <div>年龄: {age} <input type="text" value={age} onChange={e => setAge(e.target.value)} /></div>
      <div>描述：{getInfo}</div>
      <div>描述：{getInfo}</div>
      <div>描述：{getInfo}</div>
      <div>描述：{getInfo}</div>
      <div>描述：{getInfo}</div>
      <div>描述：{getInfo}</div>
      <hr />
      <button onClick={() => setShow(!show)}>切换</button>
      {show ? '显示' : '隐藏'}
      <hr />
    </div>
  )
}

export default App