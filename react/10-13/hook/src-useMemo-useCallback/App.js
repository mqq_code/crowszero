import React, { useState, useCallback, useMemo } from 'react'

let arr = []

const App = () => {
  const [count, setCount] = useState(0)
  const sub = () => {
    setCount(count - 1)
    test()
  }
  const add = () => {
    setCount(count + 1)
  }

  // 缓存函数，依赖项发生改变重新创建函数，否则返回缓存的函数
  const test = useCallback(() => {
    console.log('我是test函数', count)
  }, [count])
  
  if (arr.indexOf(test) === -1) {
    arr.push(test)
  }
  console.log(arr)


  // useMemo实现useCallback功能
  const test1 = useMemo(() => {
    return () => {
      // 缓存此函数
    }
  }, [])




  const [show, setShow] = useState(false)

  return (
    <div>
      <h1>useCallbck</h1>
      <div>
        <button onClick={sub}>-</button>
        {count}
        <button onClick={add}>+</button>
      </div>

      <button onClick={() => setShow(!show)}>change</button>{show ? 'true' : 'false'}
    </div>
  )
}

export default App