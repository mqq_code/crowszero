import React, { useState, useImperativeHandle, forwardRef } from 'react'

const Count1 = (props, ref) => {
  // ref: 接收父组件传过来的ref对象

  const [num, setNum] = useState(0)

  const add = () => {
    setNum(num + 1)
  }

  // 给父组件传过来的ref对象添加方法和属性
  useImperativeHandle(ref, () => {
    return {
      add,
      getN: () => {
        console.log('当前的num', num)
      }
    }
  }, [num])

  return (
    <div>
      <h2>count1 函数组件</h2>
      <button onClick={add}>+</button>
      {num}
    </div>
  )
}

// forwardRef: 把父组件传过来的ref对象转发给组件
export default forwardRef(Count1)
