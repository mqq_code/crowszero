import React, { Component } from 'react'

export default class Count extends Component {

  state = {
    num: 0
  }
  add = () => {
    this.setState({ num: this.state.num + 1 })
  }
  render() {
    return (
      <div>
        <button onClick={this.add}>+</button>
        {this.state.num}
      </div>
    )
  }
}
