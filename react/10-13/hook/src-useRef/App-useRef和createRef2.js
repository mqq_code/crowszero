import React, { useState, useEffect, useRef, createRef } from 'react'

const App = () => {
  const [num, setNum] = useState(0)

  const title1 = createRef() // 每次组件更新都会重新创建对象

  // 组件更新时 useRef的 current 属性始终指向同一个地址
  // 可以存一些跟页面更新无关的数据，current中的数据改变不会触发页面更新，类似class组件中的实例属性始终
  const title2 = useRef()

  console.log(title1)
  console.log(title2)

  return (
    <div>
      <button onClick={() => {
        title1.current = { name: 'aaa' }
        title2.current = { name: 'bbb' }
      }}>给ref赋值</button>

      <button onClick={() => {
        console.log(title1)
        console.log(title2)
      }}>查看ref</button>

      <button onClick={() => {
        setNum(num + 1)
      }}>点击次数{num}</button>
    </div>
  )
}

export default App