import React, { useEffect, useRef } from 'react'

const App = () => {
  // 可以获取dom元素
  const title = useRef() // 创建ref对象
  useEffect(() => {
    console.log(title)
  }, [])

  return (
    <div>
      <h1 ref={title}>标题</h1>
      <button onClick={() => {
        console.log(title)
      }}>获取h1 dom元素</button>
    </div>
  )
}

export default App