import React, { useState, useEffect, useRef } from 'react'
import './App.css'

const App = () => {
  const [pos, setPos] = useState({
    left: 0,
    top: 0
  })
  
  const mouseInfo = useRef({
    flag: false, // 是否按下
    offsetX: 0, // 存按下时鼠标到盒子左侧的距离
    offsetY: 0 // 存按下时鼠标到盒子顶部的距离
  })

  const mouseDown = (e) => {
    // 存按下时鼠标到盒子左侧和顶部的距离
    mouseInfo.current = {
      flag: true,
      offsetX: e.nativeEvent.offsetX,
      offsetY: e.nativeEvent.offsetY
    }
  }
  const mouseMove = (e) => {
    if (mouseInfo.current.flag) {
      setPos({
        left: e.pageX - mouseInfo.current.offsetX,
        top: e.pageY - mouseInfo.current.offsetY,
      })
    }
  }
  const mouseUp = () => {
    mouseInfo.current.flag = false
  }

  useEffect(() => {
    document.addEventListener('mousemove', mouseMove)
  }, [])

  return (
    <div>
      <div className="box" style={pos} onMouseDown={mouseDown} onMouseUp={mouseUp}></div>
    </div>
  )
}

export default App