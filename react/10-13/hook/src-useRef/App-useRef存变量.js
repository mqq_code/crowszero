import React, { useState, useEffect, useRef, createRef } from 'react'

const App = () => {
  const [num, setNum] = useState(0)

  // useRef 存的变量始终指向同一个地址，不会触发组件更新
  const n = useRef()

  const getNum = () => {

    setTimeout(() => {
      alert(n.current)
    }, 3000)
  }

  return (
    <div>
      <button onClick={() => {
        setNum(num + 1)
        n.current = num + 1
      }}>点击次数{num}</button>

      <button onClick={getNum}>alert</button>
    </div>
  )
}

export default App