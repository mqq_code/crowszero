import React, { useState, useEffect, useRef, createRef } from 'react'

const App = () => {
  const [num, setNum] = useState(0)

  const title1 = createRef() // 每次组件更新都会重新创建对象
  const title2 = useRef() // 组件更新时 useRef的 current 属性始终指向同一个地址

  console.log(title1)
  console.log(title2)

  useEffect(() => {
    // 组件渲染完成
    console.log(title1)
    console.log(title2)
  }, [])

  return (
    <div>
      <h1 ref={title1}>标题1</h1>
      <h1 ref={title2}>标题2</h1>

      <button onClick={() => {
        setNum(num + 1)
      }}>点击次数{num}</button>
    </div>
  )
}

export default App