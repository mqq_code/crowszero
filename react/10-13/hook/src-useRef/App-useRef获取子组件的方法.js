import React, { useRef } from 'react'
// import Count from './components/Count'
import Count1 from './components/Count1'

const App = () => {
  const CountIns = useRef(111)

  return (
    <div>
      <h1>App</h1>
      <button onClick={() => {
        // console.log(CountIns)
        // CountIns.current.getN()
        CountIns.current.add()
      }}>app中的 +</button>

      <Count1 ref={CountIns} />
    </div>
  )
}
// 使用useRef获取函数子组件中的数据和方法
// 1. 在父组件中创建 useRef 对象，通过ref属性传给子组件
// 2. 在子组件中通过 forwardRef 获取父组件传过来的ref对象，子组件的第二个参数接收
// 3. 在子组件中通过 useImperativeHandle 给父组件传入的ref对象添加方法和属性
// useImperativeHandle(ref对象，() => {
//   把返回值赋值给ref对象
//   return xxx
// }, [依赖项])




// 使用useRef 获取class组件实例，调用子组件的方法，获取子组件实例
// const App = () => {
//   const CountIns = useRef()
//   return (
//     <div>
//       <h1>App</h1>
//       <button onClick={() => {
//         console.log(CountIns.current)
//         // 获取class组件实例，调用子组件的方法，获取子组件实例
//         CountIns.current.add()
//       }}>app中的 +</button>
//       <Count ref={CountIns} />
//     </div>
//   )
// }

export default App