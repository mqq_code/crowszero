import React, { useEffect, useState, useLayoutEffect } from 'react'

const App = () => {
  const [color, setColor] = useState('red')

  // 页面渲染后执行
  // useEffect(() => {
  //   console.log('useEffect ')
  //   let n = 0
  //   while(n < 40000) {
  //     n ++
  //     console.log(n)
  //   }
  //   setColor('green')
  // }, [])

  // dom构建后页面渲染前执行
  useLayoutEffect(() => {
    console.log('useLayoutEffect ')
    let n = 0
    while(n < 40000) {
      n ++
      console.log(n)
    }
    setColor('green')
  }, [])

  return (
    <div>
      <h1 style={{ color }}>标题</h1>
    </div>
  )
}

export default App