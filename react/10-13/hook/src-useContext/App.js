import React, { useState } from 'react'
import Child1 from './components/Child1'
import { Provider } from './context/color'

const App = () => {

  const [color, setColor] = useState('#ff0000')

  return (
    <Provider value={{ color, setColor }}>
      <div>
        <h1 style={{ color }}>App组件 useContext</h1>
        修改标题颜色：<input type="color" value={color} onChange={e => setColor(e.target.value)} />
        <hr />
        <Child1 />
      </div>
    </Provider>
  )
}

export default App