import React, { useContext } from 'react'
import Child2 from './Child2'
// 引入context对象
import colorCtx from '../context/color'

const Child1 = () => {
  // 获取context对象传入的值
  const { color } = useContext(colorCtx)

  return (
    <div>
      <h2 style={{ color }}>Child1 组件</h2>
      <ul>
        <li>1</li>
        <li>1</li>
        <li>1</li>
        <li>1</li>
      </ul>
      <Child2 />
    </div>
  )
}

export default Child1