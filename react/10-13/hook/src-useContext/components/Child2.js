import React, { useContext } from 'react'
import colorCtx from '../context/color'

const Child2 = () => {
  const { color, setColor } = useContext(colorCtx)

  return (
    <div style={{ border: '1px solid #333' }}>
      <h3 style={{ color }}>Child2 组件</h3>
      <input type="color" value={color} onChange={e => setColor(e.target.value)} />
    </div>
  )
}

export default Child2