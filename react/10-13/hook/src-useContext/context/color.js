import { createContext } from 'react'

const color = createContext()

export const Provider = color.Provider
export const Consumer = color.Consumer

export default color
