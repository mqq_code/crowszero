import React from 'react'
import './App.scss'
import {
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import Detail from './pages/detail/Detail'
import Address from './pages/address/Address'
import Add from './pages/add/Add'
import Home from './pages/home/Home'
import GoHome from './pages/gohome/GoHome'
import Cart from './pages/cart/Cart'
import Mine from './pages/mine/Mine'

const App = () => {
  return (
    <Switch>
      <Route exact path="/detail" component={Detail} />
      <Route exact path="/address" component={Address} />
      <Route exact path="/add" component={Add} />
      <Route path="/" render={routeInfo => (
        <Home {...routeInfo}>
          <Switch>
            <Route exact path="/" component={GoHome} />
            <Route exact path="/cart" component={Cart} />
            <Route exact path="/mine" component={Mine} />
            <Redirect to="/" />
          </Switch>
        </Home>
      )} />
    </Switch>
  )
}

export default App