import { useLocation } from 'react-router-dom'

const useQuery = () => {
  const location = useLocation()
  const query = {}
  const arr = location.search.slice(1).split('&')
  arr.forEach(item => {
    const [key, val] = item.split('=')
    query[key] = val
  })
  return query
}

export default useQuery