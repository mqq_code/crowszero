import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'

const initState = {
  curAddress: {},
  addressList: [],
  cartList: []
}

const reducer = (state = initState, { type, payload }) => {

  if (type === 'CHANGGE_ADDRESS') {
    return {...state, curAddress: payload }
  } else if (type === 'ADD_ADDRESS') {
    return {
      ...state,
      addressList: state.addressList.concat([{ ...payload, id: Date.now() + '' }])
    }
  } else if (type === 'EDIT_ADDRESS') {
    const curAddress = state.curAddress.id === payload.id ? payload :  state.curAddress
    return {
      ...state,
      curAddress,
      addressList: state.addressList.map(v => {
        if (v.id === payload.id) {
          return payload
        }
        return v
      })
    }
  } else if (type === 'ADD_CART') {
    const newState = JSON.parse(JSON.stringify(state))
    const curGoods = newState.cartList.find(v => v.wareId === payload.wareId)
    if (curGoods) {
      curGoods.count ++
    } else {
      newState.cartList.push({ ...payload, count: 1, checked: true })
    }
    return newState
  } else if (type === 'CHANGE_CHECKED') {
    const newState = JSON.parse(JSON.stringify(state))
    newState.cartList[payload].checked = !newState.cartList[payload].checked
    return newState
  } else if (type === 'CHANGE_ALL') {
    const newState = JSON.parse(JSON.stringify(state))
    newState.cartList.forEach(v => v.checked = payload)
    return newState
  } else if (type === 'CHANGE_COUNT') {
    const newState = JSON.parse(JSON.stringify(state))
    const { index, num } = payload
    newState.cartList[index].count += num
    if (newState.cartList[index].count <= 0) {
      newState.cartList.splice(index, 1)
    }
    return newState
  }
  return state
}

const store = createStore(reducer, applyMiddleware(logger))

export default store