import React from 'react'
import { List, Button, Empty } from 'antd-mobile'
import {
  AddOutline,
  EditSOutline,
  CheckCircleOutline
} from 'antd-mobile-icons'
import style from './address.module.scss'
import { connect } from 'react-redux'

const Address = (props) => {
  const changeAddress = (payload) => {
    props.dispatch({
      type: 'CHANGGE_ADDRESS',
      payload
    })
    props.history.goBack()
  }
  return (
    <div className={style.address}>
      <List className={style.content} header='历史地址'>
        {props.addressList.length > 0 ?
          props.addressList.map(item =>
            <List.Item
              key={item.id}
              title={item.name}
              description={item.address}
              clickable
              arrow={false}
              extra={
                <EditSOutline onClick={(e) => {
                  e.stopPropagation()
                  props.history.push('/add?id=' + item.id)
                }} />
              }
              prefix={
                <CheckCircleOutline
                  color={props.curAddress.id === item.id ? 'var(--adm-color-danger)' : ''}
                />
              }
              onClick={() => changeAddress(item)}
            >
              {item.tel}
            </List.Item>
          )
        : <Empty style={{ background: '#eee' }} description='暂无数据' />}
      </List>
      <footer>
        <Button
          style={{
            '--text-color': 'orange',
            '--border-width': 0
          }}
          block
          onClick={() => props.history.push('/add')}
        ><AddOutline />新增地址</Button>
      </footer>
    </div>
  )
}

const mapState = state => {
  return {
    addressList: state.addressList,
    curAddress: state.curAddress
  }
}
export default connect(mapState)(Address)