import React from 'react'
import style from './add.module.scss'
import {
  Form,
  Input,
  Button,
  TextArea,
  Toast
} from 'antd-mobile'
import { useDispatch, useSelector } from 'react-redux'
import useQuery from '../../custom/useQuery'


const Add = (props) => {

  const query = useQuery() // 获取 ? 后的参数
  const dispatch = useDispatch() // 获取dispatch
  const editAddress = useSelector(state => {
    // state: 仓库的数据
    return state.addressList.find(v => v.id === query.id) || {}
  }) // 获取仓库数据


  const onFinish = (values) => {
    if (editAddress.id) {
      dispatch({
        type: 'EDIT_ADDRESS',
        payload: {
          ...values,
          id: editAddress.id
        }
      })
      Toast.show({
        icon: 'success',
        content: '修改成功',
      })
    } else {
      dispatch({
        type: 'ADD_ADDRESS',
        payload: values
      })
      Toast.show({
        icon: 'success',
        content: '添加成功',
      })
    }
    props.history.goBack()
  }

  return (
    <div className={style.add}>
      <Form
        layout='horizontal'
        onFinish={onFinish}
        style={{ '--prefix-width': '80px' }}
        initialValues={editAddress}
        footer={
          <Button block type='submit' color='primary' size='large'>
            提交
          </Button>
        }
      >
        <Form.Item
          name='name'
          label='姓名'
          rules={[{ required: true, message: '姓名不能为空' }]}
        >
          <Input placeholder='请输入姓名' />
        </Form.Item>
        <Form.Item
          name='tel'
          label='手机号'
          rules={[
            { required: true, message: '请输入手机号' },
            { pattern: /^1[3-9]\d{9}$/, message: '请输入正确的手机号' }
          ]}
        >
          <Input placeholder='请输入手机号' />
        </Form.Item>
        <Form.Item name='address' label='地址' rules={[{ required: true, message: '请输入地址' }]}>
          <TextArea
            placeholder='请输入地址'
            maxLength={100}
            rows={2}
            showCount
          />
        </Form.Item>
      </Form>
    </div>
  )
}

export default Add