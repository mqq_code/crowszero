import React, { useEffect, useState } from 'react'
import style from './gohome.module.scss'
import { NavBar, Space, Button, Toast } from 'antd-mobile'
import { SearchOutline, MoreOutline, EnvironmentOutline } from 'antd-mobile-icons'
import { connect } from 'react-redux'
import axios from 'axios'

const GoHome = props => {
  const [list, setList] = useState([])

  const right = (
    <div style={{ fontSize: 24 }}>
      <Space style={{ '--gap': '16px' }}>
        <SearchOutline />
        <MoreOutline />
      </Space>
    </div>
  )
  
  const goAddress = () => {
    props.history.push('/address')
  }

  const getList = async () => {
    try {
      const res = await axios.post('/mqq/mp/search/wareSearch', {
        param: JSON.stringify({
          "venderId":1,
          "storeId":10485,
          "businessCode":1,
          "from":1,
          "categoryType":1,
          "pageNum":1,
          "pageSize":20,
          "categoryId":"11340",
          "categoryLevel":1
        }),
      }, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      })
      setList(res.data.data.wareList)
    } catch (e) {
      // 处理失败
    }
  }

  useEffect(() => {
    getList()
  }, [])

  const add = (goods) => {
    props.dispatch({
      type: 'ADD_CART',
      payload: goods
    })
    Toast.show({ content: '添加购物车成功' })
  }
  return (
    <div className={style.gohome}>
      <NavBar right={right} backArrow={false}>
        <div className={style.address} onClick={goAddress}>
          <EnvironmentOutline />
          {props.curAddress.address ? `${props.curAddress.address} - ${props.curAddress.name}` : '请选择地址'}
        </div>
      </NavBar>
      <div className={style.list}>
        {list.map(item =>
          <dl key={item.wareId}>
            <dt><img src={item.wareImg} alt="" /></dt>
            <dd>
              <h4>{item.wareName}</h4>
              <p>
                <span>¥{item.warePrice / 100}</span>
                <Button style={{
                  '--border-color': 'orange',
                  '--text-color': 'orange'
                }} size="mini" onClick={() => add(item)}>+</Button>
              </p>
            </dd>
          </dl>
        )}
      </div>
    </div>
  )
}

const mapState = state => {
  return {
    curAddress: state.curAddress
  }
}
export default connect(mapState)(GoHome)
