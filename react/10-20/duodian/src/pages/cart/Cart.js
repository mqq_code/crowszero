import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import style from './cart.module.scss'
import { Button, List, Space, NoticeBar, Empty, Dialog, Toast } from 'antd-mobile'
import {
  CheckCircleOutline,
  CloseCircleOutline
} from 'antd-mobile-icons'

const Cart = (props) => {
  const dispatch = useDispatch()
  const cartList = useSelector(state => state.cartList)
  const curAddress = useSelector(state => state.curAddress)
  const [checkAll, setAll] = useState(false)
  const [total, setTotal] = useState(0)
  
  const goAddress = () => {
    props.history.push('/address')
  }
  const changeChecked = (index) => {
    dispatch({
      type: 'CHANGE_CHECKED',
      payload: index
    })
  }

  useEffect(() => {
    setAll(cartList.every(v => v.checked))
    setTotal(cartList.reduce((prev, val) => {
      return prev + (val.checked ? (val.count * val.warePrice) / 100 : 0)
    }, 0).toFixed(2))
  }, [cartList])

  const changeAll = () => {
    setAll(!checkAll)
    dispatch({
      type: 'CHANGE_ALL',
      payload: !checkAll
    })
  }
  const changeCount = (index, num, show) => {
    if (show) {
      Dialog.confirm({
        content: '是否删除此商品',
        onConfirm: () => {
          dispatch({
            type: 'CHANGE_COUNT',
            payload: {
              index,
              num
            }
          })
          Toast.show({
            icon: 'success',
            content: '提交成功',
          })
        },
      })
    } else {
      dispatch({
        type: 'CHANGE_COUNT',
        payload: {
          index,
          num
        }
      })
    }
  }

  const right = (item, index) => {
    return <Space>
      <Button onClick={() => changeCount(index, -1, item.count === 1)} size="mini" color="danger" fill="outline">-</Button>
      {item.count}
      <Button onClick={() => changeCount(index, 1)} size="mini" color="primary" fill="outline">+</Button>
    </Space>
  }
  return (
    <div className={style.cart}>
      <List style={{ marginBottom: '20px', background: '#fff' }}>
        {curAddress.name ?
          <List.Item onClick={goAddress} description={curAddress.address} arrow>
            {curAddress.name} - {curAddress.tel}
          </List.Item>
        : 
          <List.Item arrow onClick={goAddress}>请选择配送地址</List.Item>
        }
      </List>
      {cartList.length > 0 ?
        <List
          header={
            <Space onClick={changeAll}>
              <CheckCircleOutline color={checkAll ? 'var(--adm-color-danger)' : ''}/>
              全选
            </Space>
          }
          style={{ background: '#fff' }}
        >
          {cartList.map((item, index) =>
            <List.Item
              key={item.wareId}
              description={'¥' + item.warePrice / 100}
              arrow={false}
              extra={right(item, index)}
              prefix={<CheckCircleOutline onClick={() => changeChecked(index)} color={item.checked ? 'var(--adm-color-danger)' : ''}/>}
            >
              {item.wareName}
            </List.Item>
          )}
          <NoticeBar
            extra={
              <Space style={{ '--gap': '12px' }}>
                <span>查看详情</span>
                <CloseCircleOutline />
              </Space>
            }
            content={'自定义右侧功能区'}
            color='alert'
          />
          <List.Item
            description='促销已抵扣金额'
            arrow={false}
            extra={<Button color="primary">选好了</Button>}
          >
            合计：¥{total}
          </List.Item>
        </List>
      :
        <Empty style={{ background: '#eee' }} description='暂无数据' />
      }
    </div>
  )
}

export default Cart