import React from 'react'
import { Badge, TabBar } from 'antd-mobile'
import style from './home.module.scss'
import {
  AppOutline,
  MessageOutline,
  UserOutline,
} from 'antd-mobile-icons'
import { useSelector } from 'react-redux'

const Home = (props) => {

  const { history, location } = props
  const { pathname } = location
  const totalCount = useSelector(s => {
    return s.cartList.reduce((prev, val) => prev + (val.checked ? val.count : 0), 0)
  })

  const setRouteActive = (value) => {
    history.push(value)
  }
  const tabs = [
    {
      key: '/',
      title: '首页',
      icon: <AppOutline />
    },
    {
      key: '/cart',
      title: '购物车',
      icon: <MessageOutline />,
      badge: totalCount,
    },
    {
      key: '/mine',
      title: '我的',
      icon: <UserOutline />,
    },
  ]

  return (
    <div className={style.home}>
      <main>{props.children}</main>
      <footer>
        <TabBar activeKey={pathname} onChange={value => setRouteActive(value)}>
          {tabs.map(item => (
            <TabBar.Item
              key={item.key}
              icon={item.icon}
              title={item.title}
              badge={item.badge > 0 ? item.badge : null}
            />
          ))}
        </TabBar>
      </footer>
    </div>
  )
}

export default Home