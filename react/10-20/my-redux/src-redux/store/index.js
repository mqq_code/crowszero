import { createStore } from 'redux'

// 仓库初始值
const initState = {
  user: '诸葛小明',
  age: 30,
  arr: []
}

// reducer: 获取和修改state
const reducer = function (state = initState, action) {
  // state: 当前仓库的数据, action: dispatch 传入的数据
  console.log(action)

  const newState = JSON.parse(JSON.stringify(state))

  // 根据 action.type 判断要如何修改数据
  if (action.type === 'ADD_AGE') {
    newState.age += action.payload
    return newState
  } else if (action.type === 'SUB_AGE') {
    newState.age --
    return newState
  } else if (action.type === 'PUSH_ARR') {
    newState.arr.forEach(item => {
      item.count += 2
    })
    return newState
  }
  return state
}

const store = createStore(reducer)

export default store