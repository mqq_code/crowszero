import React, { useEffect, useState } from 'react'
import store from '../store'

const Child2 = () => {
  const [info, setInfo] = useState({
    user: store.getState().user,
    age: store.getState().age
  })

  const add = () => {
    store.dispatch({
      type: 'ADD_AGE',
      payload: 2
    })
  }

  useEffect(() => {
    const unsubscribe = store.subscribe(() => {
      console.log('child2更新')
      setInfo({
        user: store.getState().user,
        age: store.getState().age
      })
    })

    return () => {
      unsubscribe()
    }
  }, [])

  return (
    <div>
      {info.age} <button onClick={add}>+2</button>
    </div>
  )
}

export default Child2

// class Child2 extends Component {
//   state = {
//     user: store.getState().user,
//     age: store.getState().age
//   }

//   add = () => {
//     store.dispatch({
//       type: 'ADD_AGE',
//       payload: 2
//     })
//   }

//   componentDidMount() {
//     // 返回一个清楚监听的函数
//     this.unSubscribe = store.subscribe(() => {
//       console.log('child2 更新了')
//       this.setState({
//         age: store.getState().age
//       })
//     })
//   }
//   componentWillUnmount() {
//     // 组件销毁清除异步
//     this.unSubscribe()
//   }

//   render() {
//     return (
//       <div>
//         {this.state.age} <button onClick={this.add}>+2</button>
//       </div>
//     )
//   }
// }

// export default Child2
