import React, { Component } from 'react'
import store from '../store'

console.log(store)

class Child1 extends Component {

  state = {
    user: store.getState().user,
    age: store.getState().age
  }

  add = () => {
    // 调用 dispatch 发送 action，修改数据
    store.dispatch({
      type: 'ADD_AGE',
      payload: 1
    })
  }
  sub = () => {
    store.dispatch({
      type: 'SUB_AGE'
    })
  }

  componentDidMount() {
    this.unsubscribe = store.subscribe(() => {
      console.log('child1 更新了')

      this.setState({
        user: store.getState().user,
        age: store.getState().age
      })
    })
  }

  componentWillUnmount() {
    this.unsubscribe()
  }

  render() {
    return (
      <div>
        <h2>Child1</h2>
        <p>姓名: {this.state.user}</p>
        <p>
          年龄: {this.state.age}
          <button onClick={this.sub}>-1</button>
          <button onClick={this.add}>+1</button>
        </p>
        
        <button onClick={() => this.props.history.push('/child2')}>编辑</button>
      </div>
    )
  }
}

export default Child1





// import React from 'react'

// const Child1 = (props) => {
//   return (
//     <div>
//       <h2>Child1</h2>
//       <p>姓名: 王小明</p>
//       <p>年龄: 20</p>
//       <button onClick={() => props.history.push('/child2')}>编辑</button>
//     </div>
//   )
// }

// export default Child1