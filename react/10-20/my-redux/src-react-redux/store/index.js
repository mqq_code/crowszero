import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger' // redux 中间件

// 仓库初始值
const initState = {
  user: '诸葛小明',
  age: 30,
  arr: []
}

// reducer: 获取和修改state
const reducer = function (state = initState, action) {
  // state: 当前仓库的数据, action: dispatch 传入的数据
  const newState = JSON.parse(JSON.stringify(state))
  // 根据 action.type 判断要如何修改数据
  if (action.type === 'ADD_AGE') {
    newState.age += action.payload
    return newState
  } else if (action.type === 'SUB_AGE') {
    newState.age --
    return newState
  } else if (action.type === 'PUSH_ARR') {
    newState.arr.forEach(item => {
      item.count += 2
    })
    return newState
  } else if (action.type === 'CHANGE_USER') {
    newState.user = action.payload
    return newState
  }
  return state
}

// applyMiddleware: 添加中间件
// 中间件：增强 dispatch
const store = createStore(reducer, applyMiddleware(logger))

export default store