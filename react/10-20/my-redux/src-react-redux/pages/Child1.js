import React, { Component } from 'react'
import { connect } from 'react-redux'

class Child1 extends Component {

  state = {
    text: this.props.user
  }

  render() {
    const { user, age, sub, submit, add, history } = this.props
    return (
      <div>
        <h2>Child1</h2>
        <p>姓名: {user}</p>
        <input type="text" value={this.state.text} onChange={e => this.setState({ text: e.target.value })}/>
        <button onClick={() => submit(this.state.text)}>修改姓名</button>
        <p>
          年龄: {age}
          <button onClick={sub}>-1</button>
          <button onClick={add}>+1</button>
        </p>
        <button onClick={() => history.push('/child2')}>编辑</button>
      </div>
    )
  }
}


const mapState = state => {
  return {
    user: state.user,
    age: state.age
  }
}
const mapDispatch = dispatch => {
  // dispatch === store.dispatch
  return {
    add: () => dispatch({ type: 'ADD_AGE', payload: 1 }),
    sub: () => dispatch({type: 'SUB_AGE' }),
    submit: payload => dispatch({ type: 'CHANGE_USER', payload })
  }
}
export default connect(mapState, mapDispatch)(Child1)

