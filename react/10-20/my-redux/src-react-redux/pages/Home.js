import React from 'react'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'

const Home = (props) => {

  return (
    <div>
      <nav>
        <NavLink to="/child1">child1</NavLink>
        <NavLink to="/child2">child2</NavLink>
        <span>姓名: {props.user}</span>
        <button onClick={() => {
          props.dispatch({
            type: 'CHANGE_USER',
            payload: '慕容老王'
          })
        }}>修改名字</button>
      </nav>
      {props.children}
    </div>
  )
}

const mapStateToProps = state => {
  // state: 仓库的数据 === store.getState()
  // 返回的对象的属性会添加到组件的 props 中
  return {
    user: state.user
  }
}

// connect: 高阶组件：连接仓库和组件
export default connect(mapStateToProps)(Home)