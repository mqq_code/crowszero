import React from 'react'
import { connect } from 'react-redux'

const Child2 = (props) => {
  
  const add = () => {
    props.dispatch({
      type: 'ADD_AGE',
      payload: 2
    })
  }

  return (
    <div>
      {props.age} <button onClick={add}>+2</button>
    </div>
  )
}

const mapState = state => {
  return {
    age: state.age + '岁'
  }
}
export default connect(mapState)(Child2)
