export const ADD_LIST = 'ADD_LIST' as const
export const REMOVE_LIST = 'REMOVE_LIST' as const

export const addListAction = (payload: IListItem) => {
  return {
    type: ADD_LIST,
    payload
  }
}

export const removeListAction = (id: number) => {
  return {
    type: REMOVE_LIST,
    payload: id
  }
}

export type IAction = ReturnType<
  typeof addListAction |
  typeof removeListAction
>