import { legacy_createStore, applyMiddleware, AnyAction } from 'redux'
import logger from 'redux-logger'
import { IAction } from './actions'

const initState: IStore = {
  list: [],
  test: '测试'
}

const reducer = (state = initState, action: IAction) => {
  if (action.type === 'ADD_LIST') {
    return {...state, list: [...state.list, action.payload]}
  } else if (action.type === 'REMOVE_LIST') {
    return {...state, list: state.list.filter(v => v.time !== action.payload)}
  }
  return state
}

const store = legacy_createStore(reducer, applyMiddleware(logger))

export default store
