interface IListItem {
  name: string;
  pt: string;
  bj: number;
  bjyf: boolean;
  yj: number;
  yjyf: boolean;
  time: number;
  remark: string;
}

interface IStore {
  list: IListItem[];
  test: string;
}
