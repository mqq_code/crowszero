import React from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import style from './detail.module.scss'
import { removeListAction } from '../../store/actions'

interface IParams {
  id: string;
}

const Detail: React.FC<RouteComponentProps<IParams>> = (props) => {
  const dispatch = useDispatch()
  const id = Number(props.match.params.id)
  const info = useSelector((state: IStore) => state.list.find(v => v.time === id))
  const remove = () => {
    dispatch(removeListAction(id))
    props.history.goBack()
  }
  return (
    <div className={style.detail}>
      <div>{JSON.stringify(info)}</div>
      <button>保存</button>
      <button onClick={remove}>删除</button>
    </div>
  )
}

export default Detail