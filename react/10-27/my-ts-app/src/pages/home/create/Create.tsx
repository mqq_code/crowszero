import React, { useState } from 'react'
import style from './create.module.scss'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { addListAction } from '../../../store/actions'

type IForm = Pick<IListItem, 'name' | 'bj' | 'bjyf' | 'pt' | 'remark' | 'yj' | 'yjyf'>

const Create = () => {
  const dispatch = useDispatch()
  const history = useHistory()

  const [form, setForm] = useState<IForm>({
    name: '',
    pt: '',
    bj: 0,
    bjyf: false,
    yj: 0,
    yjyf: false,
    remark: ''
  })

  const change = <K extends keyof IForm,>(key: K, val: IForm[K]) => {
    setForm({
      ...form,
      [key]: val
    })
  }

  const submit = () => {
    dispatch(addListAction({
      ...form,
      time: Date.now()
    }))

    history.push('/')
  }

  return (
    <div className={style.new}>
      <section className={style.papas}>
        <h4>平台</h4>
        <div className={style.form}>
          <input type="radio" name="radio" id="tb" value="淘宝" onChange={e => change('pt', e.target.value)} />
          <label htmlFor="tb">淘宝</label>
          <input type="radio" name="radio" id="jd" value="京东" onChange={e => change('pt', e.target.value)} />
          <label htmlFor="jd">京东</label>
          <input type="radio" name="radio" id="pdd" value="拼多多" onChange={e => change('pt', e.target.value)} />
          <label htmlFor="pdd">拼多多</label>
        </div>
      </section>

      <section className={style.task}>
        <h4>任务</h4>
        <div className={style.form}>
          <input type="text" name="text" placeholder='如"拍台灯"' value={form.name} onChange={e => change('name', e.target.value)} />
        </div>
      </section>

      <section className={style.money}>
        <h4>本金</h4>
        <div className={style.form}>
          <input type="number" className={style.number} value={form.bj} onChange={e => change('bj', Number(e.target.value))} />
          <label>元</label>
          <input type="checkbox" name="checkbox" id="bj" className={style.checkbox} checked={form.bjyf} onChange={e => change('bjyf', e.target.checked)} />
          <label htmlFor="bj">本金已返</label>
        </div>
      </section>

      <section className={style.money}>
        <h4>佣金</h4>
        <div className={style.form}>
          <input type="number" className={style.number} value={form.yj} onChange={e => change('yj', Number(e.target.value))}  />
          <label>元</label>
          <input type="checkbox" name="checkbox" id="yj" className={style.checkbox}  checked={form.yjyf} onChange={e => change('yjyf', e.target.checked)} />
          <label htmlFor="yj">佣金已返</label>
        </div>
      </section>

      <section className={style.remarks}>
        <h4>备注</h4>
        <div className={style.form}>
          <textarea cols={30} rows={10} placeholder='输入备注, 没有可不写'  value={form.remark} onChange={e => change('remark', e.target.value)} ></textarea>
        </div>
      </section>

      <section className={style.submit}>
        <button onClick={submit}>提交</button>
      </section>
    </div>
  )
}

export default Create