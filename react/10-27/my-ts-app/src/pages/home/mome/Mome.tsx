import React from 'react'
import style from './memo.module.scss'
import { useSelector } from 'react-redux'
import { Link, RouteComponentProps } from 'react-router-dom'

const Mome: React.FC<RouteComponentProps> = (props) => {
  const list = useSelector((state: IStore) => state.list)

  const empty = (
    <div className={style.empty}>
      <p>您的备忘录空空如也，快去创建一个吧～</p>
      <Link to="/create">
        <button>新建</button>
      </Link>
    </div>
  )

  const goDetail = (id: number) => {
    props.history.push(`/detail/${id}`)
  }

  return (
    <div className={style.memo}>
      {list.length > 0 ?
        <div className={style.list}>
          {list.map(item =>
            <div className={style.item} key={item.time} onClick={() => goDetail(item.time)}>
              <h3>{item.name}</h3>
              <p>本金{item.bj}，佣金{item.yj}</p>
              <p>{item.time}</p>
            </div>
          )}
        </div>
      :
        empty
      }
    </div>
  )
}

export default Mome