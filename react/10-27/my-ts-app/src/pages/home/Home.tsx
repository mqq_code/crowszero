import React from 'react'
import { RouteComponentProps, NavLink } from 'react-router-dom'
import style from './home.module.scss'

interface IProps {
  children: React.ReactElement
} 

const Home: React.FC<IProps & RouteComponentProps> = (props) => {
  return (
    <div className={style.home}>
      <main>{props.children}</main>
      <footer>
        <NavLink activeClassName={style.active} exact to="/">备忘录</NavLink>
        <NavLink activeClassName={style.active} exact to="/create">新建</NavLink>
        <NavLink activeClassName={style.active} exact to="/mine">我的</NavLink>
      </footer>
    </div>
  )
}

export default Home