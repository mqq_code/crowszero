export default [
  {
    id: '1',
    title: '歌手',
    checked: false,
    show: true,
    children: [
      {
        id: '1-1',
        title: '港台歌手',
        checked: false,
        show: true,
        children: [
          {
            id: '1-1-1',
            checked: false,
            title: '王心凌'
          },
          {
            id: '1-1-2',
            checked: false,
            title: '杨丞琳'
          },
          {
            id: '1-1-3',
            checked: false,
            title: '刘德华'
          },
          {
            id: '1-1-4',
            checked: false,
            title: '张学友'
          },
          {
            id: '1-1-5',
            checked: false,
            title: '周杰伦'
          }
        ]
      },
      {
        id: '1-2',
        title: '内地歌手',
        checked: false,
        show: true,
        children: [
          {
            id: '1-2-1',
            checked: false,
            title: '李荣浩'
          },
          {
            id: '1-2-2',
            checked: false,
            title: '毛不易'
          },
          {
            id: '1-2-3',
            checked: false,
            title: '薛之谦'
          },
          {
            id: '1-2-4',
            checked: false,
            title: '周深'
          }
        ]
      },
      {
        id: '1-3',
        title: '海外歌手',
        checked: false,
        show: true,
        children: [
          {
            id: '1-3-1',
            checked: false,
            title: '贾斯汀比伯'
          },
          {
            id: '1-3-2',
            checked: false,
            title: '艾薇儿'
          },
          {
            id: '1-3-3',
            checked: false,
            title: '皇后乐队'
          }
        ]
      }
    ]
  },
  {
    id: '2',
    title: '演员',
    checked: false,
    show: true,
    children: [
      {
        id: '2-1',
        title: '男演员',
        checked: false,
        show: true,
        children: [
          {
            id: '2-1-1',
            checked: false,
            title: '赵本山'
          },
          {
            id: '2-1-2',
            checked: false,
            title: '范伟'
          },
          {
            id: '2-1-3',
            checked: false,
            title: '周星驰'
          },
          {
            id: '2-1-4',
            checked: false,
            title: '周润发'
          },
          {
            id: '2-1-5',
            checked: false,
            title: '周星驰'
          }
        ]
      },
      {
        id: '2-2',
        title: '女演员',
        checked: false,
        show: true,
        children: [
          {
            id: '2-2-1',
            checked: false,
            title: '徐冬冬'
          },
          {
            id: '2-2-2',
            checked: false,
            title: '杨幂'
          },
          {
            id: '2-2-3',
            checked: false,
            title: '赵丽颖'
          },
          {
            id: '2-2-4',
            checked: false,
            title: '古力娜扎'
          }
        ]
      }
    ]
  }
]