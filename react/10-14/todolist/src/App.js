import React, { useState } from 'react'
import treeData from './treeData'

// 根据id从数据中查找对象，通过callback获取找到的对象
const getItem = (id, tree, callback) => {
  const newData = JSON.parse(JSON.stringify(tree))
  newData.forEach(item => {
    if (item.id === id) {
      callback && callback(item)
    } else if (item.children) {
      item.children = getItem(id, item.children, callback)
    }
  })
  return newData
}

const App = () => {
  const [data, setData] = useState(treeData)

  const changeShow = (id) => {
    let newData = getItem(id, data, (item) => {
      item.show = !item.show
    })
    setData(newData)
  }


  const changeChecked = (id) => {

    let newData = getItem(id, data, (item) => {
      item.checked = !item.checked
    })
    
    setData(newData)
  }

  const renderTree = (tree) => {
    return <ul className='tree'>
      {tree.map(item =>
        <li key={item.id}>
          <h2>
            <input type="checkbox" checked={item.checked} onChange={e => changeChecked(item.id)} />
            <span onClick={() => changeShow(item.id)}>{item.title}-{item.id}</span>
          </h2>
          <div style={{ display: item.show ? 'block' : 'none' }}>
            {item.children && renderTree(item.children)}
          </div>
        </li>
      )}
    </ul>
  }

  return (
    <div>
      <h1>树形结构</h1>
      {renderTree(data)}
    </div>
  )
}

export default App