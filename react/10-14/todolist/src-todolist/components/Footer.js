import React from 'react'

const navlist = [
  {
    title: '全部',
    type: 'all'
  },
  {
    title: '未完成',
    type: 'todo'
  },
  {
    title: '已完成',
    type: 'done'
  }
]

const Footer = props => {

  return (
    <footer>
      <div className="left">{props.todoLen} 未完成</div>
      <div className="center">
        {navlist.map(item => {
          if (item.type === 'all' || props.doneLen > 0 && props.doneLen < props.listLen) {
            return (
              <button
                key={item.type}
                className={props.type === item.type ? 'active' : ''}
                onClick={() => props.changeType(item.type)}
              >{item.title}</button>
            )
          }
        })}
      </div>
      {props.doneLen > 0 &&
        <div className="right">
          <button onClick={props.clearDone}>清空已完成</button>
        </div>
      }
    </footer>
  )
}

export default Footer