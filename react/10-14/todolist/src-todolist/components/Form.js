import React, { useRef } from 'react'

const Form = (props) => {
  const inp = useRef()

  const submit = () => {
    if (inp.current.value.trim() !== '') {
      props.add(inp.current.value)
      inp.current.value = ''
    }
    inp.current.focus()
  }
  const keyDown = e => {
    if (e.keyCode === 13) {
      submit()
    }
  }

  return (
    <div className='form-wrapper'>
      <h1 className="title">~ Today I need to ~</h1>
      <div className="form">
        <input type="text" placeholder='add new todo' ref={inp} onKeyDown={keyDown} />
        <button onClick={submit}>submit</button>
      </div>
    </div>
  )
}

export default Form