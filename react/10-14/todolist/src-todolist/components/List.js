import React from 'react'

const List = (props) => {
  return (
    <ul className='list'>
      {props.list.map(item =>
        <li className={item.checked ? 'done' : ''} key={item.id}>
          <div className="checkbox" onClick={() => props.change(item.id)}>
            {item.checked ?
              <svg t="1665711034760" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2526" width="24" height="24"><path d="M939.36 218.912a32 32 0 0 1 45.856 44.672l-538.016 552a32 32 0 0 1-43.776 1.92L63.872 526.048a32 32 0 1 1 41.696-48.544l316.768 271.936L939.36 218.88z" p-id="2527" fill="#ffffff"></path></svg>
            :
              <svg t="1665711105865" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3567" width="24" height="24"><path d="M768 128H256c-70.4 0-128 57.6-128 128v512c0 70.4 57.6 128 128 128h512c70.4 0 128-57.6 128-128V256c0-70.4-57.6-128-128-128z m64 640c0 35.2-28.8 64-64 64H256c-35.2 0-64-28.8-64-64V256c0-35.2 28.8-64 64-64h512c35.2 0 64 28.8 64 64v512z" p-id="3568"></path></svg>
            }
          </div>
          <div className="text">
            {item.checked ? <s>{item.text}</s> : item.text}
          </div>
          <div className="close" onClick={() => props.remove(item.id)}>
            <svg t="1665711131260" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4583" width="24" height="24"><path d="M576 512l277.333333 277.333333-64 64-277.333333-277.333333L234.666667 853.333333 170.666667 789.333333l277.333333-277.333333L170.666667 234.666667 234.666667 170.666667l277.333333 277.333333L789.333333 170.666667 853.333333 234.666667 576 512z" fill="#444444" p-id="4584"></path></svg>
          </div>
        </li>
      )}
    </ul>
  )
}

export default List