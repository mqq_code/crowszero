import React from 'react'

const Header = () => {
  return (
    <header>
      <img src="https://www.todolist.cn/img/note.75134fb0.svg" alt="Note" />
      <div className="title">To-Do List</div>
    </header>
  )
}

export default Header