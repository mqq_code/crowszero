import React, { useEffect, useMemo, useState } from 'react'
import 'animate.css'
import './App.scss'
import Header from './components/Header'
import Form from './components/Form'
import List from './components/List'
import Footer from './components/Footer'

const App = () => {

  const [list, setList] = useState(JSON.parse(localStorage.getItem('list')) || [])
  const [type, setType] = useState('all') // 当前展示的状态

  const add = text => {
    setList([...list, {
      id: Date.now(),
      checked: false,
      text
    }])
  }
  const remove = id => {
    setList(list.filter(item => item.id !== id))
  }
  const changeChecked = id => {
    setList(list.map(item => {
      if (item.id === id) {
        return {
          ...item,
          checked: !item.checked
        }
      }
      return item
    }))
  }
  const clearDone = () => {
    setList(list.filter(v => !v.checked))
  }

  // 展示的数据
  const curList = useMemo(() => {
    if (type === 'all') {
      return list
    } else if (type === 'todo') {
      return list.filter(v => !v.checked)
    } else {
      return list.filter(v => v.checked)
    }
  }, [list, type])

  // 待完成
  const todoLen = useMemo(() => {
    return list.filter(v => !v.checked).length
  }, [list])
  // 已完成
  const doneLen = useMemo(() => {
    return list.length - todoLen
  }, [list, todoLen])

  useEffect(() => {
    if (todoLen === 0 || doneLen === 0) {
      setType('all')
    }
  }, [todoLen, doneLen])

  useEffect(() => {
    localStorage.setItem('list', JSON.stringify(list))
  }, [list])

  return (
    <div className='app'>
      <Header />
      <Form add={add} />
      <List list={curList} remove={remove} change={changeChecked} />
      <Footer
        todoLen={todoLen}
        listLen={list.length}
        doneLen={doneLen}
        type={type}
        clearDone={clearDone}
        changeType={setType}
      />
    </div>
  )
}

export default App