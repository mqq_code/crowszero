import React, { Component } from 'react'

class Child extends Component {

  state = {
    n: '$' + this.props.num,
    list: [1, 2, 3, 4, 5]
  }

  addList = () => {
    this.setState({
      list: [...this.state.list, this.state.list.length + 1]
    })
  }

  // 静态方法，当state的数据依赖props改变时使用此方法
  static getDerivedStateFromProps(props, state) {
    // 返回值会和state进行合并
    return {
      n: '$' + props.num
    }
  }

  getSnapshotBeforeUpdate (prevProps, prevState) {
    console.log(prevState.list)
    console.log(this.state.list)
    if (prevState.list.length !== this.state.list.length) {
      return this.ul.scrollHeight
    }
    return null
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // snapshot: getSnapshotBeforeUpdate 生命周期的返回值
    console.log('snapshot', snapshot)
    if (snapshot > 300) {
      this.ul.scrollTop = snapshot
    }
  }

  ul = null

  render() {
    return (
      <div>
        Child: {this.state.n}
        <button onClick={this.addList}>添加列表</button>
        <ul ref={el => this.ul = el}>
          {this.state.list.map(item =>
            <li key={item}>{item}</li>
          )}
        </ul>
      </div>
    )
  }
}

export default Child
