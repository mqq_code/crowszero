import React, { Component } from 'react'
import Child from './components/Child'
import './App.css'
class App extends Component {

  state = {
    title: '标题',
    num: 0
  }

  changeNum = n => {
    this.setState({
      num: this.state.num + n
    })
  }

  render() {
    return (
      <div>
        <button onClick={() => this.changeNum(-1)}>-</button>
        {this.state.num}
        <button onClick={() => this.changeNum(1)}>+</button>
        <Child num={this.state.num} />
      </div>
    )
  }
}

export default App



