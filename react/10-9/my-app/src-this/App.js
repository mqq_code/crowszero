import React, { Component } from 'react'
import './App.css'

class App extends Component {

  constructor(props) {
    super(props)
    // 在初始化阶段就把事件处理函数的this改变
    this.sub = this.sub.bind(this)
  }

  state = {
    num: 0
  }

  add = () => {
    console.log(this)
  }

  sub() {
    console.log(this)
  }

  render() {
    return (
      <div>
        {/*
          合成事件：
          react中的事件没有绑定到具体的元素
          利用事件冒泡的原理
          把项目中的所有事件绑定到根元素上（版本18之前是document，18之后是root元素）
          然后根据点击的元素去事件池查找对应函数并且执行
        */}
        <button onClick={this.sub}>-</button>
        <button onClick={() => this.sub()}>-</button>
        {this.state.num}
        <button onClick={this.add}>+</button>
      </div>
    )
  }
}

export default App



