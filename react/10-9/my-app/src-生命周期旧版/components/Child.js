import React, { Component } from 'react'

class Child extends Component {

  state = {
    aa: 'aa',
    bb: this.props.bb
  }

  // 普通的属性改变不会触发组件的更新
  flag = false

  // 只有props更新会执行，state更新不执行 (新版本已废弃)
  // state的数据需要根据props的数据生成时使用
  componentWillReceiveProps(nextProps) {
    this.setState({
      bb: nextProps.bb
    })
  }

  // 可以比较组件中使用的数据是否更新了
  shouldComponentUpdate(nextProps, nextState) {
    console.log('nextProps', nextProps) // 最新的props
    console.log('nextState', nextState) // 最新的state
    console.log('更新前的props', this.props)
    console.log('更新前的state', this.state)
    if (nextProps.title === this.props.title && nextState.aa === this.state.aa) {
      return false
    }
    return true
  }

  render() {
    console.log('%c child组件开始渲染', 'color: red')
    return (
      <div>
        <h3>{this.state.aa}</h3>
        child组件: {this.props.title}
        <div>
          <button onClick={() => {
            this.flag = !this.flag
            console.log(this.flag)
            this.forceUpdate() // 强制更新
          }}>flag：{this.flag ? 'true' : 'false'}</button>
        </div>
      </div>
    )
  }
}

export default Child
