import React, { Component } from 'react'

class Count extends Component {
  state = {
    num: 0
  }
  timer = null
  componentDidMount() {
    this.timer = setInterval(() => {
      this.setState(state => ({
        num: state.num + 1
      }))
      console.log(this.state.num)
    }, 500)
  }

  // 组件销毁之前清除异步任务
  componentWillUnmount() {
    clearInterval(this.timer)
  }

  render() {
    return (
      <div>
        <hr />
        计时器：{this.state.num}
      </div>
    )
  }
}
export default Count
