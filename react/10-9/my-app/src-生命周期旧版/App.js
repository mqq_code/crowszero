import React, { Component } from 'react'
import Child from './components/Child'
import Count from './components/Count'

class App extends Component {

  // 挂载阶段1 - 初始化，可以初始化state和props
  constructor(props) {
    super(props) // 把组件的参数传给父类
    console.log('挂载阶段1 - constructor')
    // console.log(this.props)
    this.state = {
      title: '标题',
      num: 0
    }
  }

  // 挂载阶段2 - 组件挂载之前（旧版本，新版本已废弃）
  componentWillMount() {
    console.log('挂载阶段2 - componentWillMount')
  }

  // 挂载阶段4 - 组件挂载之完成（可以调用接口，操作dom，常用）
  componentDidMount() {
    console.log('挂载阶段4 - componentDidMount')
  }

  changeNum = n => {
    this.setState({
      num: this.state.num + n
    })
  }

  // 更新阶段触发时机：props改变，调用setState，调用 foceupdate
  // 更新阶段1 - 优化组件性能，减少不必要的更新
  shouldComponentUpdate() {
    console.log('更新阶段1 - shouldComponentUpdate')
    return true
  }

  // 更新阶段2 - 组件更新之前(新版已弃用)
  componentWillUpdate() {
    console.log('更新阶段2 - componentWillUpdate')
  }

  // 更新阶段4 - 组件更新完成 (可以获取最新的dom元素)
  componentDidUpdate() {
    console.log('更新阶段4 - componentDidUpdate')
  }

  // 挂载阶段3 - 渲染虚拟dom
  // 更新阶段3 - 渲染虚拟dom
  render() {
    console.log('挂载阶段3 - 更新阶段3 - render')
    return (
      <div>
        <h2>{this.state.title} <button onClick={() => {
          this.setState({
            title: '标题' + Math.random()
          })
        }}>随机更新标题</button></h2>
        <button onClick={() => this.changeNum(-1)}>-</button>
        {this.state.num}
        <button onClick={() => this.changeNum(1)}>+</button>
        <Child title={this.state.title} />
        {this.state.num > 0 && <Count />}
      </div>
    )
  }
}

export default App



