import React, { Component, PureComponent } from 'react'

// PureComponent: 性能优化，内部实现 shouldComponentUpdate，会自动浅比较state和props的所有内容
export default class Child extends PureComponent {
  state = {
    title: '标题',
    n: 0,
    obj: {
      name: 'aa',
      info: {
        bb: 120
      }
    }
  }
  changeTitle = e => {
    this.setState({
      title: e.target.value
    })
  }

  render() {
    console.log('child更新了')
    return (
      <div>
        <h2>{this.state.title}</h2>
        <input type="text" value={this.state.title} onChange={this.changeTitle} />
        <h3>父组件传过来的num: {this.props.num}</h3>
        <div>
          <button onClick={() => {
            let obj = {...this.state.obj}
            obj.info.bb++
            this.setState({ obj })
          }}>+</button>
          {this.state.obj.info.bb}
        </div>
      </div>
    )
  }
}
