import React, { Component } from 'react'
import './App.css'
import Child from './components/Child'

class App extends Component {

  state = {
    num: 0,
    count: 0
  }

  add = () => {
    this.setState({ num: this.state.num + 1 })
  }

  sub = () => {
    this.setState({ num: this.state.num - 1 })
  }
  componentDidMount() {
    setInterval(() => {
      this.setState(state => ({
        count: state.count + 1
      }))
    }, 1000)
  }

  render() {
    return (
      <div>
        <button onClick={this.sub}>-</button>
        {this.state.num}
        <button onClick={this.add}>+</button>
        <p>计时器：<b>{this.state.count}</b></p>
        <hr />
        <Child num={this.state.num} />
      </div>
    )
  }
}

export default App



