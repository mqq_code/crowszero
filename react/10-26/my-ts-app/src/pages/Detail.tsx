import React, { useEffect } from 'react'
import { RouteComponentProps } from 'react-router-dom'

// 定义动态路由参数类型
interface IParams {
  id: string;
  a: string;
}

type IProps = {} & RouteComponentProps<IParams>


const query = <T,>(search: string): T => {
  const arr = search.slice(1).split('&')
  const obj: any = {}

  arr.forEach(item => {
    const [key, val] = item.split('=')
    obj[key] = val
  })

  return obj as T
}


const Detail: React.FC<IProps> = (props) => {

  useEffect(() => {
    // console.log(props.match.params.id)
    const q = query<{ a: string; user: string }>(props.location.search)
    console.log(q.a)
    console.log(q.user)
  })

  return (
    <div>Detail</div>
  )
}

export default Detail