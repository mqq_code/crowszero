import React, { Component } from 'react'
import { RouteComponentProps, NavLink } from 'react-router-dom'

type IProps = RouteComponentProps & {
  title: string;
  children: React.ReactElement;
}

class Home extends Component<IProps> {
  render() {
    console.log(this.props)
    return (
      <div>
        <h2>Home</h2>
        <nav>
          <NavLink to="/">child1</NavLink>
          <NavLink to="/child2">child2</NavLink>
        </nav>
        <hr />
        {this.props.children}
      </div>
    )
  }
}

export default Home



// import React from 'react'
// import { RouteComponentProps } from 'react-router-dom'

// // 交叉类型
// // type IProps = RouteComponentProps & {
// //   title: string;
// // }
// interface IProps extends RouteComponentProps {
//   title: string;
//   children: React.ReactElement;
// }

// const Home: React.FC<IProps> = (props) => {
//   console.log(props)
//   return (
//     <div>
//       <h2>Home</h2>
//       {props.children}
//     </div>
//   )
// }

// export default Home