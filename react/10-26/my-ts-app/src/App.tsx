import React from 'react'
import Home from './pages/Home'
import Detail from './pages/Detail'
import Child1 from './pages/Child1'
import Child2 from './pages/Child2'
import {
  Switch,
  Route,
  Redirect
} from 'react-router-dom'

const App = () => {
  return (
    <Switch>
      <Route exact path="/detail/:id" component={Detail} />
      <Route path="/" render={(routeInfo) => {
        return <Home {...routeInfo} title="home组件">
          <Switch>
            <Route exact path="/" component={Child1} />
            <Route exact path="/child2" component={Child2} />
            <Redirect to="/" />
          </Switch>
        </Home>
      }} />
    </Switch>
  )
}

export default App