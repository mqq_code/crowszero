import React, { Component, createRef } from 'react'
import Child from './components/Child'

interface IProps {
  title: string;
}
interface IObj {
  name: string;
  age: number;
}
interface IState {
  num: number;
  obj: IObj
}

class App extends Component<IProps, IState> {

  state = {
    num: 0,
    obj: {
      name: '小王',
      age: 10
    }
  }
  add = () => {
    this.setState({
      num: this.state.num + 1,
      obj: {
        name: '王小明',
        age: this.state.obj.age + this.state.num
      }
    })
  }
  changeName = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      obj: {...this.state.obj, name: e.target.value }
    })
  }

  title = createRef<HTMLHeadingElement>()
  childRef = createRef<Child>()

  componentDidMount() {
    console.log(this.title.current?.clientHeight)

    this.title.current!.style.cssText = 'background: red'
  }

  render() {
    const { num, obj } = this.state
    return (
      <div>
        <h1 ref={this.title}>{this.props.title}</h1>
        <button onClick={this.add}>按钮点击次数{num}</button>
        <p>姓名:{obj.name}
          <input type="text" value={obj.name} onChange={this.changeName} />
        </p>
        <p>年龄:{obj.age}</p>
        <button onClick={() => {
          console.log(this.childRef)
          // 通过ref获取组件实例对象，调用子组件的方法
          this.childRef.current?.add()

        }}>调用child组件的add函数</button>
        <Child ref={this.childRef} />
      </div>
    )
  }
}

export default App