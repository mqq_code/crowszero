import React, { Component } from 'react'

interface IState {
  num: number;
}

class Child extends Component<{}, IState> {
  state = {
    num: 10
  }
  add = () => {
    this.setState(prevState => ({
      num: prevState.num + 1
    }))
    this.setState(prevState => ({
      num: prevState.num + 1
    }))
    this.setState(prevState => ({
      num: prevState.num + 1
    }))
  }
  render() {
    return (
      <div style={{ border: '3px solid red', padding: '20px', height: '300px' }}>
        <button onClick={this.add}>+</button> {this.state.num}
      </div>
    )
  }
}
export default Child
