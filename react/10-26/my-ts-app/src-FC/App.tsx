import React, { useRef } from 'react'
import Child from './components/Child'

interface IChild {
  add: () => void;
  inp: HTMLInputElement
}

const App = () => {
  // 父组件创建ref对象
  const childRef = useRef<IChild>(null)

  return (
    <div>
      <h1>父组件</h1>
      <button onClick={() => {
        // 调用自组件内的函数
        childRef.current?.add()
      }}>添加</button>
      {/* 把ref对象传给子组件 */}
      <Child ref={childRef} />
    </div>
  )
}

export default App