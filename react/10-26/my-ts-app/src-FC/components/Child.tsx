import React, { useRef, useState, forwardRef, useImperativeHandle } from 'react'

interface Item {
  id: number;
  text: string;
}

const Child: React.ForwardRefRenderFunction<{}, {}> = (props, ref) => {

  const [list, setlist] = useState<Item[]>([])
  const inp = useRef<HTMLInputElement>(null)

  const add = () => {
    setlist([...list, {
      id: Date.now(),
      text: inp.current!.value
    }])
    inp.current!.value = ''
  }

  // 给父组件传过来的ref对象添加数据
  useImperativeHandle(ref, () => {
    // return的内容会赋值给ref的current属性
    return {
      add,
      inp: inp.current
    }
  }, [list])

  return (
    <div style={{ border: '3px solid red', padding: '20px', height: '300px' }}>
      <input type="text" ref={inp} />
      <button onClick={add}>添加</button>
      <ul>
        {list.map(item =>
          <li key={item.id}>{item.text}</li>
        )}
      </ul>
    </div>
  )
}

// 把父组件传的ref转发给Child组件的第二个参数
export default forwardRef(Child)