import React, { useEffect, useState, useMemo, useRef } from 'react'
import './App.scss'

interface IProps {
  title: string;
}

interface IObj {
  name?: string;
  age?: number;
  money?: number;
}
const App: React.FC<IProps> = (props) => {
  const [num, setNum] = useState<number>(0)
  const [count, setCount] = useState<number>(0)
  const [obj, setObj] = useState<IObj>({ money: 100, name: '小明', age: 30 })

  const info: string = useMemo(() => {
    return `我叫${obj!.name}, 今年${String(obj!.age)}岁`
  }, [obj])

  const add = (n: number): void => {
    setNum(num + n)
  }

  useEffect(() => {
    setInterval(() => {
      setCount(n => {
        // console.log('获取最新值', n)
        return n + 1
      })
    }, 1000)
  }, [])

  const title = useRef<HTMLHeadingElement>(null)

  useEffect(() => {
    console.log(title.current?.innerHTML)
  }, [])

  const changeName = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value)
    setObj({...obj, name: e.target.value})
  }
  const handleClick = (e: React.MouseEvent<HTMLDivElement>) => {
    const { offsetHeight, clientHeight, scrollTop } = e.target as HTMLDivElement
  }
  return (
    <div>
      <h1 ref={title}>{props.title}</h1>
      <p>count: {count}</p>
      <button onClick={() => setNum(n => n + 1)}>按钮点击次数{num}</button>
      <hr />
      <p>姓名: {obj.name}</p>
      <p>年龄: {obj.age}</p>
      <p>身家: ¥{obj.money?.toFixed(2)}</p>
      {/* obj.money?.toFixed(2) 可选链 */}
      {/* obj.money!.toFixed(2) 非空断言 */}
      {info}
      <hr />
      <p>姓名: {obj.name}</p>
      <input type="text" value={obj.name} onChange={changeName} />
      <hr />
      <div className="box" onClick={handleClick}></div>
    </div>
  )
}

export default App