import { takeEvery, put, call } from 'redux-saga/effects'
import axios from 'axios'

function *bannerAction() {
  // 处理异步
  const res = yield call(axios.get, 'https://zyxcl-music-api.vercel.app/banner')

  yield put({
    type: 'SET_BANNER',
    payload: res.data.banners
  })
}

function *songsAction() {
  const res = yield call(axios.get, 'https://zyxcl-music-api.vercel.app/dj/program?rid=336355127')

  yield put({
    type: 'SET_SONGS',
    payload: res.data.programs
  })

}


function *saga() {
  yield takeEvery('GET_BANNER_DATA', bannerAction)
  yield takeEvery('GET_SONGS_DATA', songsAction)
}


export default saga