import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import saga from './saga'

const initState = {
  banner:[],
  songs: []
}

const reducer = (state = initState, action) => {
  if (action.type === 'SET_BANNER') {
    return {...state, banner: action.payload}
  } else if (action.type === 'SET_SONGS') {
    return {...state, songs: action.payload}
  }
  return state
}

// 创建saga中间件
const sagaMiddleware = createSagaMiddleware()

const store = createStore(reducer, applyMiddleware(sagaMiddleware, logger))

// 运行saga
sagaMiddleware.run(saga)

export default store