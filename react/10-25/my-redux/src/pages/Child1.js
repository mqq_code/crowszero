import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

const Child1 = () => {
  const state = useSelector(s => s)
  const dispatch = useDispatch()
  console.log(state)

  useEffect(() => {
    // dispatch({
    //   type: 'GET_BANNER_DATA'
    // })
    dispatch({
      type: 'GET_SONGS_DATA'
    })
  }, [])
  
  return (
    <div>Child1</div>
  )
}

export default Child1