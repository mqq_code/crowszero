import React from 'react'
import { NavLink } from 'react-router-dom'

const Home = (props) => {

  return (
    <div>
      <nav>
        <NavLink to="/child1">child1</NavLink>
        <NavLink to="/child2">child2</NavLink>
      </nav>
      {props.children}
    </div>
  )
}

export default Home