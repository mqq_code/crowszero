import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setGoodsAction } from '../store/actions'

const Child1 = () => {
  const goods = useSelector(s => s.goods.goods)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(setGoodsAction)
  }, [])

  return (
    <div>
      <ul>
        {goods.map(item =>
          <li key={item.imageUrl}><img src={item.imageUrl} /></li>
        )}
      </ul>
    </div>
  )
}

export default Child1