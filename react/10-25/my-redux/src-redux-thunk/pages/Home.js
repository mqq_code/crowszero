import React from 'react'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'

const Home = (props) => {

  return (
    <div>
      <nav>
        <NavLink to="/child1">child1</NavLink>
        <NavLink to="/child2">child2</NavLink>
      </nav>
      {props.children}
    </div>
  )
}

const mapStateToProps = state => {
  // state: 仓库的数据 === store.getState()
  // 返回的对象的属性会添加到组件的 props 中
  return {
    user: state.user
  }
}

// connect: 高阶组件：连接仓库和组件
export default connect(mapStateToProps)(Home)