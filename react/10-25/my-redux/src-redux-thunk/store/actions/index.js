import {
  ADD_LIST,
  DONE_LIST,
  CHANGE_USER,
  ADD_AGE,
  SUB_AGE,
  SET_GOODS
} from './action-type'
import axios from 'axios'

// 添加list的action
export const addListAction = (payload) => {
  return {
    type: ADD_LIST,
    payload
  }
}

export const doneListAction = (id) => {
  return {
    type: DONE_LIST,
    payload: id
  }
}

export const changeUserAction = (payload) => {
  return {
    type: CHANGE_USER,
    payload
  }
}
export const addAgeAction = () => {
  return {
    type: ADD_AGE
  }
}
export const subAgeAction = () => {
  return {
    type: SUB_AGE
  }
}


export const setGoodsAction = (next) => {
  axios.get('https://zyxcl-music-api.vercel.app/banner').then(res => {
    next({
      type: SET_GOODS,
      payload: res.data.banners
    })
  })
}