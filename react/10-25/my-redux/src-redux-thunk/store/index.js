import { createStore, applyMiddleware, combineReducers } from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import list from './reducers/list'
import userinfo from './reducers/userinfo'
import goods from './reducers/goods'

const reducer = combineReducers({
  list,
  userinfo,
  goods
})

// thunk: 让dispatch可以接受一个函数
const store = createStore(reducer, applyMiddleware(thunk, logger))

export default store