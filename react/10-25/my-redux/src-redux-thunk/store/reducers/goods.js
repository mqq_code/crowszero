import { SET_GOODS } from '../actions/action-type'

const initState = {
  goods: [],
  cart: []
}

const reducer = (state = initState, action) => {
  switch(action.type) {
    case SET_GOODS:
      return {...state, goods: action.payload}
    default:
      return state
  }
}
export default reducer