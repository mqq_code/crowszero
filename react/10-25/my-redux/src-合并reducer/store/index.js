import { createStore, applyMiddleware, combineReducers } from 'redux'
import logger from 'redux-logger'
import list from './reducers/list'
import userinfo from './reducers/userinfo'

const reducer = combineReducers({
  list,
  userinfo
})

const store = createStore(reducer, applyMiddleware(logger))

export default store