import { ADD_LIST, DONE_LIST } from '../actions/action-type'

const reducer = (state = [], action) => {
  const newState = JSON.parse(JSON.stringify(state))
  switch(action.type) {
    case ADD_LIST:
      return [...state, {
        id: Date.now(),
        done: false,
        text: action.payload
      }]
    case DONE_LIST:
      newState.forEach(item => {
        if (item.id === action.payload) {
          item.done = !item.done
        }
      })
      return newState
    default:
      return state
  }
}
export default reducer