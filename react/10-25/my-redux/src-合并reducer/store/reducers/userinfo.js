import { CHANGE_USER, ADD_AGE, SUB_AGE } from '../actions/action-type'

const initState = {
  age: 20,
  user: '小明'
}

const reducer = (state = initState, action) => {
  const newState = JSON.parse(JSON.stringify(state))
  switch(action.type) {
    case CHANGE_USER:
      newState.user = action.payload
      return newState
    case ADD_AGE:
      newState.age ++
      return newState
    case SUB_AGE:
      newState.age --
      return newState
    default:
      return state
  }
}
export default reducer