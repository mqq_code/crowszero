export const CHANGE_USER = 'CHANGE_USER' // 修改用户名
export const ADD_AGE = 'ADD_AGE' // 增加年龄
export const SUB_AGE = 'SUB_AGE' // 减少年龄
export const ADD_LIST = 'ADD_LIST' // 添加列表
export const DONE_LIST = 'DONE_LIST' // 修改列表