import React, { useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { addListAction, doneListAction } from '../store/actions'

const Child1 = () => {

  const list = useSelector(s => s.list)
  const dispatch = useDispatch()
  const inp = useRef()

  const add = () => {
    dispatch(addListAction(inp.current.value))
    inp.current.value = ''
  }
  const done = (id) => {
    dispatch(doneListAction(id))
  }
  return (
    <div>
      <input type="text" ref={inp} />
      <button onClick={add}>添加</button>
      <ul>
        {list.map(item =>
          <li key={item.id}>
            {item.done ? <s>{item.text}</s> : item.text}
            <button onClick={() => done(item.id)}>删除</button>
          </li>
        )}
      </ul>
    </div>
  )
}

export default Child1