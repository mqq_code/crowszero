import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { changeUserAction, addAgeAction, subAgeAction } from '../store/actions'

const Child2 = () => {
  const user = useSelector(s => s.userinfo.user)
  const age = useSelector(s => s.userinfo.age)
  const dispatch = useDispatch()
  const changeUser = e => {
    dispatch(changeUserAction(e.target.value))
  }
  const add = () => {
    dispatch(addAgeAction())
  }
  const sub = () => {
    dispatch(subAgeAction())
  }
  return (
    <div>
      <div>姓名: {user} <input type="text" value={user} onChange={changeUser} /></div>
      <div>年龄:
        <button onClick={sub}>-</button>
        {age}
        <button onClick={add}>+</button>
      </div>
    </div>
  )
}

export default Child2