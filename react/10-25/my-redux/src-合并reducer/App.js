import React from 'react'
import './App.scss'
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import Home from './pages/Home'
import Child1 from './pages/Child1'
import Child2 from './pages/Child2'

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" render={routeInfo => (
          <Home {...routeInfo}>
            <Switch>
              <Route path="/child1" component={Child1} />
              <Route path="/child2" component={Child2} />
              <Redirect from="/" to="/child1" />
            </Switch>
          </Home>
        )} />
      </Switch>
    </BrowserRouter>
  )
}

export default App