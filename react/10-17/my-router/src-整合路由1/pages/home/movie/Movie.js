import React from 'react'
import style from './movie.module.scss'
import { NavLink } from 'react-router-dom'

const Movie = (props) => {
  return (
    <div className={style.movie}>
      <nav>
        <NavLink to="/movie/hot">正在热映</NavLink>
        <NavLink to="/movie/coming">即将上映</NavLink>
      </nav>
      {props.children}
    </div>
  )
}

export default Movie