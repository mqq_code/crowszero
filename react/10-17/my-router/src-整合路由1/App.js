import React from 'react'
import './App.scss'
import {
  Route,
  Switch,
  Redirect
} from 'react-router-dom'
import Detail from './pages/detail/Detail'
import Login from './pages/login/Login'
import City from './pages/city/City'
import Home from './pages/home/Home'
import Cinema from './pages/home/cinema/Cinema'
import News from './pages/home/news/News'
import Mine from './pages/home/mine/Mine'
import Movie from './pages/home/movie/Movie'
import Hot from './pages/home/movie/hot/Hot'
import Coming from './pages/home/movie/coming/Coming'

const App = () => {
  return (
    <Switch>
      <Route exact path="/detail" component={Detail} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/city" component={City} />
      <Route path="/" render={() => {
        return (
          <Home>
            <Switch>
              <Route path="/movie" render={() => {
                return (
                  <Movie>
                    <Switch>
                      <Route exact path="/movie/hot" component={Hot} />
                      <Route exact path="/movie/coming" component={Coming} />
                      <Redirect from="/movie" to="/movie/hot" />
                    </Switch>
                  </Movie>
                )
              }} />
              <Route exact path="/cinema" component={Cinema} />
              <Route exact path="/news" component={News} />
              <Route exact path="/Mine" component={Mine} />
              <Redirect from="/" to="/movie" />
            </Switch>
          </Home>
        )
      }} />
    </Switch>
  )
}

export default App