import React from 'react'
import './App.scss'
import routes from './router/routerConfig'
import RouterView from './router/RouterView'

const App = () => {
  return (
    <React.Suspense fallback={<div style={{ color: 'green', fontSize: 30 }}>loading...</div>}>
      <RouterView routes={routes} />
    </React.Suspense>
  )
}

export default App