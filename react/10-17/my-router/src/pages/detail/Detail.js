import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'
import query from '../../utils/query'

const Detail = (props) => {
  // console.log('获取search参数', query(props.location.search).filmId)
  // console.log('获取state参数', props.location.state)
  // console.log('获取动态路由参数', props.match.params.filmId)

  const params = useParams()
  console.log('使用hook获取动态路由参数', params)

  const [info, setInfo] = useState({})

  const getInfo = async () => {
    const res = await axios.get('/list/detail', {
      params: {
        filmId: props.match.params.filmId
      }
    })
    if (res.data.status === 0) {
      setInfo(res.data.data)
    } else {
      alert(res.data.msg)
    }
  }

  useEffect(() => {
    getInfo()
  }, [])

  return (
    <div>
      <h1>{info.name}</h1>
      <img src={info.poster} alt="" />
    </div>
  )
}

export default Detail