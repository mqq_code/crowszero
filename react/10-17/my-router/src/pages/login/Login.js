import React from 'react'
import query from '../../utils/query'

const Login = (props) => {
  const login = () => {
    localStorage.setItem('token', '123456')

    // 获取要跳转的地址
    const fullpath = query(props.location.search).fullpath || '/'
    props.history.push(fullpath)
  }
  return (
    <div>
      <button onClick={login}>登陆</button>
    </div>
  )
}

export default Login