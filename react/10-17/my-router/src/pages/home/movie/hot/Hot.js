import React, { useEffect, useState, useCallback } from 'react'
import axios from 'axios'
import style from './hot.module.scss'
import watch from '../../../../watch'
import MovieInfo from './components/MovieInfo'

const Hot = (props) => {
  const [total, setTotal] = useState(null)
  const [films, setFilms] = useState([])
  const [pageNum, setPageNum] = useState(1)

  const getList = useCallback(async () => {
    // 列表数量超过总数就停止请求接口
    if (total !== null && films.length >= total) return
    const res = await axios.get('/list/hot', {
      params: {
        pageNum,
        pageSize: 10
      }
    })
    if (res.data.status === 0) {
      setTotal(res.data.data.total)
      setFilms(films => [...films, ...res.data.data.films])
      setPageNum(pageNum + 1)
    } else {
      alert(res.data.msg)
    }
  }, [pageNum, total])

  useEffect(() => {
    getList()
  }, [])

  useEffect(() => {
    watch.on('getList', getList)
  }, [getList])

  return (
    <div className={style.hot}>
      {films.map(item =>
        <MovieInfo key={item.filmId} {...item} />
      )}
    </div>
  )
}

export default Hot