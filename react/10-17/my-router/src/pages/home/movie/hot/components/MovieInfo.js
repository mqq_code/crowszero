import React from 'react'
import style from '../hot.module.scss'
import { withRouter, useHistory, useLocation, useParams } from 'react-router-dom'

const MovieInfo = (props) => {

  const h = useHistory() // 获取路由中的history对象
  const ll = useLocation() // 获取路由中的location对象
  const params = useParams() // 获取路由中的动态路由参数

  const goDetail = () => {
    // 传参方式一：通过search传参
    let url = 'https://static.maizuo.com/pc/v5/usr/movie/f31367bb1a275f032ea3793a0571d9e0.jpg?name=王小明&age=100'
    // encodeURIComponent: 转译特殊字符
    // 如果通过search 传递 url 路径，必须通过 encodeURIComponent 转译特殊字符
    // props.history.push(`/detail?url=${encodeURIComponent(url)}&filmId=${filmId}&a=100&b=200`)
  
    
    // 传参方式二: 通过 state 传参，浏览器模式刷新页面还在，hash模式刷页面参数清空
    // props.history.push({
    //   pathname: '/detail',
    //   state: { filmId }
    // })

    // 传参方式三：动态路由传参
    props.history.push(`/detail/${props.filmId}?url=${encodeURIComponent(url)}&filmId=${props.filmId}&a=100&b=200`)

    // console.log(props)
    // console.log(h)
    // console.log(ll)
    // console.log(params) 
  }

  return (
    <div
      className={style.film}
      onClick={goDetail}
    >
      <img src={props.poster} alt="" />
      <div className={style.info}>
        {props.name} - {props.filmId}
      </div>
    </div>
  )
}

// withRouter: 高阶组件，给组件的props添加路由信息
export default withRouter(MovieInfo)