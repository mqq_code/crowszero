import React from 'react'
// import Test from './components/Test'

// 异步加载组件，必须配合 React.Suspense 使用
const Test = React.lazy(() => import(/* webpackChunkName: "test" */'./components/Test'))

const Coming = () => {
  return (
    <div>
      <h1>coming</h1>
      {/* fallback: 内部一步组件未加载完成之前显示的替换内容 */}
      <React.Suspense fallback={<div style={{ color: 'green', fontSize: 30 }}>loading...</div>}>
        <Test />
      </React.Suspense>
    </div>
  )
}

export default Coming