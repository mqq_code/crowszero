import React, { useState, useCallback } from 'react'
import style from './movie.module.scss'
import { NavLink, Link } from 'react-router-dom'
import watch from '../../../watch'


const Movie = (props) => {
  const [showTitle, setShow] = useState(false)

  const scroll = useCallback((e) => {
    const { scrollTop, clientHeight, scrollHeight } = e.target
    if (scrollTop + clientHeight + 1 >= scrollHeight ) {
      console.log('到底了～')
      // 利用发布订阅模式调用hot中的方法
      watch.emit('getList')
    }
    if (scrollTop > 200) {
      setShow(true)
    } else {
      setShow(false)
    }
  }, [])

  return (
    <div className={style.movie} onScroll={scroll}>
      {showTitle &&
        <div className={style.title}>
          <span><Link to="/city">城市</Link></span>
          <h2>电影</h2>
        </div>
      }
      <nav>
        <NavLink to="/movie/hot">正在热映</NavLink>
        <NavLink to="/movie/coming">即将上映</NavLink>
      </nav>
      {props.children}
    </div>
  )
}

export default Movie