import React from 'react'
import style from './home.module.scss'
import { NavLink } from 'react-router-dom'

const navlist = [
  {
    path: '/movie',
    title: '电影'
  },
  {
    path: '/cinema',
    title: '影院'
  },
  {
    path: '/news',
    title: '咨询'
  },
  {
    path: '/mine',
    title: '我的'
  }
]

const Home = (props) => {
  return (
    <div className={style.home}>
      <main>
        {props.children}
      </main>
      <footer>
        {navlist.map(item =>
          <NavLink key={item.path} to={item.path}>{item.title}</NavLink>
        )}
      </footer>
    </div>
  )
}

export default Home