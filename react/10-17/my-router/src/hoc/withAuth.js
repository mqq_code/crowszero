import { Redirect } from 'react-router-dom'

export default (Com) => {
  const withAuth = (props) => {
    const token = localStorage.getItem('token')
    const fullpath = props.location.pathname + props.location.search
    if (token) {
      return <Com {...props} />
    } else {
      return <Redirect to={`/login?fullpath=${encodeURIComponent(fullpath)}&a=aa`} />
    }
  }
  return withAuth
}