import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import {
  // HashRouter, // 路由根组件，hash模式
  BrowserRouter // 路由根组件，history模式
} from 'react-router-dom'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
);