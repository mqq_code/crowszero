class Watch {
  dep = {}

  on = (eventName, callback) => {
    this.dep[eventName] = callback
  }
  emit = (eventName, ...rest) => {
    this.dep[eventName] && this.dep[eventName](...rest)
  }
}

export default new Watch()
