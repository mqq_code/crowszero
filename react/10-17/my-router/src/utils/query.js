
const query = str => {
  let arr = str.slice(1).split('&')
  let obj = {}
  arr.forEach(item => {
    const [key, val] = item.split('=')
    // decodeURIComponent: 解密特殊字符
    obj[key] = decodeURIComponent(val)
  })
  return obj
}

export default query