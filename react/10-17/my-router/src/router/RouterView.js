import React from 'react'
import {
  Route,
  Switch,
  Redirect
} from 'react-router-dom'

const RouterView = (props) => {
  const routes = props.routes.filter(v => v.component)
  const redirects = props.routes.filter(v => v.to)
  // console.log('routes', routes)
  // console.log('redirects', redirects)
  return (
    <Switch>
      {routes.map(item =>
        <Route
          key={item.path}
          exact={item.exact}
          path={item.path}
          render={routeInfo => {
            // routeInfo: 路由信息对象
            return <item.component {...routeInfo}>
              {item.children ?
                <React.Suspense fallback={<div style={{ color: 'green', fontSize: 30 }}>loading...</div>}>
                  <RouterView routes={item.children}/>
                </React.Suspense>
              : null}
            </item.component>
          }}
        />
      )}

      {redirects.map(item =>
        <Redirect key={item.from} exact={item.exact} from={item.from} to={item.to} />
      )}
    </Switch>
  )
}

export default RouterView