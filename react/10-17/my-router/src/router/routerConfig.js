import { lazy } from 'react'
// import Detail from '../pages/detail/Detail'
import Login from '../pages/login/Login'
import City from '../pages/city/City'
import Home from '../pages/home/Home'

import Cinema from '../pages/home/cinema/Cinema'
import News from '../pages/home/news/News'
import Mine from '../pages/home/mine/Mine'
import Movie from '../pages/home/movie/Movie'

import Hot from '../pages/home/movie/hot/Hot'
import Coming from '../pages/home/movie/coming/Coming'

import withAuth from '../hoc/withAuth'

const Detail = lazy(() => import(/* webpackChunkName: "detail" */'../pages/detail/Detail'))


const routes = [
  {
    exact: true,
    path: '/detail/:filmId', // 动态路由
    component: withAuth(Detail)
  },
  {
    exact: true,
    path: '/login',
    component: Login
  },
  {
    exact: true,
    path: '/city',
    component: City
  },
  {
    path: '/',
    component: Home,
    children: [
      {
        exact: true,
        path: '/cinema',
        component: Cinema
      },
      {
        exact: true,
        path: '/news',
        component: News
      },
      {
        exact: true,
        path: '/mine',
        component: withAuth(Mine)
      },
      {
        path: '/movie',
        component: Movie,
        children: [
          {
            exact: true,
            path: '/movie/hot',
            component: Hot
          },
          {
            exact: true,
            path: '/movie/coming',
            component: Coming
          },
          {
            from: '/movie',
            to: '/movie/hot'
          }
        ]
      },
      {
        from: '/',
        to: '/movie'
      }
    ]
  }
]


export default routes