import React from 'react'
import Home from './pages/Home'
import Detail from './pages/Detail'
import Login from './pages/Login'
import NotFound from './pages/404'
import {
  Route, // 渲染路由视图
  Redirect, // 重定向组件
  Switch, // 只渲染匹配成功的第一个组件
  Link, // 跳转路由组件，没有高亮类名
  NavLink // 导航跳转路由组件，有高亮类名
} from 'react-router-dom'
import './App.css'

const App = () => {
  return (
    <div>
      <header>
        <NavLink activeClassName="abc" activeStyle={{ fontSize: '30px' }} to="/home">首页</NavLink>
        <NavLink activeClassName="abc" activeStyle={{ fontSize: '30px' }}  to="/detail">详情</NavLink>
        <NavLink activeClassName="abc" activeStyle={{ fontSize: '30px' }}  to="/login">登陆</NavLink>
      </header>
      <hr />
      <Switch>
        <Route path="/home" component={Home} />
        <Route path="/detail" component={Detail} />
        <Route path="/login" component={Login} />
        <Route path="/404" component={NotFound} />
        <Redirect exact from="/" to="/home" />
        <Redirect from="*" to="/404" />
      </Switch>
    </div>
  )
}

export default App