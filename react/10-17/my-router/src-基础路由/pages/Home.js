import React from 'react'
import Child1 from './Child1'
import Child2 from './Child2'
import {
  Route,
  Switch,
  Redirect,
  NavLink
} from 'react-router-dom'

const Home = () => {
  return (
    <div>
      <h2>Home</h2>
      <nav>
        <NavLink to="/home/child1">子页面1</NavLink>
        <NavLink to="/home/child2">子页面2</NavLink>
      </nav>
      <Switch>
        <Route path="/home/child1" component={Child1} />
        <Route path="/home/child2" component={Child2} />
        <Redirect from="/home" to="/home/child1" />
      </Switch>
    </div>
  )
}

export default Home