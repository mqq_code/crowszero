import React from 'react'
import './App.scss'
import {
  Route,
  Switch
} from 'react-router-dom'
import Home from './pages/home/Home'
import Detail from './pages/detail/Detail'
import Login from './pages/login/Login'
import City from './pages/city/City'

const App = () => {
  return (
    <Switch>
      <Route exact path="/login" component={Login} />
      <Route exact path="/detail" component={Detail} />
      <Route exact path="/city" component={City} />
      <Route path="/" component={Home} />
    </Switch>
  )
}

export default App