import React from 'react'
import './App.scss'
import {
  Route,
  Switch
} from 'react-router-dom'
import Home from './pages/home/Home'
import Detail from './pages/detail/Detail'
import Login from './pages/login/Login'
import City from './pages/city/City'

const App = () => {
  return (
    <Switch>
      {/* <Route exact path="/detail" component={Detail} /> */}
      <Route exact path="/detail" render={() => {
        return <Detail>
          <ul>
            <li>1</li>
            <li>2</li>
            <li>3</li>
            <li>4</li>
            <li>5</li>
            <li>6</li>
            <li>7</li>
            <li>8</li>
            <li>9</li>
            <li>10</li>
          </ul>
        </Detail>
      }} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/city" component={City} />
      <Route path="/" component={Home} />
    </Switch>
  )
}

export default App