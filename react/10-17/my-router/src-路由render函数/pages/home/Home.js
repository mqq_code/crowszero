import React from 'react'
import style from './home.module.scss'
import {
  Route,
  Switch,
  Redirect,
  NavLink
} from 'react-router-dom'
import Movie from './movie/Movie'
import Cinema from './cinema/Cinema'
import News from './news/News'
import Mine from './mine/Mine'

const navlist = [
  {
    path: '/movie',
    title: '电影'
  },
  {
    path: '/cinema',
    title: '影院'
  },
  {
    path: '/news',
    title: '咨询'
  },
  {
    path: '/mine',
    title: '我的'
  }
]

const Home = () => {
  return (
    <div className={style.home}>
      <main>
        <Switch>
          <Route path="/movie" component={Movie} />
          <Route exact path="/cinema" component={Cinema} />
          <Route exact path="/news" component={News} />
          <Route exact path="/Mine" component={Mine} />
          <Redirect from="/" to="/movie" />
        </Switch>
      </main>
      <footer>
        {navlist.map(item =>
          <NavLink key={item.path} to={item.path}>{item.title}</NavLink>
        )}
        {/* {navlist.map(item =>
          <NavLink activeClassName={style.active} key={item.path} to={item.path}>{item.title}</NavLink>
        )} */}
      </footer>
    </div>
  )
}

export default Home