import React from 'react'
import style from './movie.module.scss'
import Hot from './hot/Hot'
import Coming from './coming/Coming'
import {
  Switch,
  Route,
  Redirect,
  NavLink
} from 'react-router-dom'

const Movie = () => {
  return (
    <div className={style.movie}>
      <nav>
        <NavLink to="/movie/hot">正在热映</NavLink>
        <NavLink to="/movie/coming">即将上映</NavLink>
      </nav>
      <Switch>
        <Route exact path="/movie/hot" component={Hot} />
        <Route exact path="/movie/coming" component={Coming} />
        <Redirect from="/movie" to="/movie/hot" />
      </Switch>
    </div>
  )
}

export default Movie