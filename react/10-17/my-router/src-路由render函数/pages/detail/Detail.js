import React from 'react'

const Detail = (props) => {
  return (
    <div>
      <h1>Detail</h1>
      {props.children}
    </div>
  )
}

export default Detail