import path from 'path'
import { defineConfig } from 'umi';
import routes from './routes'

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: routes.routes,
  fastRefresh: {},
  history: {
    type: 'hash',
  },
  alias: {
    'zyx': path.join(__dirname, '../src/utils')
  },
  dva: {
    immer: true,
    hmr: true,
  },
});
