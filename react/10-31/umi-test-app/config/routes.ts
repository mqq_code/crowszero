import { defineConfig } from 'umi'

export default defineConfig({
  routes: [
    { exact: true, title: '登陆', path: '/login', component: '@/pages/login' },
    { exact: true, title: '详情',  path: '/detail/:id', component: '@/pages/detail', wrappers: ['@/wrappers/auth'] },
    { exact: true, title: '404',  path: '/404', component: '@/pages/404' },
    {
      path: '/',
      component: '@/layouts/home',
      routes: [
        { exact: true, title: '电影',  path: '/', component: '@/pages/movie' },
        { exact: true, title: '影院',  path: '/cinema', component: '@/pages/cinema' },
        { exact: true, title: '咨询',  path: '/news', component: '@/pages/news' },
        { exact: true, title: '我的',  path: '/mine', component: '@/pages/mine' },
        { redirect: '/404' }
      ]
    }
  ]
})
