import { IRouteComponentProps, Redirect } from "umi"


const auth = (props: IRouteComponentProps) => {
  let token = localStorage.getItem('token')

  if (token) {
    return props.children
  }

  return (
    <Redirect to="/login" />
  )
}

export default auth