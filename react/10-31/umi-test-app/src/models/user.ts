import { Effect, Subscription } from 'dva';

export interface IUserState {
  list: number[];
}

export interface UserType {
  namespace: 'user';
  state: IUserState;
  effects: {
    getList: Effect;
  };
  reducers: {
    // 启用 immer 之后
    setList: any;
  };
  subscriptions: { setup: Subscription };
}

const User: UserType = {
  namespace: 'user',

  state: {
    list: []
  },

  effects: {
    *getList() {}
  },
  reducers: {
    setList() {}
  },
  subscriptions: {
    setup() {
    },
  },
};

export default User;