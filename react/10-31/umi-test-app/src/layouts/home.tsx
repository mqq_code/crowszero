import { IRouteComponentProps } from 'umi'

const Home = (props: IRouteComponentProps) => {

  return (
    <div>
      <main>{props.children}</main>
      <footer>底部</footer>
    </div>
  )
}

export default Home