import axios from 'axios'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'dva'

const Movie = () => {

  const state = useSelector(s => s)
  const dispath = useDispatch()

  console.log(state)

  useEffect(() => {
    axios.get('/api/list').then(res => {
      // console.log(res.data)
      dispath({
        type: 'user/getList'
      })
    })
  }, [])

  return (
    <div>Movie</div>
  )
}

export default Movie