import { Prompt } from 'umi'

const Cinema = () => {
  return (
    <div>
      <Prompt message="你确定要离开么？" />
    </div>
  )
}

export default Cinema