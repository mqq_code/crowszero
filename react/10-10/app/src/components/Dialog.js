import React, { Component, Children } from 'react'
// css module
import style from './dialog.module.scss'

console.log(Children)

class Dialog extends Component {

  // 默认参数
  static defaultProps = {
    title: '默认title'
  }

  render() {

    // console.log(Children.toArray(this.props.children))
    // console.log(Children.map(this.props.children, item => <div>{item}</div>))
    // console.log(Children.only(this.props.children))

    return (
      <div className={style.dialog}>
        <div className={style.content}>
          {this.props.title}

          <div className={style.header}>
            {this.props.header ? this.props.header : <h1>默认标题</h1>}
          </div>
          <div className={style.main}>
            

            {/* {Children.toArray(this.props.children).map(item =>
              <div style={{ background: 'red', marginBottom: 10 }}>
                {item}
              </div>
            )} */}
            {Children.map(this.props.children, item =>
              <div style={{ background: 'red', marginBottom: 10 }}>
                {item}
              </div>
            )}



          </div>
          <div className={style.footer}>
            {this.props.footer}
          </div>
          <div className={style.close}>x</div>
        </div>
        {/* <button className={[style.btn, style.red].join(' ')}>aaaaa</button> */}
      </div>
    )
  }
}

export default Dialog