import React, { Component } from 'react'
import Child11 from './Child1-1'
import { Consumer } from '../context/font'

class Child1 extends Component {
  render() {
    return (
      <Consumer>
        {({ fontSize }) =>
          <div className='desc'>
            <h3>悯农</h3>
            <p style={{ fontSize }}>锄禾日当午，汗滴禾下土。谁知盘中餐，粒粒皆辛苦。</p>
            <Child11 />
          </div>
        }
      </Consumer>
    )
  }
}

export default Child1