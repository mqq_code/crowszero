import React, { Component } from 'react'
import { Consumer } from '../context/font'

class Child3 extends Component {
  render() {
    return (
      <Consumer>
        {({ fontSize }) =>
          <div className='desc' style={{ fontSize }}>
            床前明月光，疑是地上霜。举头望明月，低头思故乡。
          </div>
        }
      </Consumer>
    )
  }
}

export default Child3