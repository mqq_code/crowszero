import React, { Component } from 'react'
import { Consumer } from '../context/font'

class Child3 extends Component {
  render() {
    return (
      <Consumer>
        {({ fontSize, changeSize }) =>
          <div className='desc' >
            <p style={{ fontSize }}>日照香炉生紫烟，遥看瀑布挂前川。飞流直下三千尺，疑是银河落九天。</p>
            修改字号：<button onClick={() => changeSize(1)}>+</button> {fontSize}
          </div>
        }
      </Consumer>
    )
  }
}

export default Child3