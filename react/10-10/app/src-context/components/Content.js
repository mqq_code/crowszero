import React, { Component } from 'react'
import Child1 from './Child1'
import Child2 from './Child2'
import Child3 from './Child3'

class Content extends Component {
  render() {
    return (
      <div>
        <h2>第一段</h2>
        <Child1 />
        <hr />

        <h2>第二段</h2>
        <Child2 />
        <hr />

        <h2>第三段</h2>
        <Child3 />
      </div>
    )
  }
}

export default Content