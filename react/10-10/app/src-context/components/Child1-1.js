import React, { Component } from 'react'
import { Consumer } from '../context/font'

class Child11 extends Component {
  render() {
    return (
      <Consumer>
        {value =>
          <div>
            <h4>我是：Child1-1</h4>
            <p style={{ fontSize: value.fontSize }}>asdkjflakjladskfjalskldkafklads</p>
          </div>
        }
      </Consumer>
    )
  }
}

export default Child11