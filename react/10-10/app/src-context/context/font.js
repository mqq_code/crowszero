import { createContext } from 'react'

const fontCtx = createContext()

export const Provider = fontCtx.Provider
export const Consumer = fontCtx.Consumer
export default fontCtx