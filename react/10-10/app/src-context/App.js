import React, { Component, createContext } from 'react'
import './App.scss'
import Content from './components/Content'
import { Provider } from './context/font'


class App extends Component {
  state = {
    fontSize: 14
  }
  changeSize = n => {
    this.setState({
      fontSize: this.state.fontSize + n
    })
  }

  

  render() {
    const { fontSize } = this.state
    const value = {
      fontSize,
      changeSize: this.changeSize,
      aaa: 'aaa',
      bbb: true
    }
    
    return (
      <Provider value={value}>
        <div className="app">
          <h1>context 嵌套传参</h1>
          <div className="font-size">
            字号：
            <button onClick={() => this.changeSize(-1)}>-</button>
            {fontSize}
            <button onClick={() => this.changeSize(1)}>+</button>
          </div>
          <Content />
        </div>
      </Provider>
    )
  }
}

export default App
