import React, { Component } from 'react'

class Tag extends Component {
  render() {
    return (
      <div className="tag" style={{ background: this.props.color }}>
        <span onClick={this.props.remove}>x</span>
        {this.props.text}
      </div>
    )
  }
}

export default Tag
