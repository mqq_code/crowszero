import React, { Component } from 'react'

class TagColor extends Component {

  render() {
    return (
      <div className="tagcolor">
        <span>Tag Color:</span>
        {this.props.colors.map(color =>
          <i
            key={color}
            style={{ background: color }}
            onClick={() => this.props.changeColor(color)}
          ></i>
        )}
      </div>
    )
  }
}

export default TagColor