import React, { Component } from 'react'
import './App.scss'
import TagColor from './components/TagColor'
import Tag from './components/Tag'

class App extends Component {
  colors = ['#F95', '#6AD', '#E77', '#6C7', '#AAA']
  state = {
    color: this.colors[0],
    tags: []
  }
  changeColor = color => {
    this.setState({ color })
  }
  addTag = (e) => {
    if (e.keyCode === 13 && e.target.value.trim() !== '') {
      this.setState({
        tags: [...this.state.tags, {
          id: Date.now(),
          text: e.target.value
        }]
      })
      e.target.value = ''
    }
  }
  removeTag = id => {
    this.setState({
      tags: this.state.tags.filter(item => item.id !== id)
    })
  }

  render() {
    const { color, tags } = this.state
    return (
      <div className="app">
        <TagColor colors={this.colors} changeColor={this.changeColor} />
        <div className="content">
          <h1><span style={{ color }}>tags</span>.css</h1>
          <div className="desc">按回车确认</div>
          <div className="tag-wrap">
            {tags.map(item =>
              <Tag key={item.id} color={color} text={item.text} remove={() => this.removeTag(item.id)} />
            )}
            <input type="text" placeholder='add tag' onKeyDown={this.addTag} />
          </div>
        </div>
      </div>
    )
  }
}

export default App
