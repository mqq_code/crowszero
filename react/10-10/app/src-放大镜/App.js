import React, { Component, createRef } from 'react'
import './App.scss'

const imgs = [
  require('./img/1.jpg'),
  require('./img/2.jpg'),
  require('./img/3.jpg'),
  require('./img/4.jpg'),
  require('./img/5.jpg')
]

class App extends Component {

  imgWrap = createRef()

  state = {
    curIndex: 0,
    show: false,
    left: 0,
    top: 0
  }
  changeTab = curIndex => {
    this.setState({ curIndex })
  }
  
  mouseEnter = () => {
    this.setState({
      show: true
    })
  }
  mouseLeave = () => {
    this.setState({
      show: false
    })
  }
  mouseMove = (e) => {
    // 获取鼠标到页面左侧和顶部的距离
    const { clientX, clientY } = e.nativeEvent
    // 获取p标签到页面左侧和顶部的距离
    const { left, top, width, height } = this.imgWrap.current.getBoundingClientRect()
    // 计算鼠标到 p 标签的距离，减去蒙层的一半宽高
    let spanLeft = clientX - left - 50
    let spanTop = clientY - top - 50
    // 计算边界
    if (spanLeft < 0) spanLeft = 0
    if (spanTop < 0) spanTop = 0
    if (spanLeft > width - 100) spanLeft = width - 100
    if (spanTop > height - 100) spanTop = height - 100
    this.setState({
      left: spanLeft,
      top: spanTop
    })
  }

  render() {
    const { curIndex, show, left, top } = this.state
    return (
      <div className="app">
        <div className="wrap">
          <div className="small">
            <p ref={this.imgWrap} onMouseEnter={this.mouseEnter} onMouseLeave={this.mouseLeave} onMouseMove={this.mouseMove}>
              <img src={imgs[curIndex]} alt="" />
              {show && <span style={{ left, top }}></span>}
            </p>
          </div>
          <nav>
            {imgs.map((url, index) =>
              <p
                className={curIndex === index ? 'active' : ''}
                key={url}
                onClick={() => this.changeTab(index)}
              >
                <img src={url} alt="" />
              </p>
            )}
          </nav>
          {show &&
            <div className="big">
              <img style={{ left: left * -4, top: top * -4 }} src={imgs[curIndex]} alt="" />
            </div>
          }
        </div>
      </div>
    )
  }
}

export default App
