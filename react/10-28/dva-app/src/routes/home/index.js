import React from 'react'
import { NavLink } from 'dva/router'
import style from './index.css'

const Home = (props) => {
  return (
    <div className={style.home}>
      <nav>
        <NavLink to="/">child1</NavLink>
        <NavLink to="/child2">child2</NavLink>
      </nav>
      {props.children}
    </div>
  )
}

export default Home