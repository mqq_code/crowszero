import React, { useEffect, useRef } from 'react'
import { connect } from 'dva'

const Child2 = (props) => {
  console.log(props.songs)

  const inp = useRef()
  const submit = () => {
    props.dispatch({
      type: 'user/search',
      payload: inp.current.value
    })
  }

  return (
    <div>
      <h2>child2</h2>
      <h2>当前鼠标的位置 x: {props.mouseX}, y: {props.mouseY}</h2>
      <input type="text" ref={inp} />
      <button onClick={submit}>搜索</button>
      <ul>
        {props.songs.map(item =>
          <li key={item.id}>{item.name}</li>
        )}
      </ul>

      {/* <p>姓名：{props.name}</p>
      <p>年龄: {props.age}</p>
      <p>count: {props.count}</p>
      <p>page: {props.page}</p> */}
    </div>
  )
}

const mapState = state => {
  return {
    ...state.user,
    ...state.count
  }
}
export default connect(mapState)(Child2)