import React, { useEffect } from 'react'
import { connect } from 'dva'

const Child1 = (props) => {

  const changeAge = (n) => {
    props.dispatch({
      type: 'user/changeAge',
      payload: props.age + n
    })
  }
  const changeCount = (n) => {
    props.dispatch({
      type: 'count/changeCount',
      payload: props.count + n
    })
  }

  useEffect(() => {
    props.dispatch({
      type: 'user/getBanner'
    })
  }, [])

  return (
    <div>
      <h2>child1</h2>
      <p>姓名：{props.name}</p>
      <p>年龄: {props.age}
        <button onClick={() => changeAge(-1)}>-</button>
        <button onClick={() => changeAge(1)}>+</button>
      </p>
      <p>count: {props.count}
        <button onClick={() => changeCount(-1)}>-</button>
        <button onClick={() => changeCount(1)}>+</button>
      </p>
      <p>page: {props.page}</p>
      {props.banners.map(item => <img key={item.imageUrl} src={item.imageUrl} />)}
    </div>
  )
}

const mapState = state => {
  return {
    ...state.user,
    ...state.count
  }
}
export default connect(mapState)(Child1)