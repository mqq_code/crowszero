import request from '../utils/request';

export function getBanner() {
  return request('https://zyxcl-music-api.vercel.app/banner');
}

export function search(keywords) {
  return request('https://zyxcl-music-api.vercel.app/search?keywords=' + keywords);
}