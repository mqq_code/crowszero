import { getBanner, search } from '../services'

export default {

  namespace: 'user',

  state: {
    name: '小明',
    age: 20,
    banners: [],
    songs: [], // 搜索的歌曲列表
    mouseX: 0,
    mouseY: 0
  },

  reducers: {
    changeAge(state, { payload }) {
      return { ...state, age: payload };
    },
    changeName(state, { payload }) {
      return { ...state, name: payload };
    },
    setBanners(state, { payload }) {
      return { ...state, banners: payload }
    },
    setSongs(state, { payload }) {
      return { ...state, songs: payload }
    },
    setMouse(state, { payload }) {
      const { x, y } = payload
      return { ...state, mouseX: x, mouseY: y }
    }
  },

  effects: {
    *getBanner(action, { call, put }) {  // eslint-disable-line
      const res = yield call(getBanner)
      console.log(res.data.banners)
      yield put({
        type: 'setBanners',
        payload: res.data.banners
      })
    },
    *search({ payload }, { call, put }) {
      try {
        const res = yield call(search, payload)
        yield put({
          type: 'setSongs',
          payload: res.data.result.songs
        })
      } catch(e) {
        // 处理错误
        alert(e.message)
      }
      
    }
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
      // 可以监听事件,监听路由

      document.addEventListener('mousemove', (e) => {
        const { pageX, pageY } = e
        dispatch({
          type: 'setMouse',
          payload: {
            x: pageX,
            y: pageY
          }
        })
      })

      history.listen(() => {
        console.log('监听路由改变', history.location)
      })
    },
  },

};
