
export default {

  namespace: 'count',

  state: {
    count: 100,
    page: 10
  },

  reducers: {
    changeCount(state, action) {
      return {...state, count: action.payload}
    }
  },

 

  effects: {
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

};
