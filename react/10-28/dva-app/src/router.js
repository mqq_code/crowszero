import React from 'react';
import { Router, Route, Switch, Redirect } from 'dva/router';
import Home from './routes/home';
import Detail from './routes/detail';
import Child1 from './routes/home/child1';
import Child2 from './routes/home/child2';

function RouterConfig({ history }) {
  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/detail/:id" component={Detail} />
        <Route path="/" render={routeInfo => {
          return <Home {...routeInfo}>
            <Switch>
              <Route exact path="/" component={Child1} />
              <Route exact path="/child2" component={Child2} />
              <Redirect to="/" />
            </Switch>
          </Home>
        }} />
      </Switch>
    </Router>
  );
}

export default RouterConfig;
