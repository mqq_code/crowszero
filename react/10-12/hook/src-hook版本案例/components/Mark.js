import React, { useState } from 'react'

const Mark = (props) => {
  const [out, setOut] = useState(false)

  const close = () => {
    setOut(true)
    setTimeout(() => {
      props.close()
    }, 1000)
  }

  return (
    <div className='mark'>
      {props.navlist.map(item =>
        <div key={item.title} className={out ? 'mark-item out' : 'mark-item'} >
          <img src={item.src} alt="" />
          <h3>{item.title}</h3>
          <p>{item.price}</p>
          <p>{item.alcohol}</p>
          <p>{item.region}</p>
          <p>{item.varietal}</p>
          <p>{item.year}</p>
        </div>
      )}
      <div className='close' onClick={close}>x</div>
    </div>
  )
}

export default Mark
