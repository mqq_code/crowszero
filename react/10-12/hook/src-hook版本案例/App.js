import React, { useState, useEffect } from 'react'
import './App.scss'
import Header from './components/Header'
import Mark from './components/Mark'
import Bottle from './components/Bottle'
import axios from 'axios'
import 'animate.css';

const App = () => {
  const [list, setList] = useState([])
  const [navlist, setNavlist] = useState([])
  const [show, setShow] = useState(false)

  const getList = async () => {
    const res = await axios.get('/list')
    setList(res.data)
  }

  useEffect(() => {
    getList()
  }, [])

  // 修改选中状态
  const changeChecked = (title) => {
    
    // 如果已经选中三个，并且再次点击的元素没有选中就不让点击
    if (navlist.length >= 3 && !navlist.find(v => v.title === title)) {
      return
    }

    // 拷贝数据
    let newNavlist = [...navlist]
    const newList = [...list]
    // 根据title查找点击的对象
    newList.forEach(item => {
      if (item.title === title) {
        item.checked = !item.checked
        // 选中了就添加到navlist，取消选中就删除
        if (item.checked) {
          newNavlist.push(item)
        } else {
          newNavlist = newNavlist.filter(v => v.title !== title)
        }
      }
    })
    // 更新数据
    setList(newList)
    setNavlist(newNavlist)
  }

  return (
    <div className='app'>
      <main>
        {list.map(item =>
          <Bottle key={item.title} {...item} change={changeChecked} />
        )}
      </main>
      {navlist.length > 0 &&
        <Header navlist={navlist} change={changeChecked} compare={setShow} />
      }
      {show && <Mark navlist={navlist} close={() => setShow(false)} />}
    </div>
  )
}

export default App
