import React, { useState, useEffect } from 'react'
import axios from 'axios'

// useEffect: 处理组件副作用，可以实现类似生命周期的功能
// 组件副作用: 跟组件渲染无关的操作，例如：调接口，定时器

const App = () => {
  const [count, setCount] = useState(10)
  const [flag, setFlag] = useState(false)
  const [list, setList] = useState([])

  // 第二个参数传入空数组，只执行一次，类似 componentDidMount
  useEffect(() => {
    console.log('组件加载完成')
    axios.get('/list').then(res => {
      setList(res.data)
    })
  }, [])

  // 组件加载完成执行一次，数据更新也会执行，类似 componentDidUpdate
  useEffect(() => {
    console.log('组件数据更新了')
  })

  // 具体的依赖项改变时执行函数
  useEffect(() => {
    console.log('count或者flag 改变时执行')
  }, [count, flag])


  return (
    <div>
      <h1>useEffect</h1>
      <button onClick={() => setCount(count - 1)}>-</button>
      {count}
      <button onClick={() => setCount(count + 1)}>+</button>

      <hr />
      <button onClick={() => setFlag(!flag)}>change</button> {flag ? '已婚' : '未婚'}
    </div>
  )
}

export default App