import React, { useState } from 'react'

// hook: 让函数组件可以实现类组件的功能
// hook使用规则：
// 1. 只能在函数最外层调用 Hook。不要在循环、条件判断或者子函数中调用。
// 2. 只能在react函数组件和自定义hook中调用 hook

const App = () => {
  // 定义数据 const [变量, 修改变量的方法] = useState(10)
  const [count, setCount] = useState(10)
  const [flag, setFlag] = useState(false)
  const [title, setTitle] = useState('App')
  const [arr, setArr] = useState(['a'])

  const changeTitle = e => {
    setTitle(e.target.value)
  }
  const add = () => {
    setArr([...arr, title])
  }

  return (
    <div>
      <h1>{title}</h1>  
      <input type="text" value={title} onChange={changeTitle} />
      <button onClick={add}>添加</button>
      <ul>
        {arr.map(item => <li key={item}>{item}</li>)}
      </ul>

      <hr />
      <button onClick={() => setCount(count - 1)}>-</button>
      {count}
      <button onClick={() => setCount(count + 1)}>+</button>

      <hr />
      <button onClick={() => setFlag(!flag)}>change</button> {flag ? '已婚' : '未婚'}
    </div>
  )
}

export default App