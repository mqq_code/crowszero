import React, { useState, useEffect } from 'react'

const Count = (props) => {
  // props: 父组件传过来的参数

  const [count, setCount] = useState(0)

  // useEffect(() => {
  //   console.log('props.num 改变了')
  // }, [props.num])

  useEffect(() => {
    const timer = setInterval(() => {
      console.log('定时器')
      // setCount(newCount => newCount + 1)
      setCount(newCount => {
        // 传入函数，函数的参数可以获取到最新的数据
        return newCount + 1
      })
    }, 1000)

    return () => {
      console.log('组件销毁清除定时器')
      // 组件销毁时执行，componentWillUnmount
      clearInterval(timer)
    }

  }, [])


  return (
    <div>
      <h2>子组件</h2>
      <div>父组件的参数：{props.num}</div>
      <div>计时器: {count}</div>
    </div>
  )
}

export default Count