import React, { useState, useEffect } from 'react'
import Count from './components/Count'

const App = () => {

  const [count, setCount] = useState(10)
  const [flag, setFlag] = useState(false)
  const [show, setShow] = useState(true)

  useEffect(() => {
    console.log('获取最新的count', count)
  }, [count])

  return (
    <div>
      <h1>useEffect</h1>
      <button onClick={() => setCount(count - 1)}>-</button>
      {count}
      <button onClick={() => {
        setCount(c => c + 1)
        setCount(c => c + 1)
        setCount(c => c + 1)
        setCount(c => c + 1)
        setCount(c => c + 1)
      }}>+</button>

      <hr />
      <button onClick={() => setFlag(!flag)}>change</button> {flag ? '已婚' : '未婚'}

      <hr />
      <button onClick={() => setShow(!show)}>显示隐藏</button>
      {show && <Count num={count} />}
    </div>
  )
}

export default App