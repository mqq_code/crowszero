import React, { Component } from 'react'
import './App.scss'
import Header from './components/Header'
import Mark from './components/Mark'
import Bottle from './components/Bottle'
import axios from 'axios'
import 'animate.css';

class App extends Component {

  state = {
    list: [],
    navlist: [],
    show: false
  }

  async componentDidMount() {
    const res = await axios.get('/list')
    this.setState({
      list: res.data
    })
  }

  // 修改选中状态
  changeChecked = (title) => {
    const list = [...this.state.list]
    let navlist = [...this.state.navlist]
    // 如果已经选中三个，并且再次点击的元素没有选中就不让点击
    if (navlist.length >= 3 && !navlist.find(v => v.title === title)) {
      return
    }

    list.forEach(item => {
      if (item.title === title) {
        item.checked = !item.checked
        if (item.checked) {
          navlist.push(item)
        } else {
          navlist = navlist.filter(v => v.title !== title)
        }
      }
    })

    this.setState({
      list,
      navlist
    })
  }
  changeShow = show => {
    this.setState({ show })
  }

  render() {
    const { list, navlist, show } = this.state
    return (
      <div className='app'>
        <main>
          {list.map(item =>
            <Bottle key={item.title} {...item} change={this.changeChecked} />
          )}
        </main>
        {navlist.length > 0 &&
          <Header navlist={navlist} change={this.changeChecked} compare={this.changeShow} />
        }
        {show && <Mark navlist={navlist} close={() => this.changeShow(false)} />}
      </div>
    )
  }
}

export default App
