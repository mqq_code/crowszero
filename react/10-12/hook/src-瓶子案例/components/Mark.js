import React, { Component } from 'react'

class Mark extends Component {

  state = {
    out: false
  }

  close = () => {
    this.setState({ out: true })
    setTimeout(() => {
      this.props.close()
    }, 1000)
  }

  render() {
    const { navlist } = this.props
    const { out } = this.state
    return (
      <div className='mark'>
        {navlist.map(item =>
          <div key={item.title} className={out ? 'mark-item out' : 'mark-item'} >
            <img src={item.src} alt="" />
            <h3>{item.title}</h3>
            <p>{item.price}</p>
            <p>{item.alcohol}</p>
            <p>{item.region}</p>
            <p>{item.varietal}</p>
            <p>{item.year}</p>
          </div>
        )}
        <div className='close' onClick={this.close}>x</div>
      </div>
    )
  }
}

export default Mark
