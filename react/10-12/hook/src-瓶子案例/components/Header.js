import React, { Component } from 'react'

class Header extends Component {
  render() {
    const { navlist, change, compare } = this.props
    return (
      <header>
        <ul>
          {navlist.map(item =>
            <li key={item.title}>
              <img src={item.src} alt="" />
              <span onClick={() => change(item.title)}><svg t="1665542715789" className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="6277" width="12" height="12"><path d="M550.848 502.496l308.64-308.896a31.968 31.968 0 1 0-45.248-45.248l-308.608 308.896-308.64-308.928a31.968 31.968 0 1 0-45.248 45.248l308.64 308.896-308.64 308.896a31.968 31.968 0 1 0 45.248 45.248l308.64-308.896 308.608 308.896a31.968 31.968 0 1 0 45.248-45.248l-308.64-308.864z" p-id="6278" fill="#cecece"></path></svg></span>
            </li>
          )}
          {navlist.length < 3 && <li></li>}
        </ul>
        <button onClick={() => compare(true)} disabled={navlist.length < 2}>比较</button>
      </header>
    )
  }
}

export default Header