import React, { useEffect, useState } from 'react'
import { NavLink, useHistory, useLocation } from 'react-router-dom'
import style from './header.module.scss'
import { routelist } from '../data/menu'

const Header = () => {
  const location = useLocation()
  const history = useHistory()
  const [navlist, setNavlist] = useState([])

  useEffect(() => {
    const cur = routelist.find(v => v.path === location.pathname)
    if (cur) {
      setNavlist(oldNav => {
        if (!oldNav.find(v => v.path === location.pathname)) {
          return [...oldNav, cur]
        } else {
          return oldNav
        }
      })
    }
  }, [location])

  const remove = (index) => {
    // 点击的页面和高亮页面一致时需要跳转页面
    if (navlist[index].path === location.pathname) {
      if (index - 1 >= 0) {
        history.push(navlist[index - 1].path)
      } else if (index + 1 < navlist.length) {
        history.push(navlist[index + 1].path)
      } else {
        localStorage.removeItem('token')
        history.push('/login')
      }
    }
    // 删除数据
    const newNav = [...navlist]
    newNav.splice(index, 1)
    setNavlist(newNav)
  }

  return (
    <header className={style.header}>
      <div className={style.top}></div>
      <div className={style.btm}>
        {navlist.map((item, index) =>
          <div className={style.navItem} key={item.path}>
            <NavLink activeClassName={style.active} to={item.path}>
              {item.title}
              <span onClick={e => {
                e.preventDefault()
                remove(index)
              }}>x</span>
            </NavLink>
          </div> 
        )}
      </div>
    </header>
  )
}

export default Header