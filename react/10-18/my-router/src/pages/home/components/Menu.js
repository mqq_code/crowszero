import React, { useEffect, useState } from 'react'
import style from './menu.module.scss'
import { tablist } from '../data/menu'
import { NavLink, useLocation, useHistory } from 'react-router-dom'


const Menu = (props) => {
  const location = useLocation()
  const history = useHistory()
  const [curIndex, setCurIndex] = useState(0)
  const [title, setTitle] = useState('')

  useEffect(() => {
    // 遍历tab数据
    tablist.forEach((item, index) => {
      // 根据当前地址查找对象
      const cur = item.list.find(v => v.path === location.pathname)
      if (cur) {
        setTitle(cur.title)
        setCurIndex(index)
      }
    })
  }, [location])

  const changeTab = index => {
    setCurIndex(index)
    history.push(tablist[index].list[0].path)
  }

  return (
    <div className={style.menu}>
      <div className={style.tab}>
        {tablist.map((item, index) =>
          <p
            key={item.id}
            className={curIndex === index ? style.active : ''}
            onClick={() => changeTab(index)}
          >
            <i className={`iconfont icon-${item.icon}`}></i>
            {item.title}
          </p>
        )}
      </div>
      <div className={style.list}>
        <h2>{title}</h2>
        <ul>
          {tablist[curIndex].list.map(item =>
            <li key={item.path}>
              <NavLink activeClassName={style.navActive} to={item.path}>{item.title}</NavLink>
            </li>
          )}
        </ul>
      </div>
    </div>
  )
}

export default Menu