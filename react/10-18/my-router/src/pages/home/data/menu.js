export const tablist = [
  {
    id: 0,
    title: '资金管理',
    icon: 'tongji',
    list: [
      {
        title: '第一页',
        path: '/first'
      },
      {
        title: '第二页',
        path: '/second'
      },
      {
        title: '第三页',
        path: '/third'
      }
    ]
  },
  {
    id: 1,
    title: '系统管理',
    icon: 'shouye',
    list: [
      {
        title: '第四页',
        path: '/fouth'
      },
      {
        title: '第五页',
        path: '/fifth'
      },
      {
        title: '第六页',
        path: '/sixth'
      }
    ]
  }
]

export const routelist = tablist.map(item => item.list).flat()