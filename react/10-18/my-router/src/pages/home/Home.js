import React from 'react'
import style from './home.module.scss'
import Menu from './components/Menu'
import Header from './components/Header'

const Home = (props) => {
  return (
    <div className={style.home}>
      <Menu />
      <main>
        <Header />
        <section>{props.children}</section>
      </main>
    </div>
  )
}

export default Home