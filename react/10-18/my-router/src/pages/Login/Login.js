import React from 'react'
import style from './login.module.scss'
import { LockOutlined, UserOutlined } from '@ant-design/icons'
import { Button, Form, Input } from 'antd'
import { useHistory } from 'react-router-dom'

const Login = (props) => {
  const hh = useHistory()

  const onFinish = (values) => {
    let token = values.user + values.pwd
    localStorage.setItem('token', token)
    hh.push('/')
  }
  // 创建form实例对象
  const [form] = Form.useForm()
  return (
    <div className={style.login}>
      <Form
        className={style.form}
        onFinish={onFinish}
        form={form}
      >
        <Form.Item
          name="user"
          rules={[
            {
              required: true,
              message: '请输入用户名!',
            }
          ]}
        >
          <Input prefix={<UserOutlined />} placeholder="用户名" />
        </Form.Item>
        <Form.Item
          name="pwd"
          rules={[
            {
              required: true,
              message: '请输入密码!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined />}
            type="password"
            placeholder="密码"
          />
        </Form.Item>
        <Form.Item>
          {/* <Button type="primary" htmlType="submit" block>登陆</Button> */}
          <Button type="primary" block onClick={() => {
            form.submit()
          }}>登陆</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default Login