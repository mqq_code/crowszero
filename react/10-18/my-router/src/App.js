import './App.scss';
import {
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import Login from './pages/Login/Login'
import Home from './pages/home/Home'
import First from './pages/home/First'
import Second from './pages/home/Second'
import Third from './pages/home/Third'
import Fouth from './pages/home/Fouth'
import Fifth from './pages/home/Fifth'
import Sixth from './pages/home/Sixth'

function App() {
  return (
    <Switch>
      <Route exact path="/login" component={Login} />
      <Route path="/" render={routeInfo => {
        let token = localStorage.getItem('token')
        if (!token) return <Redirect to="/login" />
        return <Home {...routeInfo}>
          <Switch>
            <Route exact path="/first" component={First} />
            <Route exact path="/second" component={Second} />
            <Route exact path="/third" component={Third} />
            <Route exact path="/fouth" component={Fouth} />
            <Route exact path="/fifth" component={Fifth} />
            <Route exact path="/sixth" component={Sixth} />
            <Redirect from="/" to="/first" />
          </Switch>
        </Home>
      }} />
    </Switch>
  );
}

export default App;
