import { useState, useEffect } from 'react'

const addZero = n => n < 10 ? '0' + n : n

const useTime = (endTime) => {
  const [time, setTime] = useState('0天 00:00:00')

  useEffect(() => {
    const timer = setInterval(() => {
      const count = new Date(endTime) - Date.now()
      const d = Math.floor(count / (1000 * 60 * 60 * 24))
      const h = addZero(Math.floor(count / (1000 * 60 * 60) % 24))
      const m = addZero(Math.floor(count / (1000 * 60) % 60))
      const s = addZero(Math.floor(count / 1000 % 60))
      setTime(`${d}天 ${h}:${m}:${s}`)
    }, 1000)

    return () => {
      clearInterval(timer)
    }
  }, [])


  return time

}

export default useTime