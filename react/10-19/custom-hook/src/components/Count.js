import React, { memo, useState } from 'react'

const Count = (props) => {
  console.log('count组件更新了', props.list)
  const [count, setCount] = useState(0)

  return (
    <div>
      <h2>Count组件</h2>
      <button onClick={props.add}>添加列表数据</button>
      <button onClick={() => setCount(count + 1)}>+</button> {count}
    </div>
  )
}

// memo: 高阶组件，阻止组件不必要的更新，浅比较 props
// 如果给自组件传入函数，该函数必须使用 useCallback 创建，memo高阶组件才会生效
export default memo(Count, (prevProps, props) => {
  // console.log('prevProps', prevProps) // 上一次的数据
  // console.log('props', props) // 本地的数据
  if (prevProps.add !== props.add) {
    return false
  }
  return true // true: 不更新， false：更新
})