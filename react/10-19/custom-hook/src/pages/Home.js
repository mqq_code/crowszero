import React, { useState, useEffect, useCallback } from 'react'
import useTime from '../custom/useTime'
import Count from '../components/Count'

const Home = () => {
  const [list, setlist] = useState([1,2,3,4,5])
  const time = useTime('2023/1/1')

  const add = useCallback(() => {
    setlist([...list, list.length + 1])
  }, [list])

  return (
    <div>
      <h2>Home</h2>
      <button onClick={add}>添加列表数据</button>
      <ul>
        {list.map(item =>
          <li key={item}>{item}</li>
        )}
      </ul>
      <footer>过年倒计时：{time}</footer>
      <hr />
      <Count list={list} add={add} />
    </div>
  )
}

export default Home