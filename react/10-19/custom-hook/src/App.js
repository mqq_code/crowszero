import React from 'react'
import {
  BrowserRouter,
  Switch,
  Route,
  NavLink
} from 'react-router-dom'
import Home from './pages/Home'
import Detail from './pages/Detail'

const App = () => {
  return (
    <BrowserRouter>
      <NavLink to="/">首页</NavLink>
      <NavLink to="/detail">详情</NavLink>
      <hr />
      <Switch>
        <Route path="/detail" component={Detail} />
        <Route path="/" component={Home} />
      </Switch>
    </BrowserRouter>
  )
}

export default App