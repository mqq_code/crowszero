## git 版本管理系统

### 安装：
- windows淘宝镜像下载地址：https://npm.taobao.org/mirrors/git-for-windows/
- mac：xcode | brew安装

### 配置用户信息
```shell
git config --global user.name "你的名字"
git config --global user.email "你的邮箱"
git config -l // 查看配置
```

### 初始化仓库
1. 初始化git仓库 `git init`
2. 初始化以后会在当前目录下创建一个 .git 隐藏文件夹
3. .git 目录中保存git的所有记录
4. 删除 .git 文件后改文件将不被git管理

### git 常用命令
1. `git status` 查看当前仓库的状态
2. `git add <文件路径 ｜ 文件夹路径>` 把文件添加到暂存区
3. `git commit -m"本次提交描述"` 把暂存区的文件添加到本地git仓库
- `git commit -am"描述"`  相当于 git add + git commit
- ***注意*** 如果是新创建的文件必须要先执行git add 然后再 git commit -m
4. `git log` 查看 commit 提交记录
5. `git diff` 查看当前工作区的具体修改
6. `git checkout -- <文件路径>` 放弃工作区的修改 ***谨慎使用***
7. `git reset --hard <commit号>` 回退到哪个版本 ***谨慎使用***
- `git reset HEAD <文件路径>` 取消暂存，从暂存区回到工作区
8. `git reflog` 查看所有的操作记录
9. `git revert <commit号>` 撤销某一次的修改，生成一个新的commit记录 ***面试重点***

### 连接远程仓库，创建sshkey
1. 创建ssh key `ssh-keygen -t rsa -C "你的邮箱" `
2. 把 /.ssh/id_rsa.pub 文件的内容添加到远程仓库的公钥中

### 已有本地仓库，连接远程仓库
1. `git remote add origin 远程仓库地址` 关联本地仓库和远程仓库
2. `git remote -v` 查看本地仓库关联的远程仓库地址
3. `git remote remove origin` 删除本地和远程仓库的连接
4. `git push -u origin master` 第一次把本地仓库的内容推送到远程仓库


### 直接从远程仓库拉取代码
1. `git clone 仓库地址`
2. `git push` 推送本地仓库代码到远程
3. `git pull` 获取远程仓库最新代码到本地

### 分支管理
1. `git branch` 查看本地分支
- `git branch -a` 查看本地和远程的所有分支
2. `git checkout -b <分支名>` 创建本地分支
3. `git checkout <分支名>` 切换本地分支
4. `git push -u origin <远程分支名>` 第一把本地分支推送到对应的远程分支
5. `git fetch` 同步远程仓库的更新到本地
6. 本地切换到远程的新分支 ***面试重点***
- `git fetch` 把远程新分支拉去到本地
- `git checkout <分支名>`

7. `git merge <目标分支名>` 合并分支目标分支名到当前分支 ***重点***
- 如果目标分支和当前分支修改了同一个文件可能会有冲突
- 需要手动解决冲突，然后运行
- `git add`
- `git commit`

8. `git branch -d <分支名>` 删除已合并过的分支
9. `git branch -D <分支名>` 强制删除还未合并过的分支
10. `git pull` 同步远程分支的更新到本地分支，相当于 git fetch + git merge ***重点***
- `git fetch` 更新远程仓库的数据到本地
- `git merge origin/分支` 合并远程分支的代码到本地分支
11. `git push -f` 强制推送到远程 ***谨慎使用***


### 配置git命令别名
1. `git config --global alias.st status` git st ===> git status
2. `git config --global alias.ci commit` git ci ===> git commit
3. `git config --global alias.co checkout` git co ===> git checkout
4. `git config --global alias.br branch` git br ===> git branch

### 添加忽略文件
1. 创建 .gitignore 文件，配置要git忽略的文件和文件夹