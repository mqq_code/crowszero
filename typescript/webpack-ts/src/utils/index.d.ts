// .ts 文件可以写类型，也可以写逻辑
// .d.ts 类型声明文件，只可以定义类型

export const sum: (a: number, b: number) => number
export const format: (date: number) => string
export const ENV: 'production'

// 如果.d.ts中使用了export，那次文件就不可以直接扩展全局变量
// 如果想要在局部模块内修改全局的类型需要在 declare global 内定义
declare global {
  interface Window {
    _util_: string
  }

  declare const _VERSION_: string
}
