export const sum = (a, b) => a + b

export const format = (date) => {
  return new Date(date).toLocaleString()
}

export const ENV = 'production'

window._util_ = '_util_'

