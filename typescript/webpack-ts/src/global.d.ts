// 三斜线指令，引入声明文件
/// <reference path="./types/types.d.ts" />


// 扩展全局的window对象
interface Window {
  tip: (msg: string) => void
}

// 定义全局变量类型
// declare function log(msg: string): void
// declare let _aa_: string
// declare const _VERSION_: string