import './pagination.scss'

interface IParams {
  el: HTMLElement;
  total: number;
  pagesize: number;
  current?: number;
  pagesizes?: number[];
  changeCurrent?: (page: number, pagesize: number) => void
}

class Pagination {
  el: IParams['el']
  total: IParams['total'] // 总条数
  pagesize: IParams['pagesize'] // 每页条数
  totalPage: number// 总页数
  current: number = 1 // 当前页数
  pagesWrapper!: HTMLDivElement // 分页按钮的父元素
  pagesizes: number[] = [10, 50, 100] // 默认分页列表
  changeCurrent: IParams['changeCurrent'] // 分页改变时执行的函数

  constructor ({ el, total, pagesize, current, pagesizes, changeCurrent }: IParams) {
    this.el = el
    this.total = total
    this.pagesize = pagesize
    this.totalPage = Math.ceil(this.total / this.pagesize)
    if (current && current > 1 && current <= this.totalPage) {
      this.current = current
    }
    if (pagesizes) {
      this.pagesizes = pagesizes
    }
    this.changeCurrent = changeCurrent
    this.init()
  }
  init () {
    this.el.innerHTML = `
      <div class="pagination-wrap">
        <button class="prev">上一页</button>
        <div class="pages"></div>
        <button class="next">下一页</button>
        <select></select>
      </div>
    `
    this.pagesWrapper = this.el.querySelector('.pages')!
    this.renderPages()
    this.renderPagesizes()
    this.bindEvent()
  }
  renderPagesizes () {
    const select = this.$('select') as HTMLSelectElement
    select.innerHTML = this.pagesizes.map(item => `
      <option ${item === this.pagesize ? 'selected' : ''} value="${item}">每页 ${item} 条</option>
    `).join('')

    select.addEventListener('change', () => {
      // 修改每页条数
      this.pagesize = Number(select.value)
      // 重新计算总页数
      this.totalPage = Math.ceil(this.total / this.pagesize)
      // 重新渲染分页
      this.renderPages()
      // 修改当前页数
      this.change(1)
    })
  } 
  renderPages () {
    let btns = ''
    for (let i = 1; i <= this.totalPage; i ++) {
      btns += `<button data-page="${i}" class="${i === this.current ? 'active' : ''}">${i}</button>`
    }
    this.pagesWrapper.innerHTML = btns
  }
  bindEvent () {
    this.$('.prev')?.addEventListener('click', () => {
      if (this.current === 1) return 
      this.change(this.current - 1)
    })
    this.$('.next')?.addEventListener('click', () => {
      if (this.current === this.totalPage) return 
      this.change(this.current + 1)
    })
    this.pagesWrapper.addEventListener('click', e => {
      if ((e.target as HTMLElement).nodeName === 'BUTTON') {
        const page = (e.target as HTMLElement).getAttribute('data-page')
        this.change(Number(page))
      }
    })
  }
  change (index: number) {
    this.current = index
    this.$('.active')?.classList.remove('active')
    this.pagesWrapper.children[this.current - 1].classList.add('active')
    // 分页改变，通知用户
    this.changeCurrent && this.changeCurrent(this.current, this.pagesize)
  }
  $ (el: string) {
    return this.el.querySelector(el)
  }
}

export default Pagination