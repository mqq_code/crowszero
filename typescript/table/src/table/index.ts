import './table.scss'

interface IColumns<T> {
  title: string;
  key?: keyof T;
  render?: (record: T) => string
}

interface TableOptions<T> {
  el: HTMLElement;
  columns: IColumns<T>[];
  data: T[];
}

class Table<T> {
  el: TableOptions<T>['el']
  columns: TableOptions<T>['columns']
  data: TableOptions<T>['data']
  headeTr!: HTMLTableRowElement
  tbody!: HTMLTableSectionElement

  constructor ({ el, columns, data  }: TableOptions<T>) {
    this.el = el
    this.columns = columns
    this.data = data
    this.init()
  }
  init () {
    this.el.innerHTML = `
      <table class="table-container">
        <thead>
          <tr></tr>
        </thead>
        <tbody></tbody>
      </table>
    `
    this.headeTr = this.el.querySelector('thead tr')!
    this.tbody = this.el.querySelector('tbody')!
    this.renderHeader()
    this.renderContent()
  }
  renderHeader () {
    this.headeTr.innerHTML = this.columns.map(item => {
      return `<th>${item.title}</th>`
    }).join('')
  }
  renderContent () {
    // 根据data遍历出有多少行
    this.tbody.innerHTML = this.data.map(item => {
      return `
        <tr>
          ${
            // 根据表头渲染出每行有几列
            this.columns.map(col => `<td>${
              col.render ? col.render(item) : item[col.key!]
            }</td>`).join('')
          }
        </tr>
      `
    }).join('')
  }
  setData (data: T[]) {
    this.data = data
    this.renderContent()
  }

}

export default Table