import Table from './table'
import Pagination from './pagination'
import axios from 'axios'

interface IData {
  name: string;
  age: number;
  address: string;
  sex: number;
  id: string;
  index: number;
}

const table = new Table<IData>({
  el: document.querySelector('.table-wrap') as HTMLElement,
  columns: [
    {
      title: 'id',
      key: 'id'
    },
    {
      title: '序号',
      key: 'index'
    },
    {
      title: '姓名',
      key: 'name'
    },
    {
      title: '年龄',
      key: 'age',
      render: record => record.age + '岁'
    },
    {
      title: '性别',
      key: 'sex',
      render: record => record.sex ? '男' : '女'
    },
    {
      title: '地址',
      key: 'address'
    },
    {
      title: '操作',
      render: (record) => {
        return `<button data-record="${JSON.stringify(record)}">删除</button>`
      }
    }
  ],
  data: []
})

interface IRes {
  code: number;
  msg: string;
  data: IData[]
}

// 调用接口获取数据，渲染表格内容
axios.get<IRes>('/api/list').then(res => {
  // table.setData(res.data.data)
  const { data } = res.data
  table.setData(data.slice(0, 20))
  new Pagination({
    el: document.querySelector('.pagination') as HTMLDivElement,
    total: data.length,
    pagesize: 10,
    current: 1,
    pagesizes: [5, 10, 15, 20],
    changeCurrent: (page, pagesize) => {
      console.log(`当前是第 ${page} 页，每页 ${pagesize} 条`)
      table.setData(data.slice((page - 1) * pagesize, page * pagesize)) // page = 1 pagesize = 20
    }
  })
})






