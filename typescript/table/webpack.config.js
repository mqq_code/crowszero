const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const Mock = require('mockjs')

const data = Mock.mock({
  'list|200': [
    {
      "id": "@id",
      "index|+1": 1,
      "name": "@cname",
      "age|18-60": 18,
      "sex|0-1": 0,
      "address": "@city"
    }
  ]
})


module.exports = {
  mode: 'production',
  entry: './src/index.ts',
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'index.js'
  },
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(js|ts|tsx)$/,
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
  resolve: {
    // 引入文件时省略后缀名
    extensions: [".ts", ".tsx", ".js"]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html',
      filename: 'index.html'
    })
  ],
  devServer: {
    port: 3000,
    hot: true,
    open: true,
    setupMiddlewares (middlewares, devserver) {

      devserver.app.get('/api/list', (req, res) => {
        res.send({
          code: 200,
          msg: 'success',
          data: data.list
        })
      })
      
      // 奖品列表
      devserver.app.get('/api/prizelist', (req, res) => {
        res.send({
          code: 200,
          msg: 'success',
          data: [
            {
              code: 0,
              title: 'iphone14',
              img: 'http://mp.ofweek.com/Upload/News/Img/member1953/202104/26205445495931.jpg'
            },
            {
              code: 1,
              title: 'mac pro',
              img: 'http://mp.ofweek.com/Upload/News/Img/member1953/202104/26205445495931.jpg'
            },
            {
              code: 2,
              title: 'iwatch',
              img: 'http://mp.ofweek.com/Upload/News/Img/member1953/202104/26205445495931.jpg'
            },
            {
              code: 3,
              title: 'ipad',
              img: 'http://mp.ofweek.com/Upload/News/Img/member1953/202104/26205445495931.jpg'
            },
            {
              code: 4,
              title: '五常大米',
              img: 'http://mp.ofweek.com/Upload/News/Img/member1953/202104/26205445495931.jpg'
            },
            {
              code: 5,
              title: '陪陵榨菜',
              img: 'http://mp.ofweek.com/Upload/News/Img/member1953/202104/26205445495931.jpg'
            },
            {
              code: 6,
              title: '100元购物卡',
              img: 'http://mp.ofweek.com/Upload/News/Img/member1953/202104/26205445495931.jpg'
            },
            {
              code: 7,
              title: '谢谢惠顾',
              img: 'http://mp.ofweek.com/Upload/News/Img/member1953/202104/26205445495931.jpg'
            }
          ]
        })
      })
      // 抽奖结果
      devserver.app.get('/api/prize', (req, res) => {
        const timeout = Math.floor(Math.random() * 6)
        setTimeout(() => {
          const prizeCode = Math.floor(Math.random() * 8)
          res.send({
            code: 200,
            msg: 'success',
            data: prizeCode
          })
        }, timeout * 1000)
      })
      

      return middlewares
    }
  }
}