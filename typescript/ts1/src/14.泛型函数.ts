{

  // 泛型函数: 当函数中某些参数类型不确定时可以使用泛型
  // function add<T>(arr: T[], a: T): T[] {
  //   arr.push(a)
  //   return arr
  // }

  // let arr: number[] = [1, 2, 3, 4, 5]
  // let a = add<number>(arr, 100)
  // let b = add<string>(['a', 'b', 'c'], 'd')

  // 获取最小值
  // function getMin<T>(arr: T[]): T {
  //   let min: T = arr[0]
  //   for (let i = 1; i < arr.length; i ++) {
  //     if (min > arr[i]) {
  //       min = arr[i]
  //     }
  //   }
  //   return min
  // }
  // let n = getMin<number>([2, 3, 4, 5]) // 2
  // let s = getMin<string>(['d','b','a']) // 'a'
  // console.log(n, s);
  




  function ajax<T>({
    url,
    method = 'get',
    success
  }: {
    url: string,
    method?: string,
    success?: (data: T) => void
  }) {
    let xhr = new XMLHttpRequest()
    xhr.open(method, url)
    xhr.send()
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          let data = JSON.parse(xhr.responseText)
          success && success(data)
        }
      }
    }
  }


  interface IBannerItem {
    imageUrl: string;
    targetId: number;
  }
  interface IBannerRes {
    code: number;
    banners: IBannerItem[]
  }

  ajax<IBannerRes>({
    url: 'https://zyxcl-music-api.vercel.app/banner',
    success: (res) => {
      res.banners.forEach(item => {
        item.imageUrl
      })
    }
  })



  interface IComment {
    code: number;
    comments: any[];
    hotComments: any[];
    more: boolean;
  }
  ajax<IComment>({
    url: 'https://zyxcl-music-api.vercel.app/comment/music?id=186016&limit=1',
    success: (res) => {
      console.log(res.more)
    }
  })


  function test<T, U, K>(a: T, b: U, c: K): { name: T, age: U, fn: K } {
    return {
      name: a,
      age: b,
      fn: c
    }
  }
  let aa = test(100, '20', true)
  let bb = test([1,2,3], {}, () => {})





}