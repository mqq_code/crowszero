{
// 定义函数
// 函数声明
function fn1 (a: number, b: string): number {
  return a + b.length 
}

// 函数表达式
const fn2 = function (a: number, b: string): number {
  return a + b.length 
}

const fn3 = (a: number, b: string): number => {
  return a + b.length 
}
// 类型推论: 根据变量的值推断变量的类型
let aa = fn3(100, 'abc')

let obj = {
  name: '王小明',
  age: 20,
  say (food: string): void {
    console.log(`我叫${this.name}，今年${this.age}岁，爱吃${food}`);
  },
  say1: (food: string): void => {
    console.log(`爱吃${food}`);
  }
}

// 函数的完整类型
const a: number = 100
const fn4: (a: number, b: string) => number = (a: number, b: string): number => {
  return a + b.length 
}


// 函数的参数默认值
function fn6 (lastName: string, firstName = '马'): string {
  return firstName + lastName
}

// 函数的可选参数
function fn7 (lastName: string, firstName?: string): string {
  if (firstName) return firstName + lastName
  return lastName
}

// 函数剩余参数
function sum (base: number, ...rest: number[]) {
  return base + rest.reduce((prev, val) => prev + val)
}
// console.log(sum(100, 1, 2, 3, 4, 5, 6, 7, 8))



// 函数重载:
function getLen (a: number, n: number): string;
function getLen (a: any[]): number;
function getLen (a: number | any[], n?: number): string | number {
  if (typeof a === 'number') {
    return a.toFixed(n)
  } else {
    return a.length
  }
}
let cc = getLen(100, 2)
let dd = getLen([100, 2])

}