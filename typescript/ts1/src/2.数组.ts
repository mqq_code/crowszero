{

  // 定义数组方式一
  let arr: number[] = [1, 2, 3, 4, 5]
  let arr1: string[] = ['a', 'b', 'c']
  let arr2: string[] = []
  arr2.push('A')
  arr2.push('B')

  // 定义数组方式二： 通过泛型定义数组
  let arr3: Array<number> = [1, 2, 3, 4, 5, 6]
  let arr4: Array<string> = ['abc']
  let arr5: (string | number | boolean)[] = [true, 100, 'abc']

  // 元组： 元组类型允许表示一个已知元素数量和类型的数组，各元素的类型不必相同
  let arr6: [string, number, boolean] = ['abc', 100, true]


}