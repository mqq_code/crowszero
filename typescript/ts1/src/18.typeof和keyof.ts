{

  let obj = {
    name: '小明',
    age: 100,
    hobby: ['篮球'],
    x: true,
    y: 10
  }

  function getVal<T, K extends keyof T>(params: T, key: K): T[K] {
    return params[key]
  }

  console.log(getVal(obj, 'name')) // '小明'
  console.log(getVal(obj, 'age')) // 100



  // typeof js变量名 
  type IObj = typeof obj // 返回变量的类型
  type IAge = typeof obj.age // 返回变量的类型
  type IHobby = typeof obj.hobby // 返回变量的类型

  // keyof 类型  返回该类型的所有key名
  type IKeys = keyof IObj
  // type IKeys = "name" | "age" | "hobby" | "x" | "y"

}