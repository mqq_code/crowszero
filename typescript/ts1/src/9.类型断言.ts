{
  // 类型断言
  let a: unknown
  a = 'abc';
  (a as string).slice(1)

  a = 123;
  (<number>a).toFixed(2)


  // 非空断言
  // let title = document.querySelector('h1') as HTMLHeadingElement // 断言获取的元素为 h标签
  // let title = document.querySelector('h1')! // ! => 非空断言
  let title = document.querySelector('h2')

  // ? 可选链
  // title?.innerHTML  title为真值在执行 title.innerHTML
  console.log(title?.innerHTML)

  let obj: { name: string; age?: number } = { name: '小明' }
  obj.age?.toFixed(2)

}