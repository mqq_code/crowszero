{

  type ITest = string | string[] | number[]

  // a: number | string 联合类型
  function fn1 (a: string | any[]) {
    // 联合类型只能访问所有类型中 共有 的方法
    if (typeof a === 'string') {
      // 类型保护，在此判断中确定a时string类型，可以访问字符串的所有方法
      a.split('')
    } else {
      // 确定在此判断内a为数组
      a.join()
    }
  }
  fn1('100')



  // 交叉类型
  interface IDate {
    year: number;
    month: number;
    day: number;
  }
  interface ITime {
    hours: number;
    minute: number;
    second: number;
  }
  let date: IDate = {
    year: 2022,
    month: 9,
    day: 27
  }
  let time: ITime = {
    hours: 14,
    minute: 16,
    second: 18
  }

  // 交叉类型: 使用 & 合并多个类型就是交叉类型
  type FullDate = IDate & ITime
  function formatTime (date: FullDate) {
    console.log(date)
  }
  formatTime({
    ...date,
    ...time
  })

  // 使用 & 合并多个基础类型返回 never
  type ITest1 = number & string


}