{
  // 泛型类
  class Person<T> {
    name: string
    desc: T
    constructor(name: string, desc: T) {
      this.name = name
      this.desc = desc
    }
  }

  let xm = new Person<number>('小明', 11)
  console.log(xm)




  // let p = new Promise<number>((resolve, reject) => {
  //   setTimeout(() => {
  //     resolve(Math.random())
  //   }, 1000)
  // })

  // p.then(res => {
  //   res.toFixed(2)
  // })


}