{
  
  // 对象
  let 马保国: {
    name: string;
    age: number;
    sex: number;
    hobby: string[];
    address: string
  } = {
    name: '小明',
    age: 100,
    sex: 1,
    hobby: ['闪电五连鞭', '接化发'],
    address: '北京'
  }

  // 使用接口或者类型别名给对象定义类型
  // 类型别名
  // type IPerson = {
  //   name: string;
  //   age: number;
  //   sex: number;
  //   hobby: string[];
  //   address: string
  // }

  // 接口
  interface IPerson {
    name: string;
    age: number;
    sex: number;
    hobby?: string[]; // 可选参数
  }

  let xm: IPerson = {
    name: '小明',
    age: 100,
    sex: 1
  }

  // xm.hobby = ['吃饭']


  
  interface IRoute {
    path: string;
    component: string;
    name?: string;
    children?: IRoute[]
  }

  let router: IRoute[] = [
    {
      path: '/',
      component: '组件',
      children: [
        {
          path: '/',
          name: '',
          component: '组件',
          children: [
            {
              path: '',
              name: '',
              component: ''
            }
          ]
        }
      ]
    },
    {
      path: '/',
      name: '',
      component: '组件',
    }
  ]





}