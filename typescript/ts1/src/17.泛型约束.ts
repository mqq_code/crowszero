{
  // 泛型约束
  function getMaxLen<T extends { length: number }>(a: T, b: T): T {
    return a.length > b.length ? a : b
  }
  // console.log(getMaxLen<string>('abcdefg', '1234'))
  // console.log(getMaxLen<number[]>([1,2,3,4], [2,3]))
  // // console.log(getMaxLen(true, false))
  // console.log(getMaxLen({ name: '小明', length: 1 }, { name: '阿纲', length: 3 }))

  function test<T extends string | number>(a: T) {
    if (typeof a === 'string') {
      return a.length
    } else {
      return a.toFixed(2)
    }
  }
  console.log(test<string>('1234'))
  console.log(test<number>(1234))


  // 条件泛型
  type ITest<T> = T extends string ? number : string
  type II = ITest<string>

}