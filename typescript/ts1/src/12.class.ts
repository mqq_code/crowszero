{

  // class 基础用法
  // class Person {
  //   // 实例属性
  //   name: string
  //   age: number
  //   sex = '男'
  //   // 实例方法
  //   run = function () {}
  //   constructor (name: string, age: number) {
  //     this.name = name
  //     this.age = age
  //   }
  //   // 原型方法
  //   say (food: string): void {
  //     console.log(`我叫${this.name},今年${this.age}岁,爱吃${food}`);
  //   }
  // }

  // let xm = new Person('小明', 20)
  // console.log(xm);

  // function Person(name: string, age: number) {
  //   this.name = name;
  //   this.age = age;
  //   this.run = function () {}
  // }
  // Person.prototype.say = function () {}
  // let xm = new Person('小明', 20)
  // console.log(xm);

  
  // class继承
  /*
  class Person {
    name: string
    age: number
    constructor (name: string, age: number) {
      this.name = name
      this.age = age
    }
  }

  // 继承：
  // 1. 使用 extends 关键字继承父类
  // 2. 在子类的 constructor 调用 super
  // super：父类的构造函数
  class Doctor extends Person {
    job: string
    constructor (name: string, age: number, job: string) {
      super(name, age)
      this.job = job
    }
  }

  let xm = new Doctor('王小明', 20, '兽医')
  console.log(xm);
  */



  // 修饰符
  /*
  class Person {
    // 实例属性：实例对象可以访问的属性
    // readonly 只读
    public readonly name: string // 公有属性
    private age: number // 私有属性: 只能在 class 内使用，不能在子类和实例对象上调用
    protected sex: number = 1 // 受保护的属性：只能在 class 内和子类中使用，不能在实例对象上调用
    constructor (name: string, age: number) {
      this.name = name
      this.age = age
    }

    // 静态属性，Person类本身的属性和方法
    static type = 123
    static getType() {
      console.log(this.type)
    }
    static isPerson(a: any) {
      return a instanceof Person
    }
  }

  let xg = new Person('李小刚', 28)
  console.log(xg);
  // xg.name = 123
  // console.log(xg.name)
  // console.log(xg.age);
  // console.log(xg.sex);
  console.log(Person.type) // 调用静态属性
  Person.getType() // 调用静态方法
  
  
  class Doctor extends Person {
    job: string
    constructor (name: string, age: number, job: string) {
      super(name, age)
      this.job = job
      // console.log(this.sex);
      // console.log(this.age);
    }
  }

  let xm = new Doctor('王小明', 20, '兽医')
  // console.log(xm.age);
  // console.log(xm.sex);

  console.log('xg', Person.isPerson(xg))
  console.log('xm', Person.isPerson(xm))
  console.log('[]', Person.isPerson([]))
  console.log('100', Person.isPerson(100))

  // function Person1 () {
  //   this.name = 'person1' // 实例属性
  // }
  // Person1.type = '123' // 静态属性

  // let xm1 = new Person1()
  // console.log(xm1)
  // console.log(Person1.type)
  */

  // 使用接口规范类的属性
  /*
  interface IPerson {
    name: string;
    age: number;
    say(food: string): string;
  }
  type IPerson1 = {
    run(): void;
  }

  class Base {
    name: string = 'name'
    hobby = []
    run() {}
  }

  // class 必须实现 IPerson 接口的内容
  // class 只能继承一个父类，但是可以实现多个接口，用逗号分隔
  class Person extends Base implements IPerson,IPerson1 {
    age: number;
    sex = 1
    constructor () {
      super()
      this.age = 11111
    }
    say(food: string): string {
      return food
    }
  }
  */


  // 抽象类： 只能被继承，不能实例化，用来规范子类

  // 定义抽象类
  // abstract class Base {
  //   name: string = '小明'
  //   getName (): string {
  //     return this.name
  //   }
  //   // abstract 定义抽象属性和抽象方法
  //   // 只写类型不需要写具体实现
  //   // 必须在子类中实现父类中的抽象属性和方法
  //   abstract age: number
  //   abstract getAge(age: number): number
  // }

  // class Person extends Base {
  //   age: number
  //   constructor() {
  //     super()
  //     this.age = 100
  //   }
  //   getAge(age: number): number {
  //     return this.age
  //   }
  // }
  // let aa = new Person()
  // console.log(aa);


  type IDoctor = {
    name: string;
    age: number;
    say(): void;
    job: string;
  }

  abstract class Person {
    abstract name: string
    abstract age: number
    say () {
      console.log(`我的名字叫${this.name},今年${this.age}岁`);
    }
  }
  class Doctor extends Person implements IDoctor {
    name: string
    age: number
    job: string = '医生'
    constructor(name: string, age: number) {
      super()
      this.name = name
      this.age = age
    }
  }
  let xm = new Doctor('小明', 20)
  console.log(xm);
  

  







}