{


  // interface IPerson1 {
  //   readonly name: string;
  //   age?: number;
  // }
  // 使用继承扩展接口
  // interface IPerson extends IPerson1 {
  //   sex: '男' | '女';
  //   child: string;
  // }

  type IPerson = {
    readonly name: string;
    age: number;
  }
  // 使用交叉类型扩展类型
  type IPerson1 = { sex: '男' | '女'; child: string; } & IPerson


  let xm: IPerson1 = {
    name: '小明',
    sex: '男',
    child: 'aaa',
    age: 20
  }

  // 区别：
  // interface 重名会合并接口，interface 使用继承扩展接口
  // type 不可以重名，会报错，使用交叉类型扩展接口


}