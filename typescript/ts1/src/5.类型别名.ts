{

  // 类型别名
  type IArr1 = number | string | boolean
  let arr: IArr1[] = [1, 'sss', true, false]
  function push(val: IArr1) {
    arr.push(val)
  }
  push(100)

  type IObj = {
    x: number;
    y: number;
  }
  const pos: IObj = {
    x: 100,
    y: 200
  }

  // 定义多维数组
  type IDeepArr = number | IDeepArr[]
  let arr2: IDeepArr[] = [
    [1,[2,3,4],5],
    [[1,[2,[3]],4],5],
    1,2,3,4,5
  ]








}