{
  let title = document.querySelector('.title') as HTMLHeadingElement
  let btn = document.querySelector('.btn') as HTMLButtonElement
  let inp = document.querySelector('.inp') as HTMLInputElement
  let box = document.querySelector('.box') as HTMLDivElement

  btn.addEventListener('click', () => {
    title.innerHTML = inp.value
  })

  inp.addEventListener('keydown', (e) => {
    if (e.key === 'Enter') {
      title.innerHTML = (e.target as HTMLInputElement).value;
      (e.target as HTMLInputElement).value = ''
    }
  })


}