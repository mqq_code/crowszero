{

  // 接口
  interface IPerson {
    name: string;
    readonly age: number; // 只读属性不可修改
    sex: '男' | '女'; // 文字类型
    children?: IPerson[]; // 可选参数
    say(hobby: string): void;
    run: (m: number) => Promise<any>;
    child?(son: IPerson): void;
    [propName: string]: any; // 对象可以添加额外任意类型的属性
  }

  const xm: IPerson = {
    name: '王小明',
    age: 80,
    sex: '男',
    say(hobby: string) {
      console.log('我的爱好是', hobby);
    },
    run: (m: number) => {
      return Promise.resolve()
    }
  }

  // xm.say('吃饭')
  xm.address = 100


  // 函数接口
  // let sum = (a: number, b: string): [number, string]=> {
  //   return [a, b]
  // }

  interface ISumFn {
    (a: number, b: string): [number, string]
  }
  let sum: ISumFn = (a: number, b: string): [number, string]=> {
    return [a, b]
  }


  // 索引类型
  interface StringArray {
    [aa: number]: string;
  }
  let arr: StringArray = ['a', 'd', 'd']
  let arr1: string[] = []



  // 接口继承
  interface IAnimal {
    variety: string;
    color: string;
    weight: number;
    height: number;
  }

  let pig: IAnimal = {
    variety: '猪',
    color: '粉',
    weight: 400,
    height: 70
  }

  // 接口继承
  interface ICat extends IAnimal {
    name: string;
    sex: '公' | '母';
    hobby: [string];
  }

  let 汤姆: ICat = {
    variety: '猫',
    color: '蓝',
    weight: 10,
    height: 40,
    name: 'Tom',
    sex: '公',
    hobby: ['抓老鼠']
  }







  // 类接口
  // 泛型接口

}