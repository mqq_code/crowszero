{
  // 枚举类型
  // 200 成功
  // 1000 参数错误
  // 1001 认证错误
  // 1002 没有权限
  // 1003 登陆失败
  // 1004 token失效






  // const success = 200
  // const paramsFail = 1000
  // const authFail = 1001
  // const permission = 1002
  // const loginFial = 1003
  enum ResCode {
    success = 200,
    paramsFail = 1000,
    authFail = 1001,
    permission = 1002,
    loginFial = 1003,
  }
  // if (res.code === ResCode.success) {
  // } else if (res.code === ResCode.paramsFail) {
  // } else if (res.code === ResCode.authFail) {
  // } else if (res.code === ResCode.permission) {
  // } else if (res.code === ResCode.loginFial) {
  // } else if (res.code === 1004) {
  // }

  enum Sex {
    women = 0,
    men = 1,
    other = 2
  }

  function getSex (sex: 0 | 1 | 2) {
    if (sex === Sex.men) {
      return '男'
    } else if (sex === Sex.women) {
      return '女'
    } else {
      return '未知'
    }
  }
  console.log(getSex(2))




  enum KeyCode {
    Left = 37,
    Up = 38,
    Right = 39,
    Down = 40
  }

  let box = document.querySelector('.box') as HTMLDivElement
  let rect = box.getBoundingClientRect()
  box.style.cssText = `
    top: ${rect.top}px;
    left: ${rect.left}px;
  `
  console.log(box.getBoundingClientRect())
  document.addEventListener('keydown', (e) => {
    // box.getBoundingClientRect() 获取元素在页面中的位置
    let { left, top } = box.getBoundingClientRect()
    if (e.keyCode === KeyCode.Left) {
      box.style.left = left - 1 + 'px'
    } else if (e.keyCode === KeyCode.Up) {
      box.style.top = top - 1 + 'px'
    } else if (e.keyCode === KeyCode.Right) {
      box.style.left = left + 1 + 'px'
    } else if (e.keyCode === KeyCode.Down) {
      box.style.top = top + 1 + 'px'
    }
  })




}