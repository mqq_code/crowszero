{

  interface IObj {
    name: string;
    age: number;
    sex?: 0 | 1;
    hobby?: string[];
  }

  // 常用的内置泛型
  type PartialIObj = Partial<IObj> // 把IObj的所有属性变成 可选属性
  type RequiredIObj = Required<IObj> // 把IObj的所有属性变成 必填属性
  type ReadonlyIObj = Readonly<IObj> // 把IObj的所有属性变成 只读属性
  type PickIObj = Pick<IObj, 'name' | 'age' | 'hobby'>// 获取IObj中指定的部分属性
  type OmitTest = Omit<IObj, 'name' | 'age'> // 排除IObj中指定的属性

  type RecordITest = Record<'z' | 'y' | 'x', string> // 返回一个所有属性为同一类型的对象
  type RecordIObj = Record<keyof IObj, string> // 把IObj的所有属性变成 string类型

  type ExcludeTest = Exclude<'z' | 'y' | 'x', 'z'> // 返回两个参数中不一致的类型
  type ExcludeTest1 = Exclude<number | boolean | string, boolean>

  type ExtractTest = Extract<'z' | 'y' | 'x', 'z'>  // 返回两个参数中相同的类型



  const sum = (a: string, b: string) => {
    return a + b
  }
  type ISumReturn = ReturnType<typeof sum> // 获取函数返回值类型


  // type ISum = typeof sum // 获取函数的类型
  // type ISum1 = (a: string, b: string) => number
  // type ITest = ReturnType<ISum1> // 获取函数类型的返回值类型

}