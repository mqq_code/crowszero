{
  // 泛型接口

  interface IResponse<T> {
    code: number;
    msg: string;
    data: T
  }


  let res1: IResponse<number[]> = {
    code: 200,
    msg: '成功',
    data: [1, 2, 3, 4]
  }

  let res2: IResponse<{ total: number; page: number; pagesize: number }> = {
    code: 200,
    msg: '成功',
    data: {
      total: 100,
      page: 10,
      pagesize: 5
    }
  }


  type IDeepArr<T> = T | IDeepArr<T>[]

  let arr: IDeepArr<number>[] = [
    [
      [
        [
          1, 2, 3, 4, 5, 6
        ],
        [
          1, 2, 3, 4, 5, 6
        ]
      ],
      [
        1, 2, 3, 4, 5, 6
      ]
    ],
    [
      1, 2, 3, 4, 5, 6
    ],
    1, 2, 3, 4, 5, 6
  ]

  let arr1: IDeepArr<string>[] = [
    [
      [
        [
          'a', 'b', 'c'
        ],
        [
          'a', 'b', 'c'
        ]
      ],
      [
        'a', 'b', 'c'
      ]
    ],
    [
      'a', 'b', 'c'
    ],
    'a', 'b', 'c'
  ]

  
  // function flat<T>(arr: IDeepArr<T>[]): IDeepArr<T>[] {
  //   return arr.reduce((prev, val) => {
  //     return prev.concat(Array.isArray(val) ? flat(val) : [val])
  //   }, [])
  // }
  // console.log(flat<number>(arr))






  
}