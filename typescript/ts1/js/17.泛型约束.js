"use strict";
{
    // 泛型约束
    function getMaxLen(a, b) {
        return a.length > b.length ? a : b;
    }
    // console.log(getMaxLen<string>('abcdefg', '1234'))
    // console.log(getMaxLen<number[]>([1,2,3,4], [2,3]))
    // // console.log(getMaxLen(true, false))
    // console.log(getMaxLen({ name: '小明', length: 1 }, { name: '阿纲', length: 3 }))
    function test(a) {
        if (typeof a === 'string') {
            return a.length;
        }
        else {
            return a.toFixed(2);
        }
    }
    console.log(test('1234'));
    console.log(test(1234));
}
