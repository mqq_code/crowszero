"use strict";
{
    const sum = (a, b) => {
        return a + b;
    };
    // type ISum = typeof sum // 获取函数的类型
    // type ISum1 = (a: string, b: string) => number
    // type ITest = ReturnType<ISum1> // 获取函数类型的返回值类型
}
