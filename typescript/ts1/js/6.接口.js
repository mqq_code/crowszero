"use strict";
{
    const xm = {
        name: '王小明',
        age: 80,
        sex: '男',
        say(hobby) {
            console.log('我的爱好是', hobby);
        },
        run: (m) => {
            return Promise.resolve();
        }
    };
    // xm.say('吃饭')
    xm.address = 100;
    let sum = (a, b) => {
        return [a, b];
    };
    let arr = ['a', 'd', 'd'];
    let arr1 = [];
    let pig = {
        variety: '猪',
        color: '粉',
        weight: 400,
        height: 70
    };
    let 汤姆 = {
        variety: '猫',
        color: '蓝',
        weight: 10,
        height: 40,
        name: 'Tom',
        sex: '公',
        hobby: ['抓老鼠']
    };
    // 类接口
    // 泛型接口
}
