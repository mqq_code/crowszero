"use strict";
{
    // 定义函数
    // 函数声明
    function fn1(a, b) {
        return a + b.length;
    }
    // 函数表达式
    const fn2 = function (a, b) {
        return a + b.length;
    };
    const fn3 = (a, b) => {
        return a + b.length;
    };
    // 类型推论: 根据变量的值推断变量的类型
    let aa = fn3(100, 'abc');
    let obj = {
        name: '王小明',
        age: 20,
        say(food) {
            console.log(`我叫${this.name}，今年${this.age}岁，爱吃${food}`);
        },
        say1: (food) => {
            console.log(`爱吃${food}`);
        }
    };
    // 函数的完整类型
    const a = 100;
    const fn4 = (a, b) => {
        return a + b.length;
    };
    // 函数的参数默认值
    function fn6(lastName, firstName = '马') {
        return firstName + lastName;
    }
    // 函数的可选参数
    function fn7(lastName, firstName) {
        if (firstName)
            return firstName + lastName;
        return lastName;
    }
    // 函数剩余参数
    function sum(base, ...rest) {
        return base + rest.reduce((prev, val) => prev + val);
    }
    function getLen(a, n) {
        if (typeof a === 'number') {
            return a.toFixed(n);
        }
        else {
            return a.length;
        }
    }
    let cc = getLen(100, 2);
    let dd = getLen([100, 2]);
}
