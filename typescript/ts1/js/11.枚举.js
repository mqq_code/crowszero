"use strict";
{
    // 枚举类型
    // 200 成功
    // 1000 参数错误
    // 1001 认证错误
    // 1002 没有权限
    // 1003 登陆失败
    // 1004 token失效
    // const success = 200
    // const paramsFail = 1000
    // const authFail = 1001
    // const permission = 1002
    // const loginFial = 1003
    let ResCode;
    (function (ResCode) {
        ResCode[ResCode["success"] = 200] = "success";
        ResCode[ResCode["paramsFail"] = 1000] = "paramsFail";
        ResCode[ResCode["authFail"] = 1001] = "authFail";
        ResCode[ResCode["permission"] = 1002] = "permission";
        ResCode[ResCode["loginFial"] = 1003] = "loginFial";
    })(ResCode || (ResCode = {}));
    // if (res.code === ResCode.success) {
    // } else if (res.code === ResCode.paramsFail) {
    // } else if (res.code === ResCode.authFail) {
    // } else if (res.code === ResCode.permission) {
    // } else if (res.code === ResCode.loginFial) {
    // } else if (res.code === 1004) {
    // }
    let Sex;
    (function (Sex) {
        Sex[Sex["women"] = 0] = "women";
        Sex[Sex["men"] = 1] = "men";
        Sex[Sex["other"] = 2] = "other";
    })(Sex || (Sex = {}));
    function getSex(sex) {
        if (sex === Sex.men) {
            return '男';
        }
        else if (sex === Sex.women) {
            return '女';
        }
        else {
            return '未知';
        }
    }
    console.log(getSex(2));
    let KeyCode;
    (function (KeyCode) {
        KeyCode[KeyCode["Left"] = 37] = "Left";
        KeyCode[KeyCode["Up"] = 38] = "Up";
        KeyCode[KeyCode["Right"] = 39] = "Right";
        KeyCode[KeyCode["Down"] = 40] = "Down";
    })(KeyCode || (KeyCode = {}));
    let box = document.querySelector('.box');
    let rect = box.getBoundingClientRect();
    box.style.cssText = `
    top: ${rect.top}px;
    left: ${rect.left}px;
  `;
    console.log(box.getBoundingClientRect());
    document.addEventListener('keydown', (e) => {
        // box.getBoundingClientRect() 获取元素在页面中的位置
        let { left, top } = box.getBoundingClientRect();
        if (e.keyCode === KeyCode.Left) {
            box.style.left = left - 1 + 'px';
        }
        else if (e.keyCode === KeyCode.Up) {
            box.style.top = top - 1 + 'px';
        }
        else if (e.keyCode === KeyCode.Right) {
            box.style.left = left + 1 + 'px';
        }
        else if (e.keyCode === KeyCode.Down) {
            box.style.top = top + 1 + 'px';
        }
    });
}
