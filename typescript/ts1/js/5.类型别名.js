"use strict";
{
    let arr = [1, 'sss', true, false];
    function push(val) {
        arr.push(val);
    }
    push(100);
    const pos = {
        x: 100,
        y: 200
    };
    let arr2 = [
        [1, [2, 3, 4], 5],
        [[1, [2, [3]], 4], 5],
        1, 2, 3, 4, 5
    ];
}
