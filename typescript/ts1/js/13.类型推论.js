"use strict";
{
    // 类型推论: ts能够根据value反推出变量的类型
    let n = 100;
    let obj = {
        name: 'aa'
    };
    function test(a) {
        return 11;
    }
}
