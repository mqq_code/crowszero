"use strict";
{
    let title = document.querySelector('.title');
    let btn = document.querySelector('.btn');
    let inp = document.querySelector('.inp');
    let box = document.querySelector('.box');
    btn.addEventListener('click', () => {
        title.innerHTML = inp.value;
    });
    inp.addEventListener('keydown', (e) => {
        if (e.key === 'Enter') {
            title.innerHTML = e.target.value;
            e.target.value = '';
        }
    });
}
