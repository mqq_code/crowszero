"use strict";
{
    let obj = {
        name: '小明',
        age: 100,
        hobby: ['篮球'],
        x: true,
        y: 10
    };
    function getVal(params, key) {
        return params[key];
    }
    console.log(getVal(obj, 'name')); // '小明'
    console.log(getVal(obj, 'age')); // 100
    // type IKeys = "name" | "age" | "hobby" | "x" | "y"
}
