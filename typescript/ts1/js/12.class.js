"use strict";
{
    class Person {
        say() {
            console.log(`我的名字叫${this.name},今年${this.age}岁`);
        }
    }
    class Doctor extends Person {
        constructor(name, age) {
            super();
            this.job = '医生';
            this.name = name;
            this.age = age;
        }
    }
    let xm = new Doctor('小明', 20);
    console.log(xm);
}
