"use strict";
{
    // 定义数组方式一
    let arr = [1, 2, 3, 4, 5];
    let arr1 = ['a', 'b', 'c'];
    let arr2 = [];
    arr2.push('A');
    arr2.push('B');
    // 定义数组方式二： 通过泛型定义数组
    let arr3 = [1, 2, 3, 4, 5, 6];
    let arr4 = ['abc'];
    let arr5 = [true, 100, 'abc'];
    // 元组： 元组类型允许表示一个已知元素数量和类型的数组，各元素的类型不必相同
    let arr6 = ['abc', 100, true];
}
