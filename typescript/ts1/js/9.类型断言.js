"use strict";
var _a;
{
    // 类型断言
    let a;
    a = 'abc';
    a.slice(1);
    a = 123;
    a.toFixed(2);
    // 非空断言
    // let title = document.querySelector('h1') as HTMLHeadingElement // 断言获取的元素为 h标签
    // let title = document.querySelector('h1')! // ! => 非空断言
    let title = document.querySelector('h2');
    // ? 可选链
    // title?.innerHTML  title为真值在执行 title.innerHTML
    console.log(title === null || title === void 0 ? void 0 : title.innerHTML);
    let obj = { name: '小明' };
    (_a = obj.age) === null || _a === void 0 ? void 0 : _a.toFixed(2);
}
