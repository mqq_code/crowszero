"use strict";
{
    let res1 = {
        code: 200,
        msg: '成功',
        data: [1, 2, 3, 4]
    };
    let res2 = {
        code: 200,
        msg: '成功',
        data: {
            total: 100,
            page: 10,
            pagesize: 5
        }
    };
    let arr = [
        [
            [
                [
                    1, 2, 3, 4, 5, 6
                ],
                [
                    1, 2, 3, 4, 5, 6
                ]
            ],
            [
                1, 2, 3, 4, 5, 6
            ]
        ],
        [
            1, 2, 3, 4, 5, 6
        ],
        1, 2, 3, 4, 5, 6
    ];
    let arr1 = [
        [
            [
                [
                    'a', 'b', 'c'
                ],
                [
                    'a', 'b', 'c'
                ]
            ],
            [
                'a', 'b', 'c'
            ]
        ],
        [
            'a', 'b', 'c'
        ],
        'a', 'b', 'c'
    ];
    // function flat<T>(arr: IDeepArr<T>[]): IDeepArr<T>[] {
    //   return arr.reduce((prev, val) => {
    //     return prev.concat(Array.isArray(val) ? flat(val) : [val])
    //   }, [])
    // }
    // console.log(flat<number>(arr))
}
