"use strict";
{
    // 泛型类
    class Person {
        constructor(name, desc) {
            this.name = name;
            this.desc = desc;
        }
    }
    let xm = new Person('小明', 11);
    console.log(xm);
}
