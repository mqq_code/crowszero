"use strict";
{
    // a: number | string 联合类型
    function fn1(a) {
        // 联合类型只能访问所有类型中 共有 的方法
        if (typeof a === 'string') {
            // 类型保护，在此判断中确定a时string类型，可以访问字符串的所有方法
            a.split('');
        }
        else {
            // 确定在此判断内a为数组
            a.join();
        }
    }
    fn1('100');
    let date = {
        year: 2022,
        month: 9,
        day: 27
    };
    let time = {
        hours: 14,
        minute: 16,
        second: 18
    };
    function formatTime(date) {
        console.log(date);
    }
    formatTime(Object.assign(Object.assign({}, date), time));
}
