"use strict";
{
    // javascript 类型
    // 基础类型: number, string, boolean, null, undefined, symbol
    // 引用类型: object, array, function
    // typscript 类型
    // number, string, boolean, null, undefined, symbol, any, void, never, unknown
    // let num: number = 100
    // let str: string = 'abc'
    // let bool: boolean = true
    // let n: null = null // 只有 null 一个值
    // let un: undefined = undefined // 只有 undefined 一个值
    // let a: symbol = Symbol(1)
    let nu;
    // any：相当于放弃了类型校验，可以赋任意类型
    let num = 100;
    console.log(num);
    // void: 没有值，函数没有返回值时使用
    function sum() {
        console.log(Math.random());
    }
    sum();
    // unknown: 暂时不确定是什么类型,确定类型时使用 类型断言 确定类型
    let test;
    function testFn(num) {
        if (num > 10) {
            test = num.toString();
            console.log(test.slice(1));
        }
        else {
            test = num;
        }
    }
    // never: 永不出现的类型
    function err() {
        throw new Error('手动报错');
    }
    let obj;
    let sex = '男';
}
