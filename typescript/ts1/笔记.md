## 安装和解析ts文件
1. `npm i -g typescript` 全局安装ts
2. `tsc -v` 检查ts版本号
3. `tsc --init` 初始化 ts 项目,自动生成 tsconig.json
4. 修改tsconfig.json 中的 rootDirs 和 outDir
5. `tsc --watch` 实时编译 rootDirs 中的ts文件

## typescript
1. typescript 是 javascript 的超集，包含js中的语法，同时扩展js中不存在的新语法
2. 增加类型检查，编译时校验类型
3. 有类型注解



