import './swiper.scss'

interface IAutoPlay {
  delay?: number;
  speed?: number;
}

interface ISwiperOptions {
  el: HTMLElement;
  prev?: HTMLElement;
  next?: HTMLElement;
  pagination?: HTMLElement;
  activeIndex?: number;
  autoplay?: boolean | IAutoPlay;
  changeIndex?: (index: number) => void; 
}

class Swiper {
  el: ISwiperOptions['el'] // 轮播最外层父元素
  prev?: ISwiperOptions['prev'] // 上一页按钮
  next?: ISwiperOptions['next'] // 下一页按钮
  pagination?: ISwiperOptions['pagination'] // 分页的父元素
  swiperWrapper: HTMLElement // 轮播的父元素
  slide: HTMLElement[] // 轮播的元素
  activeIndex: number = 0 // 当前高亮下标
  autoplay: ISwiperOptions['autoplay'] = false // 是否自动轮播
  defaultAutoPlay: IAutoPlay = { // 自动轮播默认值
    speed: 500,
    delay: 2000
  }
  autoplayTimer: NodeJS.Timer | null = null
  changeIndex: ISwiperOptions['changeIndex']

  constructor({ el, prev, next, pagination, activeIndex, autoplay, changeIndex }: ISwiperOptions) {
    this.el = el
    this.prev = prev
    this.next = next
    this.pagination = pagination
    this.swiperWrapper = this.$('.swiper-wrapper', this.el) as HTMLElement
    this.slide = [...this.swiperWrapper.children] as HTMLElement[]
    this.swiperWrapper.style.height = this.slide[0].clientHeight + 'px'
    if (activeIndex && activeIndex > 0 && activeIndex < this.slide.length) {
      this.activeIndex = activeIndex
    }
    if (autoplay) {
      if (typeof autoplay === 'boolean') {
        this.autoplay = this.defaultAutoPlay
      } else {
        this.autoplay = {...this.defaultAutoPlay, ...autoplay}
      }
    }
    if (changeIndex) [
      this.changeIndex = changeIndex
    ]
    this.init()
  }
  // 初始化
  init (): void {
    // 显示高亮的slide
    this.slide[this.activeIndex].classList.add('swiper-slide-active')
    if (this.pagination) {
      this.renderPagination()
    }
    this.bindEvent()
    this.autoplayStart()
  }
  // 自动轮播
  autoplayStart () {
    if (this.autoplay && typeof this.autoplay === 'object') {
      const { speed, delay } = this.autoplay
      this.slide.forEach(slide => {
        slide.style.transitionDuration = speed + 'ms'
      })
      this.autoplayTimer = setInterval(() => {
        this.change(this.activeIndex + 1)
      }, delay)
    }
  }
  // 渲染分页
  renderPagination (): void {
    this.pagination!.innerHTML = this.slide.map((item, index) => `
      <span class="${index === this.activeIndex ? 'swiper-pagination-active' : ''}"></span>
    `).join('')
  }
  // 绑定事件
  bindEvent (): void {
    this.prev?.addEventListener('click', () => {
      this.change(this.activeIndex - 1)
    })
    this.next?.addEventListener('click', () => {
      this.change(this.activeIndex + 1)
    })
    if (this.pagination) {
      [...this.pagination.children].forEach((item, index) => {
        item.addEventListener('click', () => {
          this.change(index)
        })
      })
    }
    if (this.autoplay) {
      this.el.addEventListener('mouseenter', () => {
        clearInterval(this.autoplayTimer!)
      })
      this.el.addEventListener('mouseleave', () => {
        this.autoplayStart()
      })
    }
    if (this.prev && this.next) {
      this.el.addEventListener('mouseenter', () => {
        this.prev!.style.transform = 'translateX(0)'
        this.next!.style.transform = 'translateX(0)'
      })
      this.el.addEventListener('mouseleave', () => {
        this.prev!.style.transform = 'translateX(-100%)'
        this.next!.style.transform = 'translateX(100%)'
      })
    }
  }
  // 切换图片
  change (index: number): void {
    if (index < 0) {
      this.activeIndex = this.slide.length - 1
    } else if (index > this.slide.length - 1) {
      this.activeIndex = 0
    } else {
      this.activeIndex = index
    }
    // 切换图片
    this.$('.swiper-slide-active', this.swiperWrapper)?.classList.remove('swiper-slide-active')
    this.slide[this.activeIndex].classList.add('swiper-slide-active')
    // 切换分页高亮
    if (this.pagination) {
      this.$('.swiper-pagination-active', this.pagination)?.classList.remove('swiper-pagination-active')
      this.pagination.children[this.activeIndex].classList.add('swiper-pagination-active')
    }
    // 切换的时候调用此函数通知实例对象切换的当前下标
    this.changeIndex && this.changeIndex(this.activeIndex)
  }
  $ (el: string, parent: (HTMLElement | Document) = document) {
    return parent.querySelector(el)
  }
}

export default Swiper