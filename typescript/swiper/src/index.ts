import './scss/index.scss'
import Swiper from './swiper/swiper'
import axios from 'axios'

new Swiper({
  el: document.querySelector('.swiper0')!,
  prev: document.querySelector('.swiper-prev') as HTMLElement,
  next: document.querySelector('.swiper-next') as HTMLElement,
  pagination: document.querySelector('.swiper-pagination') as HTMLElement,
  activeIndex: 1,
  autoplay: {
    speed: 500,
    delay: 2000
  }
})



axios.get('https://zyxcl-music-api.vercel.app/banner').then(res => {
  console.log(res.data.banners)
  const swiper1 = document.querySelector('.swiper1')
  const swiper1Wrapper = document.querySelector('.swiper1 .swiper-wrapper')
  swiper1Wrapper!.innerHTML = res.data.banners.map((item: any) => `
    <div class="swiper-slide"><img src="${item.imageUrl}"></div>
  `).join('')

  new Swiper({
    el: swiper1 as HTMLElement,
    autoplay: true,
    pagination: document.querySelector('.swiper1 .swiper-pagination') as HTMLElement,
    changeIndex: (index) => {
      console.log('当前高亮下标:', index)
    }
  })
})














