// import './scss/index.scss'
// import axios from 'axios'
// import { $, gets } from './util'
// import fire from './assets/fire.png'

// // console.log(fire)
// // let img = new Image()
// // img.src = fire
// // document.body.appendChild(img)


// axios.get('/api/music').then(res => {
//   $('.keyboard').innerHTML = res.data.map(item => `
//     <p
//       data-code="${item.keyCode}"
//       data-url="${item.url}"
//       data-id="${item.id}"
//     >${item.keyTrigger}</p>
//   `).join('')
//   // 预加载音频文件
//   res.data.forEach(item => {
//     const audio = new Audio()
//     audio.src = item.url
//   })
// })

// // 获取开关状态
// let power = $('.power p').classList.contains('on')
// $('.power p').addEventListener('click', function () {
//   this.classList.toggle('on')
//   power = this.classList.contains('on')
//   if (!power) {
//     $('.id').innerHTML = ''
//     $('.keyboard').classList.add('off')
//   } else {
//     $('.keyboard').classList.remove('off')
//   }
// })

// // 播放
// const play = btn => {
//   btn.classList.add('active')
//   if (power) {
//     const url = btn.getAttribute('data-url')
//     const id = btn.getAttribute('data-id')
//     $('.id').innerHTML = id
//     $('audio').src = url
//     $('audio').autoplay = true
//   }
// }

// // 事件委托
// $('.keyboard').addEventListener('mousedown', e => {
//   if (e.target.nodeName === 'P') {
//     play(e.target)
//   }
// })
// $('.keyboard').addEventListener('mouseup', e => {
//   if (e.target.nodeName === 'P') {
//     e.target.classList.remove('active')
//   }
// })

// // 键盘事件
// document.addEventListener('keydown', e => {
//   gets('.keyboard p').forEach(p => {
//     const code = p.getAttribute('data-code') * 1
//     if (e.keyCode === code) {
//       play(p)
//     }
//   })
// })
// document.addEventListener('keyup', e => {
//   $('.active') && $('.active').classList.remove('active')
// })

// // 拖拽修改音量
// let mousedown = false // 是否按下
// let mdX = null // 鼠标按下的位置
// let offsetLeft = null // 鼠标按下时i到父元素的位置
// let volumeW = $('.volume').clientWidth // 父元素的宽度
// let i = $('.volume i').clientWidth // i标签的宽度
// let maxLeft = volumeW - i // 可移动的最大距离

// $('.volume i').addEventListener('mousedown', (e) => {
//   mdX =  e.clientX // 鼠标按下的位置
//   offsetLeft = $('.volume i').offsetLeft // i到父元素左侧的距离
//   mousedown = true
// })

// document.addEventListener('mousemove', (e) => {
//   if (mousedown) {
//     const moveW = e.clientX - mdX // 鼠标移动的距离
//     let left = offsetLeft + moveW // i的位置
//     if (left <= 0) { // 处理最小边界
//       left = 0
//     } else if (left >= maxLeft) { // 处理最大边界
//       left = maxLeft
//     }
//     $('.volume i').style.left = left + 'px'
//     // 修改音量
//     $('audio').volume = left / maxLeft
//   }
// })
// document.addEventListener('mouseup', () => {
//   console.log('抬起')
//   mousedown = false
// })


// class Person {
//   constructor(name) {
//     this.name = name
//   }
// }

// let xm = new Person('小明')

// const sum = (a, b) => a + b

// setTimeout(() => {
//   let aa = Math.random()
//   let bb = Math.random()
//   console.log(sum(aa, bb))
// })


import axios from "axios"


// 同源策略: 浏览器的保护机制
// 当发起的 ajax 请求，如果请求的接口协议、端口、域名和当前页面的协议、端口、域名有一个不一致就会发生跨域
axios.get('/mqq/weixin/home/businessIndex?param=%7B%22index%22%3A0%2C%22currentPage%22%3A1%2C%22reqUrl%22%3A%22https%3A%2F%2Fcmsapi.dmall.com%2Fapp%2Fweb%2Fjson%2F1%2F12527%22%2C%22pairs%22%3A%221-0-12527%22%2C%22code%22%3A%221%22%2C%22networkType%22%3A2%7D&token=&source=2&tempid=C9C8436F7B1000022B2318D9BF981504&pubParam=%7B%7D&d_track_data=%7B%22session_id%22%3A%22C9F09A1699D000026FD674D0774CF8E0%22%2C%22uuid%22%3A%22C9C8436F7B1000022B2318D9BF981504%22%2C%22project%22%3A%22%E5%BE%AE%E4%BF%A1%E5%95%86%E5%9F%8E%22%2C%22env%22%3A%22mweb%22%2C%22tdc%22%3A%22%22%2C%22tpc%22%3A%22%22%7D')
.then(res => {
  console.log(res.data)
})

