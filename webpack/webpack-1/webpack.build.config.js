const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

// CommonJS规范
module.exports = {
  mode: 'production', // 打包模式: production 生产模式， development 开发模式
  entry: './src/index.js', // 打包的入口文件
  output: {
    path: path.join(__dirname, 'build'), // 输出的目录，必须是绝对路径
    filename: 'a.js'
  },
  // 配置插件
  plugins: [
    // 打包时根据 template 生成 html 文件，自动把打包后的js文件引入页面
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ],
  module: {
    // 配置 loader, 让 js 可以解析其他类型的文件
    rules: [
      {
        test: /\.(css|scss|sass)$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        // 使用内置的资源模块解析图片
        type: 'asset',
      },
      {
        test: /\.html$/i,
        // 使用内置的资源模块解析图片
        use: 'html-loader'
      },
      {
        test: /\.js$/i,
        exclude: /node_modules/, // 不解析此文件夹内的js文件
        use: 'babel-loader'
      }
    ]
  },
  // 开发服务器配置
  devServer: {
    port: 5000,
    open: true,
    setupMiddlewares: (middlewares, devServer) => {
      const data = require('./data/data.json')
      // 模拟后端接口，devServer.app === express()
      devServer.app.get('/api/music', (_, response) => {
        response.send(data)
      })

      return middlewares
    },
    // 配置代理解决跨域问题
    proxy: {
      // '/mqq': 'https://wxcmsapi.dmall.com'
      // /mqq/weixin/home/businessIndex === https://wxcmsapi.dmall.com/mqq/weixin/home/businessIndex
      
      // '/weixin': 'https://wxcmsapi.dmall.com'
      // weixin/home/businessIndex === https://wxcmsapi.dmall.com/weixin/home/businessIndex
    
      '/mqq': {
        target: 'https://wxcmsapi.dmall.com',
        pathRewrite: { '^/mqq': '' },
        changeOrigin: true
      }
      // /mqq/weixin/home/businessIndex === https://wxcmsapi.dmall.com/weixin/home/businessIndex
    }
  }
}