import './detail.scss'
import axios from 'axios'
import Swiper, { Navigation, Pagination  } from 'swiper'
import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'
import { $ } from '~util'



axios.get('/mqq/weixin/home/businessIndex', {
  params: {
    param: {"index":0,"currentPage":1,"reqUrl":"https://cmsapi.dmall.com/app/web/json/1/12527","pairs":"1-0-12527","code":"1","networkType":2},
    source: 2,
    tempid: 'C9C8436F7B1000022B2318D9BF981504',
    pubParam: {},
    d_track_data: {"session_id":"C9F0D4641EE000026C801230D13094D0","uuid":"C9C8436F7B1000022B2318D9BF981504","project":"微信商城","env":"mweb","tdc":"","tpc":""}
  }
})
.then(res => {
  const banners = res.data.data.pageModules[0].dataList
  $('.swiper-wrapper').innerHTML = banners.map(item => `
    <div class="swiper-slide">
      <img src="${item.imageUrl}" />
    </div>
  `).join('')

  // 实例化轮播图
  var mySwiper = new Swiper('.swiper', {
    modules: [Navigation, Pagination],
    loop: true, // 循环模式选项
    // 如果需要分页器
    pagination: {
      el: '.swiper-pagination',
    },
    // 如果需要前进后退按钮
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  })
})

