// pages/goods/goods.js
const app = getApp() // 获取全局数据
Page({

  /**
   * 页面的初始数据
   */
  data: {
    root: app.globalData.rootTitle,
    title: '苹果',
    price: 10,
    num: 10
  },

  changeNum (e) {
    console.log('父组件修改数量的函数', e);
    console.log('父组件接收子组件传过来的参数', e.detail);
    this.setData({
      num: this.data.num + e.detail.n
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})