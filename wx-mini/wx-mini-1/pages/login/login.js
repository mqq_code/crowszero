// pages/login/login.js
// const app = getApp() // 获取全局数据
// console.log(app.globalData.rootTitle);

Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: '默认标题',
    num: 0,
    // list: ['a', 'b', 'c', 'd', 'e', 'f']
    list: [
      {
        id: '0',
        title: '王小明',
        age: 18
      },
      {
        id: '1',
        title: '马继英',
        age: 21
      },
      {
        id: '2',
        title: '马老师',
        age: 33
      }
    ]
  },

  add (e) {
    // 需要手动触发页面更新
    this.setData({
      num: this.data.num * 1 + e.target.dataset.num
    })
  }  
})