// pages/mine/mine.js

Page({

  /**
   * 页面的初始数据
   */
  data: {
    now: new Date().toLocaleString(),
    list: new Array(30).fill(0).map((v, i) => i)
  },

  goDetail (e) {
    console.log(e.target.dataset); // 触发事件的元素
    console.log(e.currentTarget.dataset); // 绑定事件的元素
    const id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: `/pages/detail/detail?id=${id}&user=zyx`
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log('下拉刷新');
    setTimeout(() => {
      this.setData({
        now: new Date().toLocaleString()
      })
      // 停止下拉刷新
      wx.stopPullDownRefresh()
    }, 1000)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.list.length < 90) {
      console.log('距离底部100px，可以加载新数据了');
      wx.showLoading({
        title: '加载中',
      })
      setTimeout(() => {
        const arr = new Array(30).fill(0).map((v, i) => i)
        this.setData({
          list: [...this.data.list, ...arr]
        })
        wx.hideLoading()
      }, 1000)
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})