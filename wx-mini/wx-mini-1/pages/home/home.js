// pages/home/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: '标题',
    person: {
      name: '司马继英',
      age: 20,
      sex: '男',
      hobby: '篮球',
      address: '北京'
    }
  },

  chngePerson (e) {
    const key = e.target.dataset.key // 获取要修改的属性
    const person = {...this.data.person} // 拷贝数据
    person[key] = e.detail.value // 修改拷贝后的数据
    this.setData({ person }) // 更新页面
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.request({
      url: 'http://ustbhuangyi.com/sell/api/goods', //仅为示例，并非真实的接口地址
      success (res) {
        console.log(res.data)
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})