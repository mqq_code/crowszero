// pages/tab/tab.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    curIndex: 0,
    tablist: [
      {
        id: 'singer',
        title: '歌手',
        children: ['许嵩', '徐良', '汪苏泷', '邓紫棋', '刘德华', '张国荣']
      },
      {
        id: 'actor',
        title: '演员',
        children: ['马基英', '贾乃亮', '甄子丹', '孙红雷', '黄渤', '王宝强']
      },
      {
        id: 'athletes',
        title: '运动员',
        children: ['刘翔', '林丹', '郭晶晶', '张继科', '苏炳添', '张国伟']
      }
    ]
  },
  changeTab (e) {
    // console.log(e.target.dataset.index); // 触发事件的元素
    // console.log(e.currentTarget); // 绑定事件的元素
    console.log('父组件的c hangeTab 函数执行了', e.detail);
    this.setData({
      curIndex: e.detail
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})