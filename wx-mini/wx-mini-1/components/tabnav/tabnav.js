// components/tabnav/tabnav.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    curIndex: {
      type: Number
    },
    tablist: {
      type: Array
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    trigger (e) {
      console.log('点击子组件的按钮', e.target.dataset.index);
      // 子组件中调用父组件的自定义事件, 把子组件的数据传给父组件
      this.triggerEvent('click', e.target.dataset.index)
    }
  }
})
