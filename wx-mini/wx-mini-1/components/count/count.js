// components/count/count.js
const app = getApp()

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    num: {
      type: Number
    }
  },
  // 监听数据改变
  observers: {
    'num': function(num) {
      console.log('num改变了 ========> ', num);
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    title: 'count组件'
  },

  /**
   * 组件的方法列表
   */
  methods: {
    aa () {},
    btnClick (e) {
      console.log('子组件点击按钮');
      // 调用父组件传过来的自定义事件，并且把参数穿过去
      this.triggerEvent('change', {
        n: e.target.dataset.num,
        a: 100,
        b: 200,
        aa: this.aa
      })
    },
    changeRoot () {
      app.globalData.rootTitle = '随机标题' + Math.random()
      console.log(app.globalData);
      this.setData({})
    }
  }
})
