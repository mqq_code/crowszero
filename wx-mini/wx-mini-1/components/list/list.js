// components/list/list.js
Component({
  /**
   * 组件的属性列表, 接收父组件传过来的参数
   */
  properties: {
    list: {
      type: Array
    },
    test: {
      type: String, // 参数类型
      value: '啊啊啊啊' // 参数默认值
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
