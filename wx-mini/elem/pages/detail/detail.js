// pages/detail/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: {}, // 详情页数据
    list: [], // 所有列表数据
    cartList: [] // 购物车列表数据
  },

  // 清空购物车
  clearCart () {
    const list = [...this.data.list]
    list.forEach(item => {
      item.foods.forEach(food => {
        food.count = 0
      })
    })
    this.setData({
      cartList: [],
      list
    })
  },
  // 修改商品数量
  changeGoods (e) {
    // 获取自定义属性
    const params = e.detail.name ? e.detail : e.target.dataset
    console.log('detail', params);
    // 拷贝数据
    const list = [...this.data.list]
    // 当前点击的食物
    let curFoods = {}
    // 遍历两层，根据子组件传过来的数据查找点击的食物修改数据
    list.forEach(item => {
      item.foods.forEach(food => {
        if (food.name === params.name) {
          if (food.count === undefined) {
            food.count = 1
          } else {
            food.count += params.num
          }
          curFoods = food
        }
      })
    })
    // 购物车列表中查找是否存在此商品
    const cartList = [...this.data.cartList]
    const index = cartList.findIndex(v => v.name === curFoods.name)
    if (index > -1) {
      if (curFoods.count <= 0) {
        cartList.splice(index, 1)
      } else {
        cartList.splice(index, 1, curFoods)
      }
    } else {
      cartList.push(curFoods)
    }
    // 更新数据
    this.setData({
      list,
      cartList,
      info: curFoods
    })
  },

  // 获取手机号
  getphonenumber () {
    // 获取用户信息
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(JSON.parse(res.rawData));
      },
      fail: err => {
        console.log(err);
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 读取本地数据
    const list = wx.getStorageSync('list') || []
    const cartList = wx.getStorageSync('cartList') || []
    let info = {}
    list.forEach(item => {
      item.foods.forEach(food => {
        if (options.name === food.name) {
          info = food
        }
      })
    })
    this.setData({
      list,
      cartList,
      info
    })

    // 设置分享按钮
    wx.showShareMenu({
      menus: ['shareAppMessage', 'shareTimeline'],
      success() {
        console.log('share 设置分享成功');
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    console.log('detail ====> onHide');
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    console.log('detail ====> onUnload');
    // 把数据存到本地
    wx.setStorageSync('list', this.data.list)
    wx.setStorageSync('cartList', this.data.cartList)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享给朋友
   */
  onShareAppMessage () {
    console.log('share == 分享给朋友成功');
    return {
      title: '测试分享功能', // 分享的标题
      path: '/pages/detail/detail?name=大米粥', // 分享的页面
      imageUrl: 'http://static.galileo.xiaojukeji.com/static/tms/seller_avatar_256px.jpg' // 分享的连接显示的图片
    }
  },
  /**
   * 用户点击右上角分享到朋友圈
   */
  onShareTimeline () {
    console.log('share == 分享到朋友圈成功');
    return {
      title: '测试朋友圈', // 分享的标题
      query: 'name=小米粥', // 分享的页面后的参数，分享到朋友圈功能只能分享当前页面页
      imageUrl: 'http://static.galileo.xiaojukeji.com/static/tms/seller_avatar_256px.jpg' // 分享的连接显示的图片
    }
  }
})