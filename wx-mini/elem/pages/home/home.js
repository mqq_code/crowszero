import { getGoods, getSeller } from '../../api/index'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    curIndex: 1,
    navlist: [
      {
        title: '商品',
        id: 'goods'
      },
      {
        title: '评论',
        id: 'comments'
      },
      {
        title: '商家',
        id: 'merchant'
      }
    ],
    list: [], // 商品列表
    cartList: [], // 购物车列表
    sellerInfo: {} // 商家信息
  },
  // 清空购物车
  clearCart () {
    const list = [...this.data.list]
    list.forEach(item => {
      item.foods.forEach(food => {
        food.count = 0
      })
    })
    this.setData({
      cartList: [],
      list
    })
  },
  // 修改商品数量
  changeGoods (e) {
    // 拷贝数据
    const list = [...this.data.list]
    // 当前点击的食物
    let curFoods = {}
    // 遍历两层，根据子组件传过来的数据查找点击的食物修改数据
    list.forEach(item => {
      item.foods.forEach(food => {
        if (food.name === e.detail.name) {
          if (food.count === undefined) {
            food.count = 1
          } else {
            food.count += e.detail.num
          }
          curFoods = food
        }
      })
    })
    // 购物车列表中查找是否存在此商品
    const cartList = [...this.data.cartList]
    const index = cartList.findIndex(v => v.name === curFoods.name)
    if (index > -1) {
      if (curFoods.count <= 0) {
        cartList.splice(index, 1)
      } else {
        cartList.splice(index, 1, curFoods)
      }
    } else {
      cartList.push(curFoods)
    }
    // 更新数据
    this.setData({ list, cartList })
  },
  chageTab (e) {
    const index = e.target.dataset.index
    this.setData({
      curIndex: typeof index === 'number' ? index : e.detail.current
    })
  },
  async getGoodsList () {
    const res = await getGoods()
    this.setData({
      list: res.data
    })
  },
  async getSellerInfo () {
    const res = await getSeller()
    this.setData({
      sellerInfo: res.data
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
   onLoad: function (options) {
    this.getGoodsList()
    this.getSellerInfo()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 读取本地数据
    const list = wx.getStorageSync('list') || []
    const cartList = wx.getStorageSync('cartList') || []
    this.setData({
      list,
      cartList
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    // 把数据存到本地
    wx.setStorageSync('list', this.data.list)
    wx.setStorageSync('cartList', this.data.cartList)
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})