// components/goods/goods.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    list: Array
  },

  /**
   * 组件的初始数据
   */
  data: {
    curIndex: 0
  },

  /**
   * 组件的方法列表
   */
  methods: {
    changeTab (e) {
      this.setData({
        curIndex: e.currentTarget.dataset.index
      })
    },
    changeCount (e) {
      console.log('子组件', e.target.dataset);
      // 调用父组件的自定义事件
      this.triggerEvent('changeGoods', e.target.dataset)
    },
    goDetail (e) {
      wx.navigateTo({
        url: '/pages/detail/detail?name=' + e.currentTarget.dataset.name,
      })
    }
  }
})
