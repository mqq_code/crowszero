// components/header/header.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    sellerInfo: Object
  },

  /**
   * 组件的初始数据
   */
  data: {
    showDialog: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    changeDialog () {
      this.setData({
        showDialog: !this.data.showDialog
      })
    }
  },

})
