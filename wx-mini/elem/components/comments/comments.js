// components/comments/comments.js
import { getRatings } from '../../api/index'
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    sellerInfo: Object
  },

  /**
   * 组件的初始数据
   */
  data: {
    ratings: [], // 全部评论
    switch: true, // 展示有内容的评论
    curRatings: [] // 当前展示的评论
  },
  // 生命周期
  lifetimes: {
    attached () {
      getRatings().then(res => {
        this.setData({
          ratings: res.data.map(item => ({
            ...item,
            rateTime: new Date(item.rateTime).toLocaleString()
          }))
        })
      })
    }
  },

  observers: {
    'ratings,switch'(ratings, newSwitch) {
      this.setData({
        curRatings: newSwitch ? ratings.filter(item => item.text) : ratings
      })
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    changeSwitch () {
      this.setData({
        switch: !this.data.switch
      })
    }
  }
})
