// components/footer/footer.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    cartList: Array
  },

  observers: {
    cartList (cartList) {
      let totalCount = 0
      let totalPrice = 0
      cartList.forEach(item => {
        totalCount += item.count
        totalPrice += item.count * item.price
      })
      this.setData({ totalCount, totalPrice })
      if (cartList.length === 0 && this.data.showList) {
        this.setData({
          showList: false
        })
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    totalCount: 0,
    totalPrice: 0,
    showList: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    changeShow () {
      this.setData({
        showList: !this.data.showList
      })
    },
    clear () {
      this.triggerEvent('clearCart')
    },
    changeCount (e) {
      console.log('footer', e.target.dataset);
      // 调用父组件的自定义事件
      this.triggerEvent('changeGoods', e.target.dataset)
    }
  }
})
