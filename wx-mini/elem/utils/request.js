
const baseURL = 'http://ustbhuangyi.com'

const request = ({
  url,
  method = 'GET',
  data = {},
  header = {},
  timeout = 3000
}) => {
  return new Promise((resolve, reject) => {
    wx.request({
      url: baseURL + url, //仅为示例，并非真实的接口地址
      data,
      method,
      header,
      timeout,
      success: res => {
        resolve(res.data)
      },
      fail (e) {
        reject(e)
      }
    })
  })
}
export default request