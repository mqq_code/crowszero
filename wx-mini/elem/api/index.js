import request from '../utils/request'

// 获取商品列表
export const getGoods = () => {
  return request({ url: '/sell/api/goods' })
}
// 获取商家信息
export const getSeller = () => {
  return request({ url: '/sell/api/seller' })
}
// 获取评论接口
export const getRatings = () => {
  return request({ url: '/sell/api/ratings' })
}