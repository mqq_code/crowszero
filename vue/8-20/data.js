const data = [
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 9612,
        "name": "温岚"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 29597,
      "name": "有点野",
      "pic_str": "74766790689775",
      "pic": 74766790689775
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 11,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 35,
    "h": {
      "br": 320001,
      "fid": 0,
      "size": 12764517,
      "vd": -37140,
      "sr": 44100
    },
    "l": {
      "br": 128001,
      "fid": 0,
      "size": 5105833,
      "vd": -32841,
      "sr": 44100
    },
    "sq": {
      "br": 1619359,
      "fid": 0,
      "size": 64579972,
      "vd": -37139,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000009484210",
    "mst": 9,
    "cp": 1416500,
    "crbt": null,
    "cf": "",
    "dt": 319039,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192001,
      "fid": 0,
      "size": 7658728,
      "vd": -34534,
      "sr": 44100
    },
    "name": "屋顶",
    "id": 298317,
    "privilege": {
      "id": 298317,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 260,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 21349,
      "name": "看我72变",
      "pic_str": "109951167649668323",
      "pic": 109951167649668320
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 5,
    "fee": 1,
    "mv": 186025,
    "t": 0,
    "v": 54,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11786586,
      "vd": -56508,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4714716,
      "vd": -56508,
      "sr": 44100
    },
    "sq": {
      "br": 946919,
      "fid": 0,
      "size": 34870388,
      "vd": -56508,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000000210868",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 294000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7072006,
      "vd": -56508,
      "sr": 44100
    },
    "name": "布拉格广场",
    "id": 210049,
    "privilege": {
      "id": 210049,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 8331,
        "name": "李玟"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 25475,
      "name": "Promise",
      "pic_str": "109951165959446596",
      "pic": 109951165959446600
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 2,
    "fee": 8,
    "mv": 5287134,
    "t": 0,
    "v": 30,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 7712436,
      "vd": -48633,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3085000,
      "vd": -44588,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000005319691",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 192783,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 4627479,
      "vd": -46110,
      "sr": 44100
    },
    "name": "刀马旦",
    "id": 255020,
    "privilege": {
      "id": 255020,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 4292,
        "name": "李荣浩"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 135010288,
      "name": "2021中国好声音 第1期",
      "pic_str": "109951166537300832",
      "pic": 109951166537300830
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 2,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 5,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 5334248,
      "vd": -35927,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 2133725,
      "vd": -31685,
      "sr": 44100
    },
    "sq": {
      "br": 833399,
      "fid": 0,
      "size": 13885571,
      "vd": -35886,
      "sr": 44100
    },
    "hr": null,
    "cd": "01",
    "alia": [],
    "pop": 100,
    "rt": "",
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 133290,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 3200566,
      "vd": -33321,
      "sr": 44100
    },
    "name": "默 (Live)",
    "id": 1888354230,
    "privilege": {
      "id": 1888354230,
      "fee": 1,
      "payed": 0,
      "st": 0,
      "pl": 0,
      "dl": 0,
      "sp": 0,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 128,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": null,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 9612,
        "name": "温岚"
      }
    ],
    "al": {
      "id": 512175,
      "name": "男女情歌对唱冠军全记录",
      "pic_str": "109951165671182684",
      "pic": 109951165671182690
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 682,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 12851244,
      "vd": -16667,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 5140524,
      "vd": -12374,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000009484210",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 321253,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7710764,
      "vd": -14090,
      "sr": 44100
    },
    "name": "屋顶",
    "id": 5257138,
    "privilege": {
      "id": 5257138,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 21349,
      "name": "看我72变",
      "pic_str": "109951167649668323",
      "pic": 109951167649668320
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 11,
    "fee": 1,
    "mv": 186031,
    "t": 0,
    "v": 44,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10305963,
      "vd": -55634,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4122466,
      "vd": -55634,
      "sr": 44100
    },
    "sq": {
      "br": 990957,
      "fid": 0,
      "size": 31905487,
      "vd": -55634,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000005317941",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 257000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6183632,
      "vd": -55634,
      "sr": 44100
    },
    "name": "骑士精神",
    "id": 210062,
    "privilege": {
      "id": 210062,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 9612,
        "name": "温岚"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 29585,
      "name": "爱回温 新歌+精选",
      "pic_str": "109951165774753337",
      "pic": 109951165774753340,
      "alia": [
        "Love Comes Back"
      ]
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 9,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 31,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10338323,
      "vd": -15200,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4135391,
      "vd": -10900,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000008159355",
    "mst": 9,
    "cp": 1416500,
    "crbt": null,
    "cf": "",
    "dt": 258000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6203035,
      "vd": -12700,
      "sr": 44100
    },
    "name": "祝我生日快乐 (Live)",
    "id": 298101,
    "privilege": {
      "id": 298101,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 260,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 21330,
      "name": "J-Top冠军精选",
      "pic_str": "109951165705498890",
      "pic": 109951165705498900
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 22,
    "fee": 8,
    "mv": 186031,
    "t": 0,
    "v": 23,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10327815,
      "vd": -50694,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4131152,
      "vd": -50694,
      "sr": 44100
    },
    "sq": {
      "br": 980103,
      "fid": 0,
      "size": 31623036,
      "vd": -50694,
      "sr": 44100
    },
    "hr": null,
    "cd": "2",
    "alia": [],
    "pop": 100,
    "rt": "600902000005317941",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 258120,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6196706,
      "vd": -50694,
      "sr": 44100
    },
    "name": "骑士精神",
    "id": 209760,
    "privilege": {
      "id": 209760,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 5346,
        "name": "王力宏"
      },
      {
        "id": 5196,
        "name": "陶喆"
      },
      {
        "id": 7220,
        "name": "蔡琴"
      },
      {
        "id": 8163,
        "name": "江蕙"
      },
      {
        "id": 9274,
        "name": "苏芮"
      },
      {
        "id": 6075,
        "name": "庾澄庆"
      },
      {
        "id": 10559,
        "name": "张惠妹"
      },
      {
        "id": 9273,
        "name": "顺子"
      },
      {
        "id": 11363,
        "name": "动力火车"
      },
      {
        "id": 10573,
        "name": "张清芳"
      },
      {
        "id": 6456,
        "name": "周华健"
      },
      {
        "id": 6454,
        "name": "张信哲"
      },
      {
        "id": 5357,
        "name": "伍思凯"
      },
      {
        "id": 6469,
        "name": "张宇"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 8331,
        "name": "李玟"
      },
      {
        "id": 5360,
        "name": "吴宗宪"
      },
      {
        "id": 9487,
        "name": "陶晶莹"
      },
      {
        "id": 8354,
        "name": "林慧萍"
      },
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 9943,
        "name": "许茹芸"
      },
      {
        "id": 9945,
        "name": "萧亚轩"
      },
      {
        "id": 9272,
        "name": "孙燕姿"
      },
      {
        "id": 9946,
        "name": "徐若瑄"
      },
      {
        "id": 9102,
        "name": "彭佳慧"
      },
      {
        "id": 13283,
        "name": "信乐团"
      },
      {
        "id": 6070,
        "name": "游鸿明"
      },
      {
        "id": 2736,
        "name": "范逸臣"
      },
      {
        "id": 2516,
        "name": "迪克牛仔"
      },
      {
        "id": 8348,
        "name": "李心洁"
      },
      {
        "id": 12709,
        "name": "S.H.E"
      },
      {
        "id": 6484,
        "name": "周渝民"
      },
      {
        "id": 6522,
        "name": "朱孝天"
      },
      {
        "id": 11522,
        "name": "Energy"
      },
      {
        "id": 12056,
        "name": "可米小子"
      },
      {
        "id": 11100,
        "name": "B.A.D"
      },
      {
        "id": 12273,
        "name": "黄立成&麻吉"
      },
      {
        "id": 12983,
        "name": "Tension"
      },
      {
        "id": 3692,
        "name": "林志炫"
      },
      {
        "id": 3694,
        "name": "罗志祥"
      },
      {
        "id": 4700,
        "name": "欧汉声"
      },
      {
        "id": 2842,
        "name": "光良"
      },
      {
        "id": 4755,
        "name": "品冠"
      },
      {
        "id": 9611,
        "name": "万芳"
      },
      {
        "id": 9612,
        "name": "温岚"
      },
      {
        "id": 3065,
        "name": "黄立行"
      },
      {
        "id": 9606,
        "name": "王心凌"
      },
      {
        "id": 10199,
        "name": "杨丞琳"
      },
      {
        "id": 7652,
        "name": "范玮琪"
      },
      {
        "id": 4723,
        "name": "潘玮柏"
      }
    ],
    "al": {
      "id": 512853,
      "name": "用音乐加油！！手牵手大合唱",
      "pic_str": "124244813956662",
      "pic": 124244813956662
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 0,
    "mv": 5308516,
    "t": 0,
    "v": 684,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 13689251,
      "vd": -16200,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 5475726,
      "vd": -12200,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [
      "2003年抗非典公益歌曲"
    ],
    "pop": 100,
    "rt": "",
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 342000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 8213568,
      "vd": -13800,
      "sr": 44100
    },
    "name": "手牵手",
    "id": 5268423,
    "privilege": {
      "id": 5268423,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 3017298,
      "name": "巨星金曲-合唱篇",
      "pic_str": "0",
      "pic": 0
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 3,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 676,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11757236,
      "vd": -50103,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4702920,
      "vd": -45884,
      "sr": 44100
    },
    "sq": {
      "br": 1648988,
      "fid": 0,
      "size": 60569832,
      "vd": -50100,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 293851,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7054359,
      "vd": -47519,
      "sr": 44100
    },
    "name": "布拉格广场",
    "id": 29393641,
    "privilege": {
      "id": 29393641,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 21343,
      "name": "城堡",
      "pic_str": "109951167146289431",
      "pic": 109951167146289420
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 4,
    "fee": 1,
    "mv": 185048,
    "t": 0,
    "v": 42,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11096860,
      "vd": -50126,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4438770,
      "vd": -45925,
      "sr": 44100
    },
    "sq": {
      "br": 936350,
      "fid": 0,
      "size": 32461751,
      "vd": -50113,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000005317791",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 277347,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6658133,
      "vd": -47535,
      "sr": 44100
    },
    "name": "海盗",
    "id": 209917,
    "privilege": {
      "id": 209917,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 510845,
      "name": "跨世纪金曲精选",
      "pic_str": "605830906916387",
      "pic": 605830906916387
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 8,
    "fee": 8,
    "mv": 186025,
    "t": 0,
    "v": 682,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11757236,
      "vd": -50103,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4702920,
      "vd": -45884,
      "sr": 44100
    },
    "sq": {
      "br": 1648988,
      "fid": 0,
      "size": 60569832,
      "vd": -50100,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000000210868",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 293851,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7054359,
      "vd": -47519,
      "sr": 44100
    },
    "name": "布拉格广场",
    "id": 5234479,
    "privilege": {
      "id": 5234479,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 0,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 21330,
      "name": "J-Top冠军精选",
      "pic_str": "109951165705498890",
      "pic": 109951165705498900
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 20,
    "fee": 8,
    "mv": 186025,
    "t": 0,
    "v": 27,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11703946,
      "vd": -41534,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4681604,
      "vd": -41534,
      "sr": 44100
    },
    "sq": {
      "br": 929818,
      "fid": 0,
      "size": 33998817,
      "vd": -41534,
      "sr": 44100
    },
    "hr": null,
    "cd": "2",
    "alia": [],
    "pop": 100,
    "rt": "600902000000210868",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 292520,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7022385,
      "vd": -41534,
      "sr": 44100
    },
    "name": "布拉格广场",
    "id": 209756,
    "privilege": {
      "id": 209756,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 9612,
        "name": "温岚"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 29585,
      "name": "爱回温 新歌+精选",
      "pic_str": "109951165774753337",
      "pic": 109951165774753340,
      "alia": [
        "Love Comes Back"
      ]
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 12,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 23,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 12765613,
      "vd": -8500,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 5106302,
      "vd": -4200,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000009484210",
    "mst": 9,
    "cp": 1416500,
    "crbt": null,
    "cf": "",
    "dt": 319000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7659406,
      "vd": -5800,
      "sr": 44100
    },
    "name": "屋顶",
    "id": 298110,
    "privilege": {
      "id": 298110,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 260,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34559090,
      "name": "玻璃情缘 (对唱篇) Vol.One",
      "pic_str": "1378787583916322",
      "pic": 1378787583916322
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 3,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 14,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11176272,
      "vd": -50166,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4470535,
      "vd": -45959,
      "sr": 44100
    },
    "sq": {
      "br": 1639442,
      "fid": 0,
      "size": 57246615,
      "vd": -50165,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 279346,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6705781,
      "vd": -47579,
      "sr": 44100
    },
    "name": "海盗",
    "id": 406346416,
    "privilege": {
      "id": 406346416,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18884,
      "name": "我们都爱这个伦！",
      "pic_str": "97856534884248",
      "pic": 97856534884248
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 22,
    "fee": 8,
    "mv": 186025,
    "t": 0,
    "v": 680,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11757236,
      "vd": -50103,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4702920,
      "vd": -45884,
      "sr": 44100
    },
    "sq": {
      "br": 1648988,
      "fid": 0,
      "size": 60569832,
      "vd": -50100,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000000210868",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 293851,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7054359,
      "vd": -47519,
      "sr": 44100
    },
    "name": "布拉格广场",
    "id": 185790,
    "privilege": {
      "id": 185790,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 0,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 9278,
        "name": "宋祖英"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 28609,
      "name": "台北小巨蛋音乐会",
      "pic_str": "112150186051141",
      "pic": 112150186051141
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 12,
    "fee": 0,
    "mv": 0,
    "t": 0,
    "v": 24,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 9702966,
      "vd": -18222,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3881213,
      "vd": -13993,
      "sr": 44100
    },
    "sq": {
      "br": 845980,
      "fid": 0,
      "size": 25644488,
      "vd": -18227,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "",
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 242506,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5821797,
      "vd": -15635,
      "sr": 44100
    },
    "name": "黄浦江深",
    "id": 288371,
    "privilege": {
      "id": 288371,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 9278,
        "name": "宋祖英"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 6456,
        "name": "周华健"
      }
    ],
    "al": {
      "id": 28609,
      "name": "台北小巨蛋音乐会",
      "pic_str": "112150186051141",
      "pic": 112150186051141
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 14,
    "fee": 0,
    "mv": 0,
    "t": 0,
    "v": 22,
    "h": null,
    "l": {
      "br": 96000,
      "fid": 0,
      "size": 2819387,
      "vd": 1424,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "",
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 234000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": null,
    "name": "康定情歌",
    "id": 288382,
    "privilege": {
      "id": 288382,
      "fee": 0,
      "payed": 0,
      "st": 0,
      "pl": 96000,
      "dl": 96000,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 96000,
      "fl": 96000,
      "toast": false,
      "flag": 128,
      "preSell": false,
      "playMaxbr": 96000,
      "downloadMaxbr": 96000,
      "maxBrLevel": "standard",
      "playMaxBrLevel": "standard",
      "downloadMaxBrLevel": "standard",
      "plLevel": "standard",
      "dlLevel": "standard",
      "flLevel": "standard",
      "rscl": null,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 1033013,
        "name": "嘻游记"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 3081217,
      "name": "夜店咖",
      "pic_str": "2540971374163956",
      "pic": 2540971374163956
    },
    "st": 0,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 0,
    "mv": 0,
    "t": 0,
    "v": 8,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 6832725,
      "vd": -200,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 2733173,
      "vd": -2,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 170000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 4099690,
      "vd": -2,
      "sr": 44100
    },
    "name": "夜店咖",
    "id": 29808783,
    "privilege": {
      "id": 29808783,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 9061,
        "name": "那英"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 3688,
        "name": "刘欢"
      },
      {
        "id": 2116,
        "name": "陈奕迅"
      }
    ],
    "al": {
      "id": 35757091,
      "name": "中国新歌声第二季 第1期",
      "pic_str": "18892908300315027",
      "pic": 18892908300315028
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 7,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 21,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 7113645,
      "vd": -40101,
      "sr": 48000
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 2845485,
      "vd": -35952,
      "sr": 48000
    },
    "sq": {
      "br": 1013737,
      "fid": 0,
      "size": 22528184,
      "vd": -40391,
      "sr": 48000
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 177783,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 4268205,
      "vd": -37509,
      "sr": 48000
    },
    "name": "沧海一声笑 (Live)",
    "id": 490602328,
    "privilege": {
      "id": 490602328,
      "fee": 1,
      "payed": 0,
      "st": 0,
      "pl": 0,
      "dl": 0,
      "sp": 0,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 128,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": null,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 8331,
        "name": "李玟"
      }
    ],
    "al": {
      "id": 18909,
      "name": "Partners 拍档",
      "pic_str": "84662395339570",
      "pic": 84662395339570,
      "alia": [
        "周杰伦 方文山联手创作精选"
      ]
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 61,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 7708255,
      "vd": -45053,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3083327,
      "vd": -45053,
      "sr": 44100
    },
    "sq": {
      "br": 1007598,
      "fid": 0,
      "size": 24263054,
      "vd": -45053,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000005319691",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 192000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 4624970,
      "vd": -45053,
      "sr": 44100
    },
    "name": "刀马旦",
    "id": 186064,
    "privilege": {
      "id": 186064,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 12010120,
        "name": "李硕"
      },
      {
        "id": 12319226,
        "name": "张鑫"
      }
    ],
    "al": {
      "id": 36412633,
      "name": "中国新歌声第二季 第13期",
      "pic_str": "109951163038292176",
      "pic": 109951163038292180
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 5,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 20,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 9550125,
      "vd": -26954,
      "sr": 48000
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3820077,
      "vd": -22587,
      "sr": 48000
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 238698,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": null,
    "name": "想你就写信 (Live)",
    "id": 509781655,
    "privilege": {
      "id": 509781655,
      "fee": 1,
      "payed": 0,
      "st": 0,
      "pl": 0,
      "dl": 0,
      "sp": 0,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 128,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 10,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18884,
      "name": "我们都爱这个伦！",
      "pic_str": "97856534884248",
      "pic": 97856534884248
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 14,
    "fee": 8,
    "mv": 186031,
    "t": 0,
    "v": 671,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10306970,
      "vd": -28100,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4122846,
      "vd": -24300,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000005317941",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 257000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6184221,
      "vd": -25600,
      "sr": 44100
    },
    "name": "骑士精神",
    "id": 185776,
    "privilege": {
      "id": 185776,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 0,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 8331,
        "name": "李玟"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 25460,
      "name": "1994-2008年 豪华典藏精选",
      "pic_str": "109951167618315522",
      "pic": 109951167618315520
    },
    "st": 0,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 18,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 20,
    "h": {
      "br": 320002,
      "fid": 0,
      "size": 7685268,
      "vd": -52964,
      "sr": 44100
    },
    "l": {
      "br": 128002,
      "fid": 0,
      "size": 3074133,
      "vd": -48722,
      "sr": 44100
    },
    "sq": {
      "br": 1700398,
      "fid": 0,
      "size": 40825692,
      "vd": -52960,
      "sr": 44100
    },
    "hr": null,
    "cd": "01",
    "alia": [],
    "pop": 100,
    "rt": "600902000005319691",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 192075,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192002,
      "fid": 0,
      "size": 4611178,
      "vd": -50366,
      "sr": 44100
    },
    "name": "刀马旦",
    "id": 254832,
    "privilege": {
      "id": 254832,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 0,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 9061,
        "name": "那英"
      }
    ],
    "al": {
      "id": 35757091,
      "name": "中国新歌声第二季 第1期",
      "pic_str": "18892908300315027",
      "pic": 18892908300315028
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 4,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 39,
    "h": {
      "br": 320002,
      "fid": 0,
      "size": 8084205,
      "vd": 7143,
      "sr": 48000
    },
    "l": {
      "br": 128002,
      "fid": 0,
      "size": 3233709,
      "vd": 11390,
      "sr": 48000
    },
    "sq": {
      "br": 877518,
      "fid": 0,
      "size": 22162991,
      "vd": 8586,
      "sr": 48000
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 202051,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192002,
      "fid": 0,
      "size": 4850541,
      "vd": 9726,
      "sr": 48000
    },
    "name": "因为爱情 (Live)",
    "id": 490595315,
    "privilege": {
      "id": 490595315,
      "fee": 1,
      "payed": 0,
      "st": 0,
      "pl": 0,
      "dl": 0,
      "sp": 0,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 128,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": null,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 9278,
        "name": "宋祖英"
      },
      {
        "id": 3707,
        "name": "郎朗"
      },
      {
        "id": 189020,
        "name": "多明戈"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 28650,
      "name": "中国北京鸟巢夏季音乐会",
      "pic_str": "871912720826764",
      "pic": 871912720826764
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 23,
    "fee": 0,
    "mv": 0,
    "t": 0,
    "v": 18,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 16680878,
      "vd": -9400,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 6672427,
      "vd": -5400,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": "",
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 416000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 10008577,
      "vd": -6900,
      "sr": 44100
    },
    "name": "结尾曲-友谊地久天长(Live)",
    "id": 288820,
    "privilege": {
      "id": 288820,
      "fee": 0,
      "payed": 0,
      "st": 0,
      "pl": 320000,
      "dl": 320000,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 320000,
      "toast": false,
      "flag": 128,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "exhigh",
      "dlLevel": "exhigh",
      "flLevel": "exhigh",
      "rscl": null,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 9278,
        "name": "宋祖英"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 28650,
      "name": "中国北京鸟巢夏季音乐会",
      "pic_str": "871912720826764",
      "pic": 871912720826764
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 6,
    "fee": 0,
    "mv": 5557204,
    "t": 0,
    "v": 17,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11497141,
      "vd": -20512,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4598933,
      "vd": -20512,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": "",
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 287000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6898336,
      "vd": -20512,
      "sr": 44100
    },
    "name": "山歌好比春江水.多谢了(Live)",
    "id": 288750,
    "privilege": {
      "id": 288750,
      "fee": 0,
      "payed": 0,
      "st": 0,
      "pl": 320000,
      "dl": 320000,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 320000,
      "toast": false,
      "flag": 128,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "exhigh",
      "dlLevel": "exhigh",
      "flLevel": "exhigh",
      "rscl": null,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 8331,
        "name": "李玟"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 2261208,
      "name": "最完美 影音典藏精选",
      "pic_str": "109951165630444428",
      "pic": 109951165630444430
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 14,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 8,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 7687413,
      "vd": -22200,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3075024,
      "vd": -18200,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": "",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 192000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 4612487,
      "vd": -19500,
      "sr": 44100
    },
    "name": "刀马旦",
    "id": 25640004,
    "privilege": {
      "id": 25640004,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34429091,
      "name": "爱舞炫 盛装舞曲精选",
      "pic_str": "109951165986730839",
      "pic": 109951165986730830
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 2,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 17,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11669464,
      "vd": -56395,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4667812,
      "vd": -52298,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 291709,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7001696,
      "vd": -53873,
      "sr": 44100
    },
    "name": "布拉格广场",
    "id": 400162703,
    "privilege": {
      "id": 400162703,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 73876816,
      "name": "2018中国好声音 澳门演唱会",
      "pic_str": "109951163604724077",
      "pic": 109951163604724080
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 3,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 12,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 12521056,
      "vd": -37463,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 5008448,
      "vd": -37463,
      "sr": 44100
    },
    "sq": {
      "br": 904583,
      "fid": 0,
      "size": 35386074,
      "vd": -37463,
      "sr": 48000
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": null,
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 312949,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7512651,
      "vd": -37463,
      "sr": 44100
    },
    "name": "屋顶 (Live)",
    "id": 1317494434,
    "privilege": {
      "id": 1317494434,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 128,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 489872,
      "name": "全民发烧偶像剧冠军主题曲Top32",
      "pic_str": "109951163353504369",
      "pic": 109951163353504370
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 21,
    "fee": 8,
    "mv": 186025,
    "t": 0,
    "v": 682,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11757236,
      "vd": -50103,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4702920,
      "vd": -45884,
      "sr": 44100
    },
    "sq": {
      "br": 1648988,
      "fid": 0,
      "size": 60569832,
      "vd": -50100,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [
      "《上班女郎》"
    ],
    "pop": 95,
    "rt": "600902000000210868",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 293851,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7054359,
      "vd": -47519,
      "sr": 44100
    },
    "name": "布拉格广场",
    "id": 4874403,
    "privilege": {
      "id": 4874403,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 8331,
        "name": "李玟"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34429091,
      "name": "爱舞炫 盛装舞曲精选",
      "pic_str": "109951165986730839",
      "pic": 109951165986730830
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 27,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 18,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 7637203,
      "vd": -52936,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3054907,
      "vd": -48764,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 190928,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 4582339,
      "vd": -50359,
      "sr": 44100
    },
    "name": "刀马旦",
    "id": 400161754,
    "privilege": {
      "id": 400161754,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 8331,
        "name": "李玟"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 79885567,
      "name": "You & I 经典全纪录 Part II",
      "pic_str": "109951165986697918",
      "pic": 109951165986697920
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 4,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 8,
    "h": {
      "br": 320002,
      "fid": 0,
      "size": 7646607,
      "vd": -60674,
      "sr": 44100
    },
    "l": {
      "br": 128002,
      "fid": 0,
      "size": 3058669,
      "vd": -56517,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "01",
    "alia": [],
    "pop": 95,
    "rt": "",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 191164,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192002,
      "fid": 0,
      "size": 4587982,
      "vd": -58115,
      "sr": 44100
    },
    "name": "刀马旦",
    "id": 1372847156,
    "privilege": {
      "id": 1372847156,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 8331,
        "name": "李玟"
      }
    ],
    "al": {
      "id": 18884,
      "name": "我们都爱这个伦！",
      "pic_str": "97856534884248",
      "pic": 97856534884248
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 2,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 671,
    "h": {
      "br": 320000,
      "fid": 1121501860340033,
      "size": 7748511,
      "vd": -0.97,
      "sr": 44100
    },
    "l": {
      "br": 96000,
      "fid": 1155586720801296,
      "size": 2353077,
      "vd": -0.61,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": "600902000005319691",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 192862,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 160000,
      "fid": 1136895023128901,
      "size": 3894928,
      "vd": -0.57,
      "sr": 44100
    },
    "name": "刀马旦",
    "id": 185750,
    "privilege": {
      "id": 185750,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 8331,
        "name": "李玟"
      }
    ],
    "al": {
      "id": 512879,
      "name": "2002 国语冠军金选 24首",
      "pic_str": "75866302334161",
      "pic": 75866302334161
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 4,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 671,
    "h": {
      "br": 320000,
      "fid": 1077521395227415,
      "size": 7745395,
      "vd": -0.97,
      "sr": 44100
    },
    "l": {
      "br": 96000,
      "fid": 1059929209183034,
      "size": 2349961,
      "vd": -0.61,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": "600902000005319691",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 192862,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 160000,
      "fid": 997257046399844,
      "size": 3891812,
      "vd": -0.57,
      "sr": 44100
    },
    "name": "刀马旦",
    "id": 5268828,
    "privilege": {
      "id": 5268828,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 21330,
      "name": "J-Top冠军精选",
      "pic_str": "109951165705498890",
      "pic": 109951165705498900
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 8,
    "fee": 8,
    "mv": 185048,
    "t": 0,
    "v": 24,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11034166,
      "vd": -45167,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4413693,
      "vd": -45167,
      "sr": 44100
    },
    "sq": {
      "br": 929130,
      "fid": 0,
      "size": 32031763,
      "vd": -45167,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": "600902000005317791",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 275800,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6620517,
      "vd": -45167,
      "sr": 44100
    },
    "name": "海盗",
    "id": 209735,
    "privilege": {
      "id": 209735,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 8331,
        "name": "李玟"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 3017298,
      "name": "巨星金曲-合唱篇",
      "pic_str": "0",
      "pic": 0
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 11,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 669,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 7709445,
      "vd": -45044,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3083890,
      "vd": -45044,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 192000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 4625742,
      "vd": -45044,
      "sr": 44100
    },
    "name": "刀马旦",
    "id": 29393649,
    "privilege": {
      "id": 29393649,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 511168,
      "name": "K情歌8",
      "pic_str": "109951165641468578",
      "pic": 109951165641468580
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 19,
    "fee": 8,
    "mv": 186031,
    "t": 0,
    "v": 671,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10328905,
      "vd": -23300,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4131615,
      "vd": -19300,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": "600902000005317941",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 258000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6197378,
      "vd": -20800,
      "sr": 44100
    },
    "name": "骑士精神",
    "id": 5239560,
    "privilege": {
      "id": 5239560,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34429108,
      "name": "J女神 影音典藏精选",
      "pic_str": "109951165958812099",
      "pic": 109951165958812100
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 9,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 23,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11096859,
      "vd": -50124,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4438769,
      "vd": -50124,
      "sr": 44100
    },
    "sq": {
      "br": 927182,
      "fid": 0,
      "size": 32143930,
      "vd": -50124,
      "sr": 44100
    },
    "hr": null,
    "cd": "01",
    "alia": [],
    "pop": 95,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 277347,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6658132,
      "vd": -50124,
      "sr": 44100
    },
    "name": "海盗",
    "id": 400161915,
    "privilege": {
      "id": 400161915,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 512177,
      "name": "最佳广告主题曲年鉴",
      "pic_str": "796046418510139",
      "pic": 796046418510139
    },
    "st": 0,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 15,
    "fee": 8,
    "mv": 185048,
    "t": 0,
    "v": 682,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11176272,
      "vd": -50166,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4470535,
      "vd": -45959,
      "sr": 44100
    },
    "sq": {
      "br": 1639442,
      "fid": 0,
      "size": 57246615,
      "vd": -50165,
      "sr": 44100
    },
    "hr": null,
    "cd": "01",
    "alia": [],
    "pop": 95,
    "rt": "600902000005317791",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 279346,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6705781,
      "vd": -47579,
      "sr": 44100
    },
    "name": "海盗",
    "id": 5257206,
    "privilege": {
      "id": 5257206,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 0,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 8331,
        "name": "李玟"
      }
    ],
    "al": {
      "id": 512175,
      "name": "男女情歌对唱冠军全记录",
      "pic_str": "109951165671182684",
      "pic": 109951165671182690
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 23,
    "fee": 8,
    "mv": 0,
    "t": 0,
    "v": 682,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 7730199,
      "vd": -29047,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3092106,
      "vd": -24933,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": "600902000005319691",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 193253,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 4638137,
      "vd": -26493,
      "sr": 44100
    },
    "name": "刀马旦",
    "id": 5257160,
    "privilege": {
      "id": 5257160,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 1,
      "subp": 1,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34432052,
      "name": "星座圣典",
      "pic_str": "109951165958770652",
      "pic": 109951165958770660
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 9,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 15,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10273480,
      "vd": -50702,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4109418,
      "vd": -46613,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 256835,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6164106,
      "vd": -48150,
      "sr": 44100
    },
    "name": "骑士精神",
    "id": 400579242,
    "privilege": {
      "id": 400579242,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34429108,
      "name": "J女神 影音典藏精选",
      "pic_str": "109951165958812099",
      "pic": 109951165958812100
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 11,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 25,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11786493,
      "vd": -56483,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4714623,
      "vd": -52290,
      "sr": 44100
    },
    "sq": {
      "br": 1195000,
      "fid": 0,
      "size": 34905005,
      "vd": -56504,
      "sr": 44100
    },
    "hr": null,
    "cd": "01",
    "alia": [],
    "pop": 95,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 294600,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7071913,
      "vd": -53893,
      "sr": 44100
    },
    "name": "布拉格广场",
    "id": 400162858,
    "privilege": {
      "id": 400162858,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 21326,
      "name": "绝版公主 梦绮地精选",
      "pic_str": "109951165958730017",
      "pic": 109951165958730020
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 3,
    "fee": 1,
    "mv": 186025,
    "t": 0,
    "v": 35,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11663195,
      "vd": -35536,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4665304,
      "vd": -31451,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "2",
    "alia": [],
    "pop": 95,
    "rt": "600902000000210868",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 291578,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6997934,
      "vd": -33027,
      "sr": 44100
    },
    "name": "布拉格广场",
    "id": 209660,
    "privilege": {
      "id": 209660,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34432052,
      "name": "星座圣典",
      "pic_str": "109951165958770652",
      "pic": 109951165958770660
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 8,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 18,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10987146,
      "vd": -54443,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4394884,
      "vd": -50290,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 95,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 274677,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6592305,
      "vd": -51907,
      "sr": 44100
    },
    "name": "海盗",
    "id": 400581225,
    "privilege": {
      "id": 400581225,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34429108,
      "name": "J女神 影音典藏精选",
      "pic_str": "109951165958812099",
      "pic": 109951165958812100
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 10,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 23,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10305871,
      "vd": -55612,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4122374,
      "vd": -55612,
      "sr": 44100
    },
    "sq": {
      "br": 986881,
      "fid": 0,
      "size": 31774366,
      "vd": -55612,
      "sr": 44100
    },
    "hr": null,
    "cd": "01",
    "alia": [],
    "pop": 95,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 257574,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6183540,
      "vd": -55612,
      "sr": 44100
    },
    "name": "骑士精神",
    "id": 400161918,
    "privilege": {
      "id": 400161918,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 21326,
      "name": "绝版公主 梦绮地精选",
      "pic_str": "109951165958730017",
      "pic": 109951165958730020
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 2,
    "fee": 1,
    "mv": 186031,
    "t": 0,
    "v": 30,
    "h": {
      "br": 320001,
      "fid": 0,
      "size": 10287064,
      "vd": -33756,
      "sr": 44100
    },
    "l": null,
    "sq": null,
    "hr": null,
    "cd": "2",
    "alia": [],
    "pop": 90,
    "rt": "600902000005317941",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 257175,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192001,
      "fid": 0,
      "size": 6172256,
      "vd": -31201,
      "sr": 44100
    },
    "name": "骑士精神",
    "id": 209659,
    "privilege": {
      "id": 209659,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 21326,
      "name": "绝版公主 梦绮地精选",
      "pic_str": "109951165958730017",
      "pic": 109951165958730020
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 6,
    "fee": 1,
    "mv": 185048,
    "t": 0,
    "v": 31,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10994460,
      "vd": -30211,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4397810,
      "vd": -26058,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "2",
    "alia": [],
    "pop": 90,
    "rt": "600902000005317791",
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 274860,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6596693,
      "vd": -27659,
      "sr": 44100
    },
    "name": "海盗",
    "id": 209663,
    "privilege": {
      "id": 209663,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 7219,
        "name": "蔡依林"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34429091,
      "name": "爱舞炫 盛装舞曲精选",
      "pic_str": "109951165986730839",
      "pic": 109951165986730830
    },
    "st": 3,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 13,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 16,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11031031,
      "vd": -50184,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4412439,
      "vd": -46015,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 90,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 275774,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6618636,
      "vd": -47637,
      "sr": 44100
    },
    "name": "海盗",
    "id": 400161746,
    "privilege": {
      "id": 400161746,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 4,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": true,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 9061,
        "name": "那英"
      },
      {
        "id": 5347,
        "name": "汪峰"
      },
      {
        "id": 6075,
        "name": "庾澄庆"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34783199,
      "name": "中国新歌声 第1期",
      "pic_str": "3394192410959263",
      "pic": 3394192410959263
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 18,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 27224860,
      "vd": -27793,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 10889970,
      "vd": -27793,
      "sr": 44100
    },
    "sq": {
      "br": 932598,
      "fid": 0,
      "size": 79335499,
      "vd": -27793,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [
      "导师开场歌"
    ],
    "pop": 90,
    "rt": null,
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 680554,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 16334933,
      "vd": -27793,
      "sr": 44100
    },
    "name": "只有为你 + 安静 + 不管有多苦 + 满 + 我要夏天 (Live)",
    "id": 421160888,
    "privilege": {
      "id": 421160888,
      "fee": 1,
      "payed": 0,
      "st": 0,
      "pl": 0,
      "dl": 0,
      "sp": 0,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 10,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 71747366,
      "name": "2018中国好声音 第1期",
      "pic_str": "109951163409765276",
      "pic": 109951163409765280
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 13,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 28499636,
      "vd": -50891,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 11399880,
      "vd": -50891,
      "sr": 44100
    },
    "sq": {
      "br": 1007024,
      "fid": 0,
      "size": 89679453,
      "vd": -50891,
      "sr": 48000
    },
    "hr": null,
    "cd": "01",
    "alia": [],
    "pop": 85,
    "rt": null,
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 712431,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 17099799,
      "vd": -50891,
      "sr": 44100
    },
    "name": "让我一次爱个够 + 因为爱所以爱 + 抚仙湖 + 等你下课 + 霍元甲 + 万里长城永不倒 (Live)",
    "id": 866049359,
    "privilege": {
      "id": 866049359,
      "fee": 1,
      "payed": 0,
      "st": 0,
      "pl": 0,
      "dl": 0,
      "sp": 0,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 128,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": null,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 27819671,
        "name": "陈颖恩"
      },
      {
        "id": 29780520,
        "name": "肖敏晔"
      },
      {
        "id": 12788261,
        "name": "朱文婷"
      }
    ],
    "al": {
      "id": 36412633,
      "name": "中国新歌声第二季 第13期",
      "pic_str": "109951163038292176",
      "pic": 109951163038292180
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 19,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 7733805,
      "vd": -49127,
      "sr": 48000
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3093549,
      "vd": -44708,
      "sr": 48000
    },
    "sq": {
      "br": 1002652,
      "fid": 0,
      "size": 24225343,
      "vd": -49230,
      "sr": 48000
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 80,
    "rt": null,
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 193290,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 4640301,
      "vd": -46501,
      "sr": 48000
    },
    "name": "乌克丽丽 (Live)",
    "id": 509781651,
    "privilege": {
      "id": 509781651,
      "fee": 1,
      "payed": 0,
      "st": 0,
      "pl": 0,
      "dl": 0,
      "sp": 0,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 128,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": null,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 3688,
        "name": "刘欢"
      }
    ],
    "al": {
      "id": 35757091,
      "name": "中国新歌声第二季 第1期",
      "pic_str": "18892908300315027",
      "pic": 18892908300315028
    },
    "st": 1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 2,
    "fee": 1,
    "mv": 0,
    "t": 0,
    "v": 39,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 6697965,
      "vd": -7769,
      "sr": 48000
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 2679213,
      "vd": -3547,
      "sr": 48000
    },
    "sq": {
      "br": 897646,
      "fid": 0,
      "size": 18782245,
      "vd": -7786,
      "sr": 48000
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 70,
    "rt": null,
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 167391,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 4018797,
      "vd": -5209,
      "sr": 48000
    },
    "name": "一笑而过 + 你的背包 (Live)",
    "id": 490602323,
    "privilege": {
      "id": 490602323,
      "fee": 1,
      "payed": 0,
      "st": 0,
      "pl": 0,
      "dl": 0,
      "sp": 0,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 10,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 39744141,
      "name": "2006 来电答铃 国语歌曲排行总冠军",
      "tns": [
        "Top Ranking Mandarin Ringback Tones"
      ],
      "pic_str": "109951166116670361",
      "pic": 109951166116670370
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 0,
    "mv": 0,
    "t": 0,
    "v": 28,
    "h": {
      "br": 320001,
      "fid": 0,
      "size": 10283929,
      "vd": -46680,
      "sr": 44100
    },
    "l": {
      "br": 128001,
      "fid": 0,
      "size": 4113598,
      "vd": -42300,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "02",
    "alia": [],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 257097,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192001,
      "fid": 0,
      "size": 6170375,
      "vd": -44052,
      "sr": 44100
    },
    "name": "借口",
    "id": 573969609,
    "tns": [
      "Excuse"
    ],
    "privilege": {
      "id": 573969609,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 1,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34433014,
      "name": "婚礼歌手：幸福情歌精选",
      "pic_str": "109951165994895269",
      "pic": 109951165994895260
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 18,
    "fee": 0,
    "mv": 5343509,
    "t": 0,
    "v": 26,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10731146,
      "vd": -44271,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4292484,
      "vd": -40230,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "01",
    "alia": [],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 268277,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6438705,
      "vd": -41768,
      "sr": 44100
    },
    "name": "简单爱",
    "id": 400581054,
    "privilege": {
      "id": 400581054,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 257,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 13228454,
        "name": "蔡威泽"
      }
    ],
    "al": {
      "id": 37579061,
      "name": "2018年中央电视台春节联欢晚会",
      "pic_str": "109951163142162226",
      "pic": 109951163142162220
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 9,
    "fee": 0,
    "mv": 0,
    "t": 0,
    "v": 135,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 8570297,
      "vd": -25766,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3428145,
      "vd": -25766,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 677016,
    "crbt": null,
    "cf": "",
    "dt": 214203,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5142195,
      "vd": -25766,
      "sr": 44100
    },
    "name": "魔术与歌曲：告白气球",
    "id": 536570450,
    "privilege": {
      "id": 536570450,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 1875,
        "name": "阿信"
      }
    ],
    "al": {
      "id": 81679689,
      "name": "说好不哭",
      "pic_str": "109951164615900701",
      "pic": 109951164615900700
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 0,
    "mv": 0,
    "t": 0,
    "v": 10,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 8895405,
      "vd": -21216,
      "sr": 48000
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3558189,
      "vd": -16880,
      "sr": 48000
    },
    "sq": {
      "br": 825655,
      "fid": 0,
      "size": 22946329,
      "vd": -21369,
      "sr": 48000
    },
    "hr": {
      "br": 1593007,
      "fid": 0,
      "size": 44272343,
      "vd": -21216,
      "sr": 48000
    },
    "cd": "01",
    "alia": [
      "Won't Cry"
    ],
    "pop": 100,
    "rt": "",
    "mst": 9,
    "cp": 0,
    "crbt": null,
    "cf": "",
    "dt": 222333,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5337261,
      "vd": -18601,
      "sr": 48000
    },
    "name": "说好不哭",
    "id": 1391274164,
    "privilege": {
      "id": 1391274164,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "hires",
      "playMaxBrLevel": "hires",
      "downloadMaxBrLevel": "hires",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 1999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18905,
      "name": "叶惠美",
      "pic_str": "109951165566379710",
      "pic": 109951165566379710,
      "alia": [
        "Yeh Hui–mei"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 3,
    "fee": 0,
    "mv": 504177,
    "t": 0,
    "v": 153,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10792794,
      "vd": -34548,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4317143,
      "vd": -34548,
      "sr": 44100
    },
    "sq": {
      "br": 935088,
      "fid": 0,
      "size": 31529673,
      "vd": -34548,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889364",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 269000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6475693,
      "vd": -34548,
      "sr": 44100
    },
    "name": "晴天",
    "id": 186016,
    "privilege": {
      "id": 186016,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34720827,
      "name": "周杰伦的床边故事",
      "pic_str": "3265549553028224",
      "pic": 3265549553028224,
      "alia": [
        "Jay Chou's Bedtime Stories"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 8,
    "fee": 0,
    "mv": 0,
    "t": 0,
    "v": 86,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 8608958,
      "vd": -49485,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3443609,
      "vd": -49485,
      "sr": 44100
    },
    "sq": {
      "br": 932153,
      "fid": 0,
      "size": 25068722,
      "vd": -49485,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 215146,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5165392,
      "vd": -49485,
      "sr": 44100
    },
    "name": "告白气球",
    "id": 418603077,
    "privilege": {
      "id": 418603077,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18877,
      "name": "魔杰座",
      "pic_str": "109951163200234839",
      "pic": 109951163200234830,
      "alia": [
        "Capricorn"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 11,
    "fee": 0,
    "mv": 506121,
    "t": 0,
    "v": 146,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 8941236,
      "vd": -45493,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3576520,
      "vd": -41420,
      "sr": 44100
    },
    "sq": {
      "br": 934357,
      "fid": 0,
      "size": 26098171,
      "vd": -45883,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889008",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 223453,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5364759,
      "vd": -42946,
      "sr": 44100
    },
    "name": "稻香",
    "id": 185709,
    "privilege": {
      "id": 185709,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18903,
      "name": "七里香",
      "pic_str": "7746059418324672",
      "pic": 7746059418324672,
      "alia": [
        "Common Jasmine Orange"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 2,
    "fee": 0,
    "mv": 185014,
    "t": 0,
    "v": 139,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11970394,
      "vd": -52295,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4788183,
      "vd": -52295,
      "sr": 44100
    },
    "sq": {
      "br": 960536,
      "fid": 0,
      "size": 35924094,
      "vd": -52295,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889320",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 299000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7182253,
      "vd": -52295,
      "sr": 44100
    },
    "name": "七里香",
    "id": 186001,
    "privilege": {
      "id": 186001,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18896,
      "name": "11月的萧邦",
      "pic_str": "109951167749320136",
      "pic": 109951167749320130,
      "alia": [
        "November's Chopin"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 12,
    "fee": 0,
    "mv": 507143,
    "t": 0,
    "v": 148,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11830378,
      "vd": -56282,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4732177,
      "vd": -52014,
      "sr": 44100
    },
    "sq": {
      "br": 961072,
      "fid": 0,
      "size": 35522788,
      "vd": -56572,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [
      "电影《头文字D》插曲"
    ],
    "pop": 100,
    "rt": "600902000006889220",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 295692,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7098244,
      "vd": -53680,
      "sr": 44100
    },
    "name": "一路向北",
    "id": 185924,
    "privilege": {
      "id": 185924,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 6090,
        "name": "杨瑞代"
      }
    ],
    "al": {
      "id": 37251353,
      "name": "等你下课",
      "pic_str": "109951163110962030",
      "pic": 109951163110962030
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 0,
    "mv": 0,
    "t": 0,
    "v": 126,
    "h": {
      "br": 320001,
      "fid": 0,
      "size": 10801965,
      "vd": -37798,
      "sr": 48000
    },
    "l": {
      "br": 128001,
      "fid": 0,
      "size": 4320813,
      "vd": -33549,
      "sr": 48000
    },
    "sq": {
      "br": 954419,
      "fid": 0,
      "size": 32211667,
      "vd": -37295,
      "sr": 48000
    },
    "hr": {
      "br": 1722876,
      "fid": 0,
      "size": 58147067,
      "vd": -37789,
      "sr": 48000
    },
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 270000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192001,
      "fid": 0,
      "size": 6481197,
      "vd": -35221,
      "sr": 48000
    },
    "name": "等你下课 (with 杨瑞代)",
    "id": 531051217,
    "privilege": {
      "id": 531051217,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "hires",
      "playMaxBrLevel": "hires",
      "downloadMaxBrLevel": "hires",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        },
        {
          "rate": 1999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18877,
      "name": "魔杰座",
      "pic_str": "109951163200234839",
      "pic": 109951163200234830,
      "alia": [
        "Capricorn"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 2,
    "fee": 0,
    "mv": 345051,
    "t": 0,
    "v": 138,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10144957,
      "vd": -46784,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4058008,
      "vd": -46784,
      "sr": 44100
    },
    "sq": {
      "br": 1001337,
      "fid": 0,
      "size": 31737396,
      "vd": -46784,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889048",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 253000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6086991,
      "vd": -46784,
      "sr": 44100
    },
    "name": "给我一首歌的时间",
    "id": 185694,
    "privilege": {
      "id": 185694,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18896,
      "name": "11月的萧邦",
      "pic_str": "109951167749320136",
      "pic": 109951167749320130,
      "alia": [
        "November's Chopin"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 0,
    "mv": 504364,
    "t": 0,
    "v": 151,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 9076027,
      "vd": -59504,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3630437,
      "vd": -59504,
      "sr": 44100
    },
    "sq": {
      "br": 947790,
      "fid": 0,
      "size": 26873132,
      "vd": -59504,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [
      "Nocturne"
    ],
    "pop": 100,
    "rt": "600902000006889264",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 226827,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5445634,
      "vd": -59504,
      "sr": 44100
    },
    "name": "夜曲",
    "id": 185904,
    "privilege": {
      "id": 185904,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18886,
      "name": "我很忙",
      "pic_str": "109951163533011733",
      "pic": 109951163533011730,
      "alia": [
        "On The Run!"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 3,
    "fee": 0,
    "mv": 0,
    "t": 0,
    "v": 147,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 9576532,
      "vd": -30231,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3830638,
      "vd": -30231,
      "sr": 44100
    },
    "sq": {
      "br": 876973,
      "fid": 0,
      "size": 26237797,
      "vd": -30231,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889088",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 239000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5745936,
      "vd": -30231,
      "sr": 44100
    },
    "name": "青花瓷",
    "id": 185811,
    "privilege": {
      "id": 185811,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18886,
      "name": "我很忙",
      "pic_str": "109951163533011733",
      "pic": 109951163533011730,
      "alia": [
        "On The Run!"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 2,
    "fee": 0,
    "mv": 5293415,
    "t": 0,
    "v": 151,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10553512,
      "vd": -19701,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4221430,
      "vd": -19701,
      "sr": 44100
    },
    "sq": {
      "br": 834605,
      "fid": 0,
      "size": 27519460,
      "vd": -19701,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [
      "电影《命运呼叫转移》片尾曲"
    ],
    "pop": 100,
    "rt": "600902000006889092",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 263784,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6332124,
      "vd": -19701,
      "sr": 44100
    },
    "name": "彩虹",
    "id": 185809,
    "privilege": {
      "id": 185809,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18903,
      "name": "七里香",
      "pic_str": "7746059418324672",
      "pic": 7746059418324672,
      "alia": [
        "Common Jasmine Orange"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 6,
    "fee": 0,
    "mv": 506114,
    "t": 0,
    "v": 137,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 9607879,
      "vd": -52874,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3843177,
      "vd": -52874,
      "sr": 44100
    },
    "sq": {
      "br": 873358,
      "fid": 0,
      "size": 26215333,
      "vd": -52874,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889304",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 240000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5764745,
      "vd": -52874,
      "sr": 44100
    },
    "name": "搁浅",
    "id": 186005,
    "privilege": {
      "id": 186005,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34433014,
      "name": "婚礼歌手：幸福情歌精选",
      "pic_str": "109951165994895269",
      "pic": 109951165994895260
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 2,
    "fee": 0,
    "mv": 0,
    "t": 0,
    "v": 21,
    "h": {
      "br": 320002,
      "fid": 0,
      "size": 9448011,
      "vd": -24767,
      "sr": 44100
    },
    "l": {
      "br": 128002,
      "fid": 0,
      "size": 3779231,
      "vd": -20530,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "01",
    "alia": [],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 236173,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192002,
      "fid": 0,
      "size": 5668824,
      "vd": -22183,
      "sr": 44100
    },
    "name": "可爱女人",
    "id": 400579056,
    "privilege": {
      "id": 400579056,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 257,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18888,
      "name": "不能说的秘密 电影原声带",
      "pic_str": "109951167355197081",
      "pic": 109951167355197090
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 25,
    "fee": 0,
    "mv": 507196,
    "t": 0,
    "v": 163,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11863815,
      "vd": -33735,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4745552,
      "vd": -29489,
      "sr": 44100
    },
    "sq": {
      "br": 911478,
      "fid": 0,
      "size": 33785477,
      "vd": -33797,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [
      "电影《不能说的秘密》主题曲"
    ],
    "pop": 100,
    "rt": "600902000006889100",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 296533,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7118306,
      "vd": -31172,
      "sr": 44100
    },
    "name": "不能说的秘密",
    "id": 185868,
    "privilege": {
      "id": 185868,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "classic"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18893,
      "name": "依然范特西",
      "pic_str": "7980255395852522",
      "pic": 7980255395852522,
      "alia": [
        "Still Fantasy"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 2,
    "fee": 0,
    "mv": 451008,
    "t": 0,
    "v": 142,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10625610,
      "vd": -44116,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4250270,
      "vd": -44116,
      "sr": 44100
    },
    "sq": {
      "br": 950601,
      "fid": 0,
      "size": 31556828,
      "vd": -44116,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889204",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 265000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6375383,
      "vd": -44116,
      "sr": 44100
    },
    "name": "听妈妈的话",
    "id": 185879,
    "privilege": {
      "id": 185879,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18877,
      "name": "魔杰座",
      "pic_str": "109951163200234839",
      "pic": 109951163200234830,
      "alia": [
        "Capricorn"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 6,
    "fee": 0,
    "mv": 345057,
    "t": 0,
    "v": 144,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10271391,
      "vd": -31217,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4108582,
      "vd": -31217,
      "sr": 44100
    },
    "sq": {
      "br": 845550,
      "fid": 0,
      "size": 27133808,
      "vd": -31217,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889032",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 256720,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6162852,
      "vd": -31217,
      "sr": 44100
    },
    "name": "说好的幸福呢",
    "id": 185699,
    "privilege": {
      "id": 185699,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18875,
      "name": "跨时代",
      "pic_str": "19165587184063665",
      "pic": 19165587184063664,
      "alia": [
        "The Era"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 3,
    "fee": 0,
    "mv": 507197,
    "t": 0,
    "v": 154,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10537839,
      "vd": -33894,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4215161,
      "vd": -33894,
      "sr": 44100
    },
    "sq": {
      "br": 863856,
      "fid": 0,
      "size": 28441061,
      "vd": -33894,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000008436845",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 263000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6322720,
      "vd": -33894,
      "sr": 44100
    },
    "name": "烟花易冷",
    "id": 185668,
    "privilege": {
      "id": 185668,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18886,
      "name": "我很忙",
      "pic_str": "109951163533011733",
      "pic": 109951163533011730,
      "alia": [
        "On The Run!"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 10,
    "fee": 0,
    "mv": 347005,
    "t": 0,
    "v": 151,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 9436517,
      "vd": -24877,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3774633,
      "vd": -20519,
      "sr": 44100
    },
    "sq": {
      "br": 824788,
      "fid": 0,
      "size": 24314755,
      "vd": -24850,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889060",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 235840,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5661928,
      "vd": -22252,
      "sr": 44100
    },
    "name": "最长的电影",
    "id": 185821,
    "privilege": {
      "id": 185821,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18896,
      "name": "11月的萧邦",
      "pic_str": "109951167749320136",
      "pic": 109951167749320130,
      "alia": [
        "November's Chopin"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 3,
    "fee": 0,
    "mv": 186004,
    "t": 0,
    "v": 142,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11972484,
      "vd": -61244,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4789019,
      "vd": -61244,
      "sr": 44100
    },
    "sq": {
      "br": 973553,
      "fid": 0,
      "size": 36415829,
      "vd": -61244,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889256",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 299000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7183507,
      "vd": -61244,
      "sr": 44100
    },
    "name": "发如雪",
    "id": 185906,
    "privilege": {
      "id": 185906,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18886,
      "name": "我很忙",
      "pic_str": "109951163533011733",
      "pic": 109951163533011730,
      "alia": [
        "On The Run!"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 5,
    "fee": 0,
    "mv": 348001,
    "t": 0,
    "v": 142,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 9891047,
      "vd": -34277,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3956444,
      "vd": -34277,
      "sr": 44100
    },
    "sq": {
      "br": 868988,
      "fid": 0,
      "size": 26851698,
      "vd": -34277,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889080",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 247199,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5934645,
      "vd": -34277,
      "sr": 44100
    },
    "name": "蒲公英的约定",
    "id": 185815,
    "privilege": {
      "id": 185815,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18904,
      "name": "寻找周杰伦",
      "pic_str": "109951165564941972",
      "pic": 109951165564941970,
      "alia": [
        "Hidden Track"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 2,
    "fee": 0,
    "mv": 509095,
    "t": 0,
    "v": 133,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11903520,
      "vd": -38560,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4761434,
      "vd": -38560,
      "sr": 44100
    },
    "sq": {
      "br": 887631,
      "fid": 0,
      "size": 33012614,
      "vd": -38560,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [
      "《寻找周杰伦》电影插曲"
    ],
    "pop": 100,
    "rt": "600902000006889324",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 297000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7142129,
      "vd": -38560,
      "sr": 44100
    },
    "name": "断了的弦",
    "id": 186011,
    "privilege": {
      "id": 186011,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18915,
      "name": "范特西",
      "pic_str": "109951165606034156",
      "pic": 109951165606034160,
      "alia": [
        "Fantasy"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 10,
    "fee": 0,
    "mv": 507018,
    "t": 0,
    "v": 149,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 13371602,
      "vd": -36560,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 5348666,
      "vd": -36560,
      "sr": 44100
    },
    "sq": {
      "br": 798800,
      "fid": 0,
      "size": 33372543,
      "vd": -36560,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889428",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 334000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 8022978,
      "vd": -36560,
      "sr": 44100
    },
    "name": "安静",
    "id": 186139,
    "privilege": {
      "id": 186139,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "megabass"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18905,
      "name": "叶惠美",
      "pic_str": "109951165566379710",
      "pic": 109951165566379710,
      "alia": [
        "Yeh Hui–mei"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 0,
    "mv": 154071,
    "t": 0,
    "v": 140,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 13682982,
      "vd": -41275,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 5473219,
      "vd": -41275,
      "sr": 44100
    },
    "sq": {
      "br": 796250,
      "fid": 0,
      "size": 34039739,
      "vd": -41275,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889372",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 342000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 8209807,
      "vd": -41275,
      "sr": 44100
    },
    "name": "以父之名",
    "id": 186014,
    "privilege": {
      "id": 186014,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18907,
      "name": "八度空间",
      "pic_str": "109951166698447900",
      "pic": 109951166698447900,
      "alia": [
        "The Eight Dimensions"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 2,
    "fee": 0,
    "mv": 28118,
    "t": 0,
    "v": 137,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 12698689,
      "vd": -46834,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 5079502,
      "vd": -42660,
      "sr": 44100
    },
    "sq": {
      "br": 926245,
      "fid": 0,
      "size": 36748806,
      "vd": -46944,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889408",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 317400,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7619231,
      "vd": -44283,
      "sr": 44100
    },
    "name": "半岛铁盒",
    "id": 186046,
    "privilege": {
      "id": 186046,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18896,
      "name": "11月的萧邦",
      "pic_str": "109951167749320136",
      "pic": 109951167749320130,
      "alia": [
        "November's Chopin"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 6,
    "fee": 0,
    "mv": 186005,
    "t": 0,
    "v": 141,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11028941,
      "vd": -60471,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4411602,
      "vd": -60471,
      "sr": 44100
    },
    "sq": {
      "br": 889757,
      "fid": 0,
      "size": 30659615,
      "vd": -60471,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889244",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 275000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6617381,
      "vd": -60471,
      "sr": 44100
    },
    "name": "枫",
    "id": 185912,
    "privilege": {
      "id": 185912,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18877,
      "name": "魔杰座",
      "pic_str": "109951163200234839",
      "pic": 109951163200234830,
      "alia": [
        "Capricorn"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 4,
    "fee": 0,
    "mv": 345052,
    "t": 0,
    "v": 136,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10586949,
      "vd": -41135,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4234805,
      "vd": -41135,
      "sr": 44100
    },
    "sq": {
      "br": 882513,
      "fid": 0,
      "size": 29189166,
      "vd": -41135,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889040",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 264000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6352186,
      "vd": -41135,
      "sr": 44100
    },
    "name": "花海",
    "id": 185697,
    "privilege": {
      "id": 185697,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 2263029,
      "name": "十二新作",
      "pic_str": "109951163533013578",
      "pic": 109951163533013580,
      "alia": [
        "Opus 12"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 4,
    "fee": 0,
    "mv": 4909,
    "t": 0,
    "v": 144,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10417676,
      "vd": -31896,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4167097,
      "vd": -31896,
      "sr": 44100
    },
    "sq": {
      "br": 850332,
      "fid": 0,
      "size": 27676906,
      "vd": -31896,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000009525160",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 260386,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6250623,
      "vd": -31896,
      "sr": 44100
    },
    "name": "明明就",
    "id": 25641369,
    "privilege": {
      "id": 25641369,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18915,
      "name": "范特西",
      "pic_str": "109951165606034156",
      "pic": 109951165606034160,
      "alia": [
        "Fantasy"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 3,
    "fee": 0,
    "mv": 5343509,
    "t": 0,
    "v": 147,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10841904,
      "vd": -44256,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4336787,
      "vd": -44256,
      "sr": 44100
    },
    "sq": {
      "br": 957367,
      "fid": 0,
      "size": 32429220,
      "vd": -44256,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889456",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 270000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6505159,
      "vd": -44256,
      "sr": 44100
    },
    "name": "简单爱",
    "id": 186119,
    "privilege": {
      "id": 186119,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "megabass"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18886,
      "name": "我很忙",
      "pic_str": "109951163533011733",
      "pic": 109951163533011730,
      "alia": [
        "On The Run!"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 9,
    "fee": 0,
    "mv": 506106,
    "t": 0,
    "v": 147,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 9756255,
      "vd": -29183,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3902527,
      "vd": -29183,
      "sr": 44100
    },
    "sq": {
      "br": 983423,
      "fid": 0,
      "size": 29976230,
      "vd": -29183,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889064",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 243852,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5853770,
      "vd": -29183,
      "sr": 44100
    },
    "name": "甜甜的",
    "id": 185820,
    "privilege": {
      "id": 185820,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 6090,
        "name": "杨瑞代"
      }
    ],
    "al": {
      "id": 18875,
      "name": "跨时代",
      "pic_str": "19165587184063665",
      "pic": 19165587184063664,
      "alia": [
        "The Era"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 10,
    "fee": 0,
    "mv": 5397171,
    "t": 0,
    "v": 139,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10238998,
      "vd": -57020,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4095625,
      "vd": -57020,
      "sr": 44100
    },
    "sq": {
      "br": 975853,
      "fid": 0,
      "size": 31217559,
      "vd": -57020,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000003280405",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 255000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6143416,
      "vd": -57020,
      "sr": 44100
    },
    "name": "爱的飞行日记",
    "id": 185678,
    "privilege": {
      "id": 185678,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      },
      {
        "id": 9119,
        "name": "潘儿"
      }
    ],
    "al": {
      "id": 18893,
      "name": "依然范特西",
      "pic_str": "7980255395852522",
      "pic": 7980255395852522,
      "alia": [
        "Still Fantasy"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 0,
    "mv": 348010,
    "t": 0,
    "v": 136,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 9148124,
      "vd": -53994,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3659275,
      "vd": -53994,
      "sr": 44100
    },
    "sq": {
      "br": 968950,
      "fid": 0,
      "size": 27691113,
      "vd": -53994,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889208",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 228000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5488892,
      "vd": -53994,
      "sr": 44100
    },
    "name": "夜的第七章",
    "id": 185878,
    "privilege": {
      "id": 185878,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 3084335,
      "name": "哎呦，不错哦",
      "pic_str": "18874216602702134",
      "pic": 18874216602702136,
      "alia": [
        "Aiyo, Not Bad"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 3,
    "fee": 0,
    "mv": 375004,
    "t": 0,
    "v": 143,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11580754,
      "vd": -49643,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4632391,
      "vd": -49643,
      "sr": 44100
    },
    "sq": {
      "br": 882085,
      "fid": 0,
      "size": 31916336,
      "vd": -49643,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 289000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6948512,
      "vd": -49643,
      "sr": 44100
    },
    "name": "算什么男人",
    "id": 29818120,
    "privilege": {
      "id": 29818120,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18915,
      "name": "范特西",
      "pic_str": "109951165606034156",
      "pic": 109951165606034160,
      "alia": [
        "Fantasy"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 5,
    "fee": 0,
    "mv": 421024,
    "t": 0,
    "v": 146,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11401969,
      "vd": -41508,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4560813,
      "vd": -41508,
      "sr": 44100
    },
    "sq": {
      "br": 893878,
      "fid": 0,
      "size": 31841458,
      "vd": -41508,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889448",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 284000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6841199,
      "vd": -41508,
      "sr": 44100
    },
    "name": "开不了口",
    "id": 186125,
    "privilege": {
      "id": 186125,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "megabass"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18918,
      "name": "Jay",
      "pic_str": "7946170535396804",
      "pic": 7946170535396804,
      "alia": [
        "杰伦"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 9,
    "fee": 0,
    "mv": 504988,
    "t": 0,
    "v": 139,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10017479,
      "vd": -40780,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4007017,
      "vd": -40780,
      "sr": 44100
    },
    "sq": {
      "br": 889240,
      "fid": 0,
      "size": 27828853,
      "vd": -40780,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889472",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 250000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6010505,
      "vd": -40780,
      "sr": 44100
    },
    "name": "龙卷风",
    "id": 186160,
    "privilege": {
      "id": 186160,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "megabass"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18904,
      "name": "寻找周杰伦",
      "pic_str": "109951165564941972",
      "pic": 109951165564941970,
      "alia": [
        "Hidden Track"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 1,
    "fee": 0,
    "mv": 509097,
    "t": 0,
    "v": 133,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 13076941,
      "vd": -42489,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 5230802,
      "vd": -42489,
      "sr": 44100
    },
    "sq": {
      "br": 930590,
      "fid": 0,
      "size": 38020790,
      "vd": -42489,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [
      "《寻找周杰伦》电影主题曲"
    ],
    "pop": 100,
    "rt": "600902000006889328",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 326000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7846181,
      "vd": -42489,
      "sr": 44100
    },
    "name": "轨迹",
    "id": 186010,
    "privilege": {
      "id": 186010,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18886,
      "name": "我很忙",
      "pic_str": "109951163533011733",
      "pic": 109951163533011730,
      "alia": [
        "On The Run!"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 7,
    "fee": 0,
    "mv": 347002,
    "t": 0,
    "v": 172,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 11553479,
      "vd": -24652,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4621417,
      "vd": -24652,
      "sr": 44100
    },
    "sq": {
      "br": 899347,
      "fid": 0,
      "size": 32462017,
      "vd": -24652,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [
      "距离"
    ],
    "pop": 100,
    "rt": "600902000006889072",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 288000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6932105,
      "vd": -24652,
      "sr": 44100
    },
    "name": "我不配",
    "id": 185818,
    "privilege": {
      "id": 185818,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18896,
      "name": "11月的萧邦",
      "pic_str": "109951167749320136",
      "pic": 109951167749320130,
      "alia": [
        "November's Chopin"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 4,
    "fee": 0,
    "mv": 186006,
    "t": 0,
    "v": 143,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10089578,
      "vd": -46858,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4035857,
      "vd": -46858,
      "sr": 44100
    },
    "sq": {
      "br": 898831,
      "fid": 0,
      "size": 28331214,
      "vd": -46858,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889252",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 252160,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6053764,
      "vd": -46858,
      "sr": 44100
    },
    "name": "黑色毛衣",
    "id": 185908,
    "privilege": {
      "id": 185908,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 2263029,
      "name": "十二新作",
      "pic_str": "109951163533013578",
      "pic": 109951163533013580,
      "alia": [
        "Opus 12"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 8,
    "fee": 0,
    "mv": 96,
    "t": 0,
    "v": 138,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10965203,
      "vd": -33939,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4386107,
      "vd": -33939,
      "sr": 44100
    },
    "sq": {
      "br": 834489,
      "fid": 0,
      "size": 28588215,
      "vd": -33939,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000009524268",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 274066,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6579139,
      "vd": -33939,
      "sr": 44100
    },
    "name": "红尘客栈",
    "id": 25641368,
    "privilege": {
      "id": 25641368,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 1178287,
        "name": "派伟俊"
      },
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 34439096,
      "name": "Kung Fu Panda 3 (Music from the Motion Picture)",
      "pic_str": "109951166326634933",
      "pic": 109951166326634930,
      "alia": [
        "功夫熊猫3 电影原声带"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 22,
    "fee": 0,
    "mv": 0,
    "t": 0,
    "v": 94,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 9616239,
      "vd": -55516,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3846521,
      "vd": -55516,
      "sr": 44100
    },
    "sq": null,
    "hr": null,
    "cd": "1",
    "alia": [
      "电影《功夫熊猫3》全球主题曲"
    ],
    "pop": 100,
    "rt": null,
    "mst": 9,
    "cp": 7001,
    "crbt": null,
    "cf": "",
    "dt": 240352,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5769760,
      "vd": -55516,
      "sr": 44100
    },
    "name": "Try (Kung Fu Panda 3 Official Theme Song)",
    "id": 400876427,
    "privilege": {
      "id": 400876427,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 320000,
      "fl": 0,
      "toast": false,
      "flag": 1,
      "preSell": false,
      "playMaxbr": 320000,
      "downloadMaxbr": 320000,
      "maxBrLevel": "exhigh",
      "playMaxBrLevel": "exhigh",
      "downloadMaxBrLevel": "exhigh",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    }
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18905,
      "name": "叶惠美",
      "pic_str": "109951165566379710",
      "pic": 109951165566379710,
      "alia": [
        "Yeh Hui–mei"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 5,
    "fee": 0,
    "mv": 154070,
    "t": 0,
    "v": 140,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 12619275,
      "vd": -24859,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 5047736,
      "vd": -24859,
      "sr": 44100
    },
    "sq": {
      "br": 886719,
      "fid": 0,
      "size": 34960519,
      "vd": -24859,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889356",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 315000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 7571582,
      "vd": -24859,
      "sr": 44100
    },
    "name": "东风破",
    "id": 186018,
    "privilege": {
      "id": 186018,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18893,
      "name": "依然范特西",
      "pic_str": "7980255395852522",
      "pic": 7980255395852522,
      "alia": [
        "Still Fantasy"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 5,
    "fee": 0,
    "mv": 507128,
    "t": 0,
    "v": 137,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 10461561,
      "vd": -45164,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 4184650,
      "vd": -45164,
      "sr": 44100
    },
    "sq": {
      "br": 921158,
      "fid": 0,
      "size": 30106517,
      "vd": -45164,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889192",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 261000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 6276954,
      "vd": -45164,
      "sr": 44100
    },
    "name": "退后",
    "id": 185884,
    "privilege": {
      "id": 185884,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  },
  {
    "rtUrls": [],
    "ar": [
      {
        "id": 6452,
        "name": "周杰伦"
      }
    ],
    "al": {
      "id": 18907,
      "name": "八度空间",
      "pic_str": "109951166698447900",
      "pic": 109951166698447900,
      "alia": [
        "The Eight Dimensions"
      ]
    },
    "st": -1,
    "noCopyrightRcmd": null,
    "songJumpInfo": null,
    "rtype": 0,
    "djId": 0,
    "no": 8,
    "fee": 0,
    "mv": 154014,
    "t": 0,
    "v": 135,
    "h": {
      "br": 320000,
      "fid": 0,
      "size": 9341430,
      "vd": -50837,
      "sr": 44100
    },
    "l": {
      "br": 128000,
      "fid": 0,
      "size": 3736598,
      "vd": -50837,
      "sr": 44100
    },
    "sq": {
      "br": 815941,
      "fid": 0,
      "size": 23811877,
      "vd": -50837,
      "sr": 44100
    },
    "hr": null,
    "cd": "1",
    "alia": [],
    "pop": 100,
    "rt": "600902000006889384",
    "mst": 9,
    "cp": 1007,
    "crbt": null,
    "cf": "",
    "dt": 233000,
    "rtUrl": null,
    "ftype": 0,
    "rurl": null,
    "pst": 0,
    "a": null,
    "m": {
      "br": 192000,
      "fid": 0,
      "size": 5604875,
      "vd": -50837,
      "sr": 44100
    },
    "name": "回到过去",
    "id": 186055,
    "privilege": {
      "id": 186055,
      "fee": 0,
      "payed": 0,
      "st": -100,
      "pl": 0,
      "dl": 0,
      "sp": 7,
      "cp": 0,
      "subp": 0,
      "cs": false,
      "maxbr": 999000,
      "fl": 0,
      "toast": false,
      "flag": 256,
      "preSell": false,
      "playMaxbr": 999000,
      "downloadMaxbr": 999000,
      "maxBrLevel": "lossless",
      "playMaxBrLevel": "lossless",
      "downloadMaxBrLevel": "lossless",
      "plLevel": "none",
      "dlLevel": "none",
      "flLevel": "none",
      "rscl": 0,
      "freeTrialPrivilege": {
        "resConsumable": false,
        "userConsumable": false,
        "listenType": null
      },
      "chargeInfoList": [
        {
          "rate": 128000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 192000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 320000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 0
        },
        {
          "rate": 999000,
          "chargeUrl": null,
          "chargeMessage": null,
          "chargeType": 1
        }
      ]
    },
    "eq": "pop"
  }
]

const list = data.map(({ name, al, ar }) => {
  return {
    name, 
    al,
    ar: ar.map(v => v.name)
  }
})