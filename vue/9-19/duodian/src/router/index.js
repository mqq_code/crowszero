import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home.vue'
import GoHome from '../views/home/gohome/GoHome.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    children: [
      {
        path: '/',
        name: 'gohome',
        component: GoHome
      },
      {
        path: '/classify',
        name: 'classify',
        component: () => import('../views/home/classify/Classify.vue')
      },
      {
        path: '/cart',
        name: 'cart',
        component: () => import('../views/home/cart/Cart.vue')
      }
    ]
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: () => import('../views/detail/Detail.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
