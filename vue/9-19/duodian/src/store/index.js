import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    allList: [],
    cart: []
  },
  mutations: {
    setAllList (state, paylaod) {
      state.allList = paylaod
    },
    // 加入购物车
    addCart (state, { id }) {
      const index = state.cart.findIndex(v => v.id === id)
      if (index > -1) {
        state.cart[index].count += 1
      } else {
        const curGoods = state.allList.find(v => v.id === id)
        curGoods.count = 1
        state.cart.push({
          ...curGoods,
          checked: true
        })
      }
    },
    // 修改数量
    changeCart (state, { id, num }) {
      const index = state.cart.findIndex(v => v.id === id)
      state.cart[index].count += num
      if (state.cart[index].count <= 0) {
        state.cart.splice(index, 1)
      }
    },
    // 修改选中状态
    changeChecked (state, { id }) {
      state.cart.forEach(item => {
        if (item.id === id) {
          item.checked = !item.checked
        }
      })
    },
    // 修改全部状态
    changeAllChecked (state, { checked }) {
      state.cart.forEach(item => {
        item.checked = checked
      })
    }
  },
  actions: {
    async getList ({ commit }) {
      const res = await axios.get('/api/list')
      commit('setAllList', res.data.data)
    }
  },
  getters: {
    cartLen (state) {
      return state.cart.reduce((prev, val) => {
        return val.checked ? (prev + val.count) : prev
      }, 0)
    },
    total (state) {
      return state.cart.reduce((prev, val) => {
        return val.checked ? (prev + val.count * val.price) : prev
      }, 0)
    }
  }
})
