const { defineConfig } = require('@vue/cli-service')
const Mock = require('mockjs')

const data = Mock.mock({
  "list|50": [
    {
      "id": "@id",
      "name": "@cname",
      "count": 0,
      "price|1-100.1": 10,
      "img": "@image(100x100, @color, @word)",
      "desc": "@ctitle(100)",
      "banners|4-6": [{
        "imgId": "@id",
        "url": "@image(375x200, @color, @word)"
      }]
    }
  ]
})

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middleWares, devServer) {
      devServer.app.get('/api/list', (req, res) => {
        res.send({
          code: 200,
          msg: '成功',
          data: data.list
        })
      })
      return middleWares
    }
  }
})
