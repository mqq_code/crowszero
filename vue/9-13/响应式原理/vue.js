class Vue {
  constructor ({ el, data }) {
    this.$el = document.querySelector(el)
    this.$data = data

    // 数据劫持把data的所有属性改成访问器属性
    observe(this.$data)

    // 把 data 的所有属性挂载到实例对象，把 data 的属性和 this 关联起来
    Object.keys(this.$data).forEach(key => {
      Object.defineProperty(this, key, {
        get () {
          return this.$data[key]
        },
        set (val) {
          this.$data[key] = val
        }
      })
    })

    // 编译模版，把模版中的变量渲染成具体数据
    compile(this.$el, this)

  }
}

// 编译模版
function compile(el, vm) {
  // 遍历所有子节点
  el.childNodes.forEach(node => {
    // 查找文本节点
    if (node.nodeType === 3) {
      // 根据正则匹配 {{}}， 从data中查找数据
      let val = node.nodeValue.replace(/\{\{\s+(\S+)\s+\}\}/g, (...rest) => {
        console.log(rest[1]) // 找到的 {{}} 中的变量名
        new Watcher(vm, rest[1], (newVal) => {
          console.log('数据更新了')
          node.nodeValue = newVal
        })
        return getVal(vm, rest[1])
      })
      // 修改节点内容
      node.nodeValue = val
    } else {
      compile(node, vm)
    }
  })
}
function getVal (obj, key) {
  return key.split('.').reduce((prev, val) => prev[val], obj)
}

// 数据劫持
function observe(obj) {
  if (typeof obj !== 'object' || obj === null) return obj
  Object.keys(obj).forEach(key => {
    const subject = new Subject()
    // 递归把对象的所有属性都改成访问器属性
    observe(obj[key])
    let oldval = obj[key]
    Object.defineProperty(obj, key, {
      get () {
        console.log('有人获取数据', oldval, Subject.target)
        Subject.target && subject.add(Subject.target)
        return oldval
      },
      set (val) {
        console.log('修改', key)
        oldval = val
        // 通知页面更新
        subject.notify()
      }
    })
  })
}




// 发布订阅模式（无耦合，具体实现：EventBus）
// 观察者模式（低耦合）

// 被观察者
class Subject {
  list = []
  add (watcher) {
    this.list.push(watcher)
  }
  notify () {
    this.list.forEach(watcher => {
      watcher.update()
    })
  }
}

// 观察者
class Watcher {
  constructor(vm, keys, cb) {
    this.vm = vm
    this.keys = keys
    this.cb = cb
    Subject.target = this
    getVal(this.vm, this.keys)
    Subject.target = null
  }
  update () {
    this.cb(getVal(this.vm, this.keys))
  }
}
