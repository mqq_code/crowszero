class Router {
  constructor({ routes, routerView }) {
    this.routes = routes
    this.routerView = routerView
    // 初始化页面
    this.update()
    // 监听hash改变
    window.addEventListener('hashchange', () => {
      this.update()
    })
  }
  update () {
    // 获取当前地址栏的hash参数，去routes配置项中查找对应的path
    const hash = location.hash.slice(1)
    const cur = this.routes.find(item => item.path === hash)
    this.routerView.innerHTML = cur.component
  }
}