const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      // 配置代理处理跨域问题
      '/mqq': {
        target: 'http://ustbhuangyi.com',
        pathRewrite: { '^/mqq': '' },
        changeOrigin: true
      } 
    }
  }
})
