
export default {
  // 添加命名空间，让 mutations 和 action 函数称为局部方法
  namespaced: true,
  state: () => ({
    user: '王老吉',
    age: 20,
    sex: '男'
  }),
  mutations: {
    setUser (state, payload) {
      console.log('userinfo.js')
      state.user = payload
    },
    setAge (state, payload) {
      state.age = payload
    },
    setSex (state, payload) {
      state.sex = payload
    }
  },
  actions: {},
  getters: {}
}
