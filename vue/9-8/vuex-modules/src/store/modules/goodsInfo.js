
export default {
  namespaced: true,
  state: () => ({
    goodsTitle: '苹果',
    goodsPrice: 20,
    goodsCount: 0
  }),
  mutations: {
    setUser (state, payload) {
      console.log('goodsInfo.js')
    },
    setGoodsTitle (state, payload) {
      state.goodsTitle = payload
    },
    setGoodsPrice (state, payload) {
      state.goodsPrice = payload
    },
    setGoodsCount (state, payload) {
      state.goodsCount = payload
    }
  },
  actions: {},
  getters: {}
}
