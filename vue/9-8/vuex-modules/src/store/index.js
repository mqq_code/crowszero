import Vue from 'vue'
import Vuex from 'vuex'
import logger from 'vuex/dist/logger'
import userInfo from './modules/userInfo'
import goodsInfo from './modules/goodsInfo'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    rootTitle: '总部数据'
  },
  mutations: {
    setUser (state, payload) {
      console.log('index.js')
    }
  },
  actions: {
  },
  modules: {
    userInfo,
    goodsInfo
  },
  plugins: [logger()]
})

export default store
