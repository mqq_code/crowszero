const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devServer) {
      const food = require('./data/food.json')
      const milk = require('./data/milk.json')
      const vegetable = require('./data/vegetable.json')
      const data = [
        {
          title: '蔬菜',
          id: 'vegetable',
          child: vegetable
        },
        {
          title: '蛋奶豆制品',
          id: 'milk',
          child: milk
        },
        {
          title: '主食',
          id: 'food',
          child: food
        }
      ]

      devServer.app.get('/api/list', (req, res) => {
        res.send(data)
      })

      return middlewares
    }
  }
})
