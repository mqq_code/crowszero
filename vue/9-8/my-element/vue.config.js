const { defineConfig } = require('@vue/cli-service')
const express = require('express')
const md5 = require('md5')

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devServer) {
      devServer.app.use(express.json())
      devServer.app.post('/api/login', (req, res) => {
        const { name, password } = req.body
        if (name === 'zyx' && password === '123') {
          res.send({
            code: 0,
            msg: '成功',
            token: md5(name + password + Date.now())
          })
        } else {
          res.send({
            code: -1,
            msg: '用户名或者密码错误'
          })
        }
      })
      const list = require('./data/list.json')
      devServer.app.get('/api/list', (req, res) => {
        const { page, pagesize, query } = req.query
        if (Number(page) > 0 && Number(pagesize) > 0) {
          let newList = [...list]
          if (query) {
            newList = newList.filter(item => item.name.includes(query))
          }
          res.send({
            code: 0,
            msg: '成功',
            values: newList.slice(page * pagesize - pagesize, page * pagesize),
            total: newList.length
          })
        } else {
          res.send({
            code: -1,
            msg: '参数错误'
          })
        }
      })
      devServer.app.delete('/api/delete', (req, res) => {
        const { id } = req.query
        const index = list.findIndex(item => item.id === id)
        if (index > -1) {
          list.splice(index, 1)
          res.send({
            code: 0,
            msg: '成功'
          })
        } else {
          res.send({
            code: -1,
            msg: '参数错误'
          })
        }
      })
      devServer.app.post('/api/create', (req, res) => {
        const { name, age, sex } = req.body
        list.push({
          name,
          age,
          sex,
          num: list[list.length - 1].num + 1,
          id: Date.now() + ''
        })
        res.send({
          code: 0,
          msg: '成功'
        })
      })
      devServer.app.put('/api/update', (req, res) => {
        const { id } = req.body
        const index = list.findIndex(item => item.id === id)
        if (index > -1) {
          list.splice(index, 1, req.body)
          res.send({
            code: 0,
            msg: '成功'
          })
        } else {
          res.send({
            code: -1,
            msg: '参数错误'
          })
        }
      })
      return middlewares
    }
  }
})
