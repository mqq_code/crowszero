const Mock = require('mockjs')
const fs = require('fs')
const path = require('path')

const data = Mock.mock({
  "list|1000": [
    {
      "num|+1": 1,
      "id": "@id",
      "name": "@cname",
      "age|18-40": 18,
      "sex|0-1": 0
    }
  ]
})

fs.writeFileSync(path.join(__dirname, './list.json'), JSON.stringify(data.list))


