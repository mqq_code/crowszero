import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/home/Home.vue'
import Login from '@/views/login/Login.vue'
import Charts from '@/views/home/charts/Charts.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history', // 路由模式，hash 和 history
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        isAuth: true
      },
      children: [
        {
          path: '/',
          name: 'charts',
          component: Charts
        },
        {
          path: '/list',
          name: 'list',
          component: () => import('@/views/home/list/List.vue')
        },
        {
          path: '/add',
          name: 'add',
          component: () => import('@/views/home/add/Add.vue')
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(v => v.meta.isAuth)) {
    const token = localStorage.getItem('token')
    if (!token) {
      next('/login')
      return
    }
  }
  next()
})

export default router
