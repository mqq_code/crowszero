import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 完整引入，注册全局组件
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)

// 按需引入
// import { Button, InputNumber, Radio } from 'element-ui'
// Vue.use(Button)
// Vue.use(InputNumber)
// Vue.use(Radio)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
