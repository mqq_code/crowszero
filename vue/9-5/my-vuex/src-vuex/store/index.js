import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  // 存仓库的数据
  state: {
    num: 0,
    title: '王小明',
    hobby: [],
    isGrid: false
  },
  // 修改仓库数据的唯一方式
  mutations: {
    setTitle (state, payload) {
      state.title = payload
    },
    setNum (state, payload) {
      state.num += payload
    },
    addHobby (state, payload) {
      state.hobby.push(payload)
    },
    setGrid (state, payload) {
      state.isGrid = payload
    }
  }
})
