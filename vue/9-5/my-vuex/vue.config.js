const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devServer) {
      const xl = require('./data/xl.json')
      const zh = require('./data/zh.json')
      const sx = require('./data/sx.json')
      const all = [...xl.items, ...zh.items, ...sx.items]

      devServer.app.get('/api/xl', (req, res) => {
        res.send({
          code: 0,
          msg: '成功',
          values: xl
        })
      })
      devServer.app.get('/api/zh', (req, res) => {
        res.send({
          code: 0,
          msg: '成功',
          values: zh
        })
      })
      devServer.app.get('/api/sx', (req, res) => {
        res.send({
          code: 0,
          msg: '成功',
          values: sx
        })
      })
      devServer.app.get('/api/detail', (req, res) => {
        const { id } = req.query
        const info = all.find(item => item.item_id === id * 1)
        if (info) {
          res.send({
            code: 0,
            msg: '成功',
            values: info
          })
        } else {
          res.send({
            code: -1,
            msg: '参数错误'
          })
        }
      })

      return middlewares
    }
  }
})
