import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  // 存仓库的数据
  state: {
    isGrid: false,
    sortType: 0 // 0: 默认 1: 升序 2: 降序
  },
  // 修改仓库数据的唯一方式
  mutations: {
    setGrid (state, payload) {
      state.isGrid = payload
    },
    setSortType (state) {
      if (state.sortType === 2) {
        state.sortType = 0
      } else {
        state.sortType++
      }
    }
  }
})
