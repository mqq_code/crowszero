import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    username: 'zyx',
    age: 30,
    sex: '男',
    hotComments: []
  },
  mutations: {
    // 修改state数据的唯一方式，必须是同步函数
    setHotComments (state, payload) {
      state.hotComments = payload
    },
    setUser (state, payload) {
      state.username = payload
    },
    setAge (state, payload) {
      state.age = payload
    }
  },
  actions: {
    // 处理异步逻辑
    async getComments (context, params) {
      // context: 类似 this.$store 实例对象
      const res = await axios.get('https://zyxcl-music-api.vercel.app/artist/list', {
        params
      })
      // 调用 mutations 函数
      context.commit('setHotComments', res.data.artists)
      return res
    }
  },
  getters: {
    // 类似组件中的计算属性
    userInfo (state) {
      return `我的名字叫${state.username}, 今年${state.age}, 性别:${state.sex}`
    }
  }
})
