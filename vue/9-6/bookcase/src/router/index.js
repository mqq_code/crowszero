import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home.vue'
import Detail from '@/pages/Detail.vue'
import Bookcase from '@/pages/Bookcase.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/detail/:id',
      name: 'detail',
      component: Detail
    },
    {
      path: '/bookcase',
      name: 'bookcase',
      component: Bookcase
    }
  ]
})

export default router
