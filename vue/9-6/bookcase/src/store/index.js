import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import logger from 'vuex/dist/logger' // 修改vuex的数据时自动打印修改前后的数据
import VuexPersistence from 'vuex-persist' // vuex数据持久化储存

// 创建持久化存储的对象
const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    allList: [], // 所有数据
    bookcase: [] // 书架数据
  },
  mutations: {
    setAll (state, payload) {
      state.allList = payload
    },
    setBookcase (state, payload) {
      Vue.set(payload, 'isCase', !payload.isCase)
      Vue.set(payload, 'checked', false)
      if (payload.isCase) {
        state.bookcase.push(payload)
      } else {
        state.bookcase = state.bookcase.filter(item => item.id !== payload.id)
      }
    },
    changeAll (state, payload) {
      state.bookcase.forEach(book => {
        book.checked = payload
      })
    },
    changeBookChecked (state, index) {
      console.log(index)
      state.bookcase[index].checked = !state.bookcase[index].checked
      console.log(state.bookcase)
    },
    // 删除选中的书籍
    remove (state) {
      state.bookcase = state.bookcase.filter(item => !item.checked)
      state.allList.forEach(item => {
        if (item.checked) {
          item.isCase = false
        }
      })
    },
    // 置顶
    toTop (state) {
      state.bookcase.sort((a, b) => b.checked - a.checked)
    }
  },
  actions: {
    async getList (context) {
      const res = await axios.get('/api/list')
      context.commit('setAll', res.data.values)
    }
  },
  // vuex的插件
  plugins: [logger(), vuexLocal.plugin]
})

export default store
