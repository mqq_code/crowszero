const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devServer) {
      const data = require('./data/list.json')
      devServer.app.get('/api/list', (req, res) => {
        res.send({
          code: 0,
          msg: '成功',
          values: data.data
        })
      })
      devServer.app.get('/api/detail', (req, res) => {
        const { id } = req.query
        const book = data.data.find(item => item.id === id * 1)
        console.log(id, book)
        if (book) {
          res.send({
            code: 0,
            msg: '成功',
            values: book
          })
        } else {
          res.send({
            code: -1,
            msg: '参数错误'
          })
        }
      })
      return middlewares
    }
  }
})
