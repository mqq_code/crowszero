## Vue

### 指令
1. v-text、v-html 渲染内容
2. v-show 添加删除 display: none
3. v-if、v-else-if、v-else 条件渲染，添加删除dom元素
4. v-for 列表渲染
5. v-model 表单双向绑定
6. v-on 简写 @
7. v-bind 简写 :
8. v-slot 具名插槽

### 事件修饰符
1. .stop 阻止冒泡
2. .self 点击自身才执行
3. .prevent 阻止默认行为
4. .capture 事件捕获的方式绑定事件
5. .once 事件只执行一次
6. .keycode 点击对应的键码执行

### 计算属性
1. 是一个函数，必须有返回值，使用时当成data中的变量使用，不需要手动调用该函数
2. 不可以手动修改计算属性的值，会自动监听函数中使用的依赖项，依赖项改变重新执行函数计算返回值
3. 有缓存，依赖项改变执行函数，依赖项没改变从缓存中读取

### watch
1. 监听某个变量的改变执行对应的函数
2. 如果监听的是引用类型，监听不到属性的变化，需要使用 deep: true 深度监听
3. 变量改变才执行，如果希望进入页面立即执行一次需要使用 immediate: true

### 生命周期
1. 创建阶段
- beforeCreate 组件创建前，不能使用组件的数据
- created 组件创建成功，可以使用数据和方法，可以调用接口
2. 挂载阶段
- beforeMount 组件挂载之前，页面展示之前修改数据的最后机会，可以请求接口
- mounted 组件挂载之后，可以操作dom
3. 更新阶段
- beforeUpdate 数据更新后，页面更新前
- updated 数据更新后，页面更新后，可以获取到最新的dom元素
4. 销毁阶段
- beforeDestroy 组件销毁前，可以清除异步任务，例如：定时器、事件监听
- destroyed 组件销毁后

### 组件传参
1. 父组件传子组件
```jsx
// 父组件
<Child title="标题" />

// 子组件
props: ['title']
props: {
  title: {
    type: String, // 校验参数类型
    default: '默认标题' // 参数默认值
  }
}
```
2. 子组件传父组件
```jsx
// 父组件
<Child @change="change" />
methods: {
  change(val) {
    // val: 子组件传过来的数据
  }
}

// 子组件，通过$emit调用父组件的自定义事件
<button @click="$emit('change', '数据')"></button>
```

### 组件上的 v-model
```html
<!-- 父组件 -->
<Count v-model="num" />
<!-- v-model是以下用法的语法糖 -->
<Count :value="num" @input="num = $event" />

<!-- 子组件 -->
<template>
  <div>
    <button @click="$emit('input', value - 1)">-</button>
    {{ value }}
    <button @click="$emit('input', value + 1)">+</button>
  </div>
</template>

props: ['value']

```

### 组件上的 .sync 修饰符
```html
<!-- 父组件 -->
<Count :num.sync="num" />
<!-- v-model是以下用法的语法糖 -->
<Count :num="num" @update:num="num = $event" />

<!-- 子组件 -->
<template>
  <div>
    <button @click="$emit('update:num', value - 1)">-</button>
    {{ num }}
    <button @click="$emit('update:num', value + 1)">+</button>
  </div>
</template>

props: ['num']
```

### 插槽
- 当组件内的某一部分内容调用时才确定的情况下可以使用 <slot></slot> 占位，调用组件时使用组件双标签内的元素替换 slot
```html
<!-- 父组件 -->
<Child>
  <template v-slot:header="data">
    <div>头部内容 {{ data.num }}</div>
  </template>

  <p>内容</p>
  <p>内容</p>
  <p>内容</p>

  <template v-slot:footer>
    <div>底部内容</div>
  </template>
</Child>

<!-- 子组件 -->
<template>
  <div>
    <slot name="header" :test="test" :num="num"></slot>
    <h1>子组件</h1>
    <slot></slot>
    <footer>
      <slot name="footer">插槽默认值</slot>
    </footer>
  </div>
</template>
```

### 动态组件
- 根据组件名切换展示的组件内容
```html
<!-- comName: 组件名 -->
<component :is="comName"></component>
```

### 异步组件(组件懒加载)
1. 打包时把异步组件打包成单独的js文件
2. Com组件调用时在加载对应的js文件

```js
components: {
  Com: () => import('组件路径')
}
```

### keep-alive
1. keep-alive: vue的内置组件，缓存组件状态，切换组件时不会销毁
2. :include="['First', 'Second']" 只缓存名称匹配的组件
3. :exclude="['First']" 不缓存指定的组件
4. max="2" 最多缓存几个组件
```html
<keep-alive :include="['Com1', 'Com2']" :exclude="['Com3']" max="2">
  <Com1 />
  <Com2 />
  <Com3 />
</keep-alive>
```

### Vue.set、this.$set
```js
// this.obj.age = 100 // 直接给对象添加新属性页面不会更新
this.$set(this.obj, 'age', 100) // 给对象添加能够被vue监听的属性
```

### nextTick
```js
methods: {
  add () {
    this.count ++
    this.$nextTick(() => {
      // 修改数据之后等待本次页面更新完成以后执行，可以获取最新的dom元素
      console.log(this.$refs.num.innerHTML)
    })
  }
}
```
### 过渡动画

## Router
### vue-router 
- 让单页面实现多页面的效果