const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/sell': {
        target: 'http://ustbhuangyi.com',
        changeOrigin: true
      }
    }
  }
})
