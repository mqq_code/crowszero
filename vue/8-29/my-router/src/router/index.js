import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/home/Home.vue'
import Detail from '../pages/detail/Detail.vue'
import Login from '../pages/login/Login.vue'
import City from '../pages/city/City.vue'
import NotFound from '../pages/404.vue'
import Movie from '../pages/home/movie/Movie.vue'
import Cinema from '../pages/home/cinema/Cinema.vue'
import Mine from '../pages/home/mine/Mine.vue'
import Hot from '../pages/home/movie/hot/Hot.vue'
import Coming from '../pages/home/movie/coming/Coming.vue'

// 给 vue 添加路由插件
Vue.use(VueRouter)

// 实例化路由插件
const router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect: '/movie', // 自动重定向到 /movie
      children: [ // 配置子路由
        {
          path: '/movie', // 子路由的path必须包含父路由的path
          name: 'movie',
          redirect: '/movie/hot',
          component: Movie,
          children: [
            {
              path: '/movie/hot',
              name: 'hot',
              component: Hot
            },
            {
              path: '/movie/coming',
              name: 'coming',
              component: Coming
            }
          ]
        },
        {
          path: '/cinema',
          name: 'cinema',
          component: Cinema
        },
        {
          path: '/mine',
          name: 'mine',
          component: Mine
        }
      ]
    },
    {
      path: '/detail/:id', // 配置动态路由
      name: 'detail',
      component: Detail
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/city',
      name: 'city',
      component: City
    },
    {
      path: '*',
      // redirect: '/', 如果以上路径都没有匹配成功，自动跳转首页
      component: NotFound // 如果以上路径都没有匹配成功，自动跳转404页面
    }
  ]
})

export default router
