const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    // server: 'https',
    // proxy: {
    //   '/mqq': {
    //     target: 'https://m.maizuo.com',
    //     pathRewrite: { '^/mqq': '' },
    //     changeOrigin: true,
    //     secure: false,
    //   }
    // }
    setupMiddlewares: (middlewares, devServer) => {
      const city = require('./data/city.json')
      const hot = require('./data/hot.json')
      const coming = require('./data/coming.json')

      devServer.app.get('/mqq/city', (_, response) => {
        response.send(city);
      });
      devServer.app.get('/mqq/hot', (_, response) => {
        response.send(hot);
      });
      devServer.app.get('/mqq/coming', (_, response) => {
        response.send(coming);
      });

      return middlewares;
    }
  }
})
