import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

new Vue({
  router, // 给 vue 实例添加路由配置
  render: h => h(App)
}).$mount('#app')
