import Vue from 'vue'
import VueRouter from 'vue-router'
import Movie from '../pages/Movie.vue'
import Cinema from '../pages/Cinema.vue'
import Mine from '../pages/Mine.vue'

// 给 vue 添加路由插件
Vue.use(VueRouter)

// 实例化路由插件
const router = new VueRouter({
  routes: [
    // 路由页面配置
    {
      path: '/',
      redirect: '/movie' // 重定向，当地址为 '/' 时，自动跳转 '/movie' 页面
    },
    {
      path: '/movie',
      name: 'movie',
      component: Movie
    },
    {
      path: '/movie/cinema',
      name: 'cinema',
      component: Cinema
    },
    {
      path: '/movie/cinema/mine',
      name: 'mine',
      component: Mine
    }
  ]
})

export default router
