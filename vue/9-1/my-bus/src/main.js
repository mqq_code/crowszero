import Vue from 'vue'
import App from './App.vue'
import router from './router'
import loading from './assets/loading.gif'
import fail from './assets/fail.jpeg'

Vue.prototype.$bus = new Vue()
Vue.config.productionTip = false

// 注册自定义指令 Vue.directive('指令名', 指令配置项)
Vue.directive('loadImg', {
  bind (el, binding) { // 指令第一次绑定到元素时调用
    // console.log('bind', el)
    console.log('bind', binding) // 获取指令中的值
    el.src = loading // 把图片换成loading
    // 动态加载图片
    const img = new Image()
    img.src = binding.value
    img.onload = () => {
      // 加载成功把图片换回去
      setTimeout(() => {
        el.src = binding.value
      }, 3000)
    }
    img.onerror = () => {
      // 加载失败换成失败的图片
      setTimeout(() => {
        el.src = fail
      }, 3000)
    }
  },
  inserted () { // 指令的元素添加到父元素时调用，可以访问父元素
    console.log('inserted')
  },
  update (el, binding) { // 组件更新时调用
    console.log('update', binding)
    if (binding.value === binding.oldValue) return
    el.src = loading // 把图片换成loading
    // 动态加载图片
    const img = new Image()
    img.src = binding.value
    img.onload = () => {
      // 加载成功把图片换回去
      setTimeout(() => {
        el.src = binding.value
      }, 3000)
    }
    img.onerror = () => {
      // 加载失败换成失败的图片
      setTimeout(() => {
        el.src = fail
      }, 3000)
    }
  },
  componentUpdated () { // 组件更新完成调用
    console.log('componentUpdated')
  },
  unbind () { // 元素销毁时调用
    console.log('unbind')
  }
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
