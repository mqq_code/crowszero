import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/home/Home.vue'
import NotFound from '../pages/404.vue'
import Movie from '../pages/home/movie/Movie.vue'
import Cinema from '../pages/home/cinema/Cinema.vue'
import Mine from '../pages/home/mine/Mine.vue'
import Hot from '../pages/home/movie/hot/Hot.vue'
import Coming from '../pages/home/movie/coming/Coming.vue'

// 给 vue 添加路由插件
Vue.use(VueRouter)

// 实例化路由插件
const router = new VueRouter({
  mode: 'hash',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect: '/movie', // 自动重定向到 /movie
      children: [ // 配置子路由
        {
          path: '/movie', // 子路由的path必须包含父路由的path
          name: 'movie',
          redirect: '/movie/hot',
          component: Movie,
          children: [
            {
              path: '/movie/hot',
              name: 'hot',
              component: Hot
            },
            {
              path: '/movie/coming',
              name: 'coming',
              component: Coming
            }
          ]
        },
        {
          path: '/cinema',
          name: 'cinema',
          component: Cinema,
          meta: {
            isAuth: true // 表示需要拦截
          }
        },
        {
          path: '/mine',
          name: 'mine',
          meta: {
            isAuth: true // 表示需要拦截
          },
          component: Mine
        }
      ]
    },
    {
      path: '/detail/:id', // 配置动态路由
      name: 'detail',
      meta: {
        isAuth: true // 表示需要拦截
      },
      // 组件懒加载：跳转到此页面时再加载对应的js文件
      component: () => import(/* webpackChunkName: 'detail' */ '../pages/detail/Detail.vue')
      // beforeEnter (to, form, next) {
      //   console.log(to)
      //   next()
      // }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: 'login' */ '../pages/login/Login.vue')
    },
    {
      path: '/city',
      name: 'city',
      component: () => import(/* webpackChunkName: 'city' */ '../pages/city/City.vue')
    },
    {
      path: '*',
      // redirect: '/', 如果以上路径都没有匹配成功，自动跳转首页
      component: NotFound // 如果以上路径都没有匹配成功，自动跳转404页面
    }
  ]
})

// 全局路由前置守卫（路由拦截）
router.beforeEach((to, from, next) => {
  console.log('to', to)
  // console.log('from', from)
  if (to.meta.isAuth) {
    const token = localStorage.getItem('token')
    if (!token) {
      // 如果本地没有token表示没有登陆，跳转到登陆页面
      next({
        path: '/login',
        query: {
          // 把原本要跳转的路径传给登陆页，登陆成功之后跳转过去
          fullPath: to.fullPath
        }
      })
      return
    }
  }
  next()
})

export default router
