const { defineConfig } = require('@vue/cli-service')
const express = require('express')
const md5 = require('md5')

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    // server: 'https',
    // proxy: {
    //   '/mqq': {
    //     target: 'https://m.maizuo.com',
    //     pathRewrite: { '^/mqq': '' },
    //     changeOrigin: true,
    //     secure: false,
    //   }
    // }
    setupMiddlewares: (middlewares, devServer) => {
      const city = require('./data/city.json')
      const hot = require('./data/hot.json')
      const coming = require('./data/coming.json')
      const allData = [...hot.data.films, ...coming.data.films]

      devServer.app.get('/mqq/city', (_, response) => {
        response.send(city);
      });

      devServer.app.get('/mqq/hot', (_, response) => {
        response.send(hot);
      });
      devServer.app.get('/mqq/coming', (_, response) => {
        response.send(coming);
      });

      devServer.app.get('/mqq/detail', (res, response) => {
        const { id } = res.query
        if (!id) {
          response.send({
            code: -1,
            msg: '参数错误'
          });
          return
        }
        const info = allData.find(item => item.filmId === Number(id))
        if (info) {
          response.send({
            code: 0,
            msg: '成功',
            value: info
          });
        } else {
          response.send({
            code: -1,
            msg: '参数错误'
          });
        }
      });
      // 让 post 可以接受 json 格式的数据
      devServer.app.use(express.json())
      devServer.app.post('/mqq/login', (req, res) => {
        const { username, password } = req.body
        res.send({
          code: 0,
          msg: '成功',
          token: md5(username + password)
        })
      })

      return middlewares;
    }
  }
})
