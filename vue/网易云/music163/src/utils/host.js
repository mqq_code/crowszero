const getHost = () => {
  // 根据环境变量区分是开发环境还是生产环境
  switch (process.env.NODE_ENV) {
    case 'production':
      return 'https://zyxcl-music-api.vercel.app'
    default:
      return 'http://localhost:3000'
  }
}

export default getHost()
