import axios from 'axios'
import host from '@/utils/host'
import router from '@/router'
import { Toast } from 'vant'
import Cookies from 'js-cookie'

// 二次封装axios，添加拦截器，
// 创建 axios 实例对象
const request = axios.create({
  withCredentials: true, // 请求接口是自动把cookie传入请求头
  baseURL: host,
  timeout: 3000 // 请求超时时间
})

// 请求拦截器，可以修改请求参数、请求头，添加公共参数
request.interceptors.request.use(function (config) {
  config.url += `?csrf_token=${Cookies.get('__csrf')}&now=${Date.now()}`
  return config
}, function (error) {
  // 请求错误拦截
  return Promise.reject(error)
})

// 响应拦截器处理公共错误，例如登陆失效自动跳转登陆
request.interceptors.response.use(function (response) {
  // 响应成功拦截
  const data = response.data
  if (data.code === 302) {
    // 没有登陆直接跳转登陆
    Toast('登陆信息失效，请重新登陆')
    router.push('/login')
    return Promise.reject(Error(data.message))
  } else {
    return data
  }
}, function (error) {
  // 响应错误拦截
  const data = error.response.data
  return Promise.reject(data || error.message)
})

export default request
