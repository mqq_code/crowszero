import request from '../request'

// 轮播图接口
export const getBanner = () => {
  return request.post('/banner')
}
