import request from '../request'

// 用户详情
export const userDetail = (uid) => {
  return request.post('/user/detail', { uid })
}

// 账号信息
export const userAccount = (params = {}) => {
  return request.post('/user/account', params)
}

// 用户信息
export const userSubcount = (params = {}) => {
  return request.post('/user/subcount', params)
}

// 用户等级信息
export const userLevel = (params = {}) => {
  return request.post('/user/level', params)
}
