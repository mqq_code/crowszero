import request from '../request'

// 热搜接口
export const getSearchHot = () => {
  return request.get('/search/hot')
}
