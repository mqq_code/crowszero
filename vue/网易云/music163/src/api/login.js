import request from '../request'

// 手机登陆接口
export const loginPhone = (params = {}) => {
  return request.post('/login/cellphone', params)
}

// 二维码 key 生成接口
export const qrKey = () => {
  return request.post('/login/qr/key')
}

// 二维码生成接口
export const qrCreate = (key, qrimg) => {
  return request.post('/login/qr/create', { key, qrimg })
}

// 二维码检测扫码状态接口
export const qrCheck = key => {
  return request.post('/login/qr/check', { key })
}
