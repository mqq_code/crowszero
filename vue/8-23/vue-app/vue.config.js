const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares: (middlewares, devServer) => {
      const data = require('./data.json')
      devServer.app.get('/task/list', (req, res) => {
        res.send(data)
      });

      return middlewares;
    },
  }
})
