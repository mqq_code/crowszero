export default {
  data () {
    return {
      test: '测试一下',
      size: 'middle',
      num: 0
    }
  },
  computed: {},
  watch: {
    size () {
      console.log('窗口大小改变了', this.size)
    }
  },
  methods: {
    changeSize () {
      if (window.innerWidth >= 1000) {
        this.size = 'big'
      } else if (window.innerWidth < 1000 && window.innerWidth >= 600) {
        this.size = 'middle'
      } else {
        this.size = 'small'
      }
    }
  },
  created () {
    this.changeSize()
    window.addEventListener('resize', this.changeSize)
    console.log('混入的生命周期')
  }
}
