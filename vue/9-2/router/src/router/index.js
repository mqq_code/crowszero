import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/home/Home.vue'
import Login from '@/views/login/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      isauth: true
    },
    children: [
      {
        path: '/',
        name: 'child1',
        component: () => import('@/views/home/Child1.vue')
      },
      {
        path: '/child2',
        name: 'child2',
        component: () => import('@/views/home/Child2.vue')
      },
      {
        path: '/child3',
        name: 'child3',
        component: () => import('@/views/home/Child3.vue')
      },
      {
        path: '/child4',
        name: 'child4',
        component: () => import('@/views/home/Child4.vue')
      },
      {
        path: '/child5',
        name: 'child5',
        component: () => import('@/views/home/Child5.vue')
      },
      {
        path: '/child6',
        name: 'child6',
        component: () => import('@/views/home/Child6.vue')
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  console.log(to)
  // 判断所有父级路由和当前路由是否需要判断登陆
  if (to.matched.some(v => v.meta.isauth)) {
    const token = localStorage.getItem('token')
    if (!token) {
      next({
        path: '/login',
        query: {
          fullPath: to.fullPath
        }
      })
      return
    }
  }
  next()
})

export default router
