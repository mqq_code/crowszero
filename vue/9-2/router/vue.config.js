const { defineConfig } = require('@vue/cli-service')
const express = require('express')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares: (middlewares, devServer) => {
      devServer.app.use(express.json())
      devServer.app.post('/api/login', (req, res) => {
        const { name, password } = req.body
        if (!name || !password) {
          res.send({
            code: -1,
            msg: '参数错误',
          })
          return
        }
        res.send({
          code: 0,
          msg: '成功',
          token: name + password
        })
      })
      return middlewares;
    }
  }
})
