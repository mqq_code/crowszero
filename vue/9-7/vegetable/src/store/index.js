import Vue from 'vue'
import Vuex from 'vuex'
import logger from 'vuex/dist/logger'
import VuexPersistence from 'vuex-persist'
import axios from 'axios'

Vue.use(Vuex)

const local = new VuexPersistence({
  local: window.localStorage
})

const store = new Vuex.Store({
  state: {
    allList: [], // 所有食物
    selected: [] // 选中觉得食物
  },
  mutations: {
    // 获取所有数据
    setAllList (state, payload) {
      state.allList = payload
    },
    // 添加或者删除选中的食物
    changeSelected (state, payload) {
      // 根据传过来的食物名在所有数据中找到点击的食物
      state.allList.forEach(item => {
        item.child.forEach(food => {
          if (food.name === payload) {
            Vue.set(food, 'checked', !food.checked)
            if (food.checked) {
              state.selected.push(food)
            } else {
              state.selected = state.selected.filter(v => v.name !== payload)
            }
          }
        })
      })
    }
  },
  actions: {
    async getList ({ commit }) {
      const res = await axios.get('/api/list')
      commit('setAllList', res.data)
    }
  },
  plugins: [logger(), local.plugin]
})

export default store
