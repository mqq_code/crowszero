import Vue from 'vue'
import Vuex from 'vuex'
import logger from 'vuex/dist/logger' // 修改vuex的数据时自动打印修改前后的数据
import VuexPersistence from 'vuex-persist' // vuex数据持久化储存

// 创建持久化存储的对象
const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    list: [
      {
        id: 0,
        text: '打羽毛球',
        checked: true
      },
      {
        id: 1,
        text: '打篮球球',
        checked: true
      },
      {
        id: 3,
        text: '保龄球球',
        checked: true
      },
      {
        id: 4,
        text: '高尔夫球',
        checked: true
      }
    ],
    test: '1111'
  },
  mutations: {
    changeAll (state, payload) {
      state.list.forEach(item => {
        item.checked = payload
      })
    },
    changeItem (state, index) {
      state.list[index].checked = !state.list[index].checked
    }
  },
  // vuex的插件
  plugins: [logger(), vuexLocal.plugin]
})

export default store
